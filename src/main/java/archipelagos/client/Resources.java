package archipelagos.client;

import archipelagos.common.settings.SettingNames;
import archipelagos.common.settings.Settings;
import archipelagos.common.settings.SettingsFile;

import java.util.*;

/**
 * Stores a set of resources used within the Client; some are provided by default.
 */
public final class Resources
{
    // Configuration file settings

    private static final String SETTINGS_FILE_NAME = "archipelagos.settings";
    private static final String SETTINGS_FILE_LOCATION = ".";


    // Default values for (possible) configuration file settings

    private static final String HTTP_URL_DEFAULT = "http://www.archipelagos-labs.com";
    private static final String HTTP_PORT_DEFAULT = "6725";


    // The static variables and the code to initialize them

    private final static Map<String, String> SETTINGS = new HashMap<>();

    static
    {
        // Specify any default settings

        SETTINGS.put(SettingNames.HTTP_SERVER_URL, HTTP_URL_DEFAULT);
        SETTINGS.put(SettingNames.HTTP_SERVER_PORT, HTTP_PORT_DEFAULT);


        // Check if a configuration file was provided

        SettingsFile settingsFile = SettingsFile.locate(SETTINGS_FILE_NAME, SETTINGS_FILE_LOCATION);

        if (settingsFile != null)
        {
            Settings settings = settingsFile.getSettings();

            if (settings.hasSetting(SettingNames.HTTP_SERVER_URL))
            {
                String value = settings.getSetting(SettingNames.HTTP_SERVER_URL);
                SETTINGS.put(SettingNames.HTTP_SERVER_URL, value);
            }

            if (settings.hasSetting(SettingNames.HTTP_SERVER_PORT))
            {
                String value = settings.getSetting(SettingNames.HTTP_SERVER_PORT);
                SETTINGS.put(SettingNames.HTTP_SERVER_URL, value);
            }
        }
    }


    /**
     * Update a setting.
     *
     * @param name  The name of the setting.
     * @param value The value of the setting.
     * @return The previous value of the setting, or null if there was no such value.
     */
    public static String updateSetting(String name, String value)
    {
        return SETTINGS.put(name, value);
    }


    /**
     * Determine if a specified setting exists.
     *
     * @param name The name of the setting.
     * @return true if there is such a setting, false otherwise.
     */
    public static boolean containsSetting(String name)
    {
        return SETTINGS.containsKey(name);
    }


    /**
     * Obtain the value for a specified setting; assumes containsSetting(name).
     *
     * @param name The name of the setting.
     * @return The value of the setting.
     */
    public static String getSetting(String name)
    {
        return SETTINGS.get(name);
    }


    /**
     * Remove a specified setting.
     *
     * @param name The name of the setting.
     * @return The previous value of the setting, or null if there was no such value.
     */
    public static String removeSetting(String name)
    {
        return SETTINGS.remove(name);
    }
}
