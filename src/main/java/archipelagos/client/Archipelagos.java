package archipelagos.client;

import archipelagos.common.settings.SettingNames;
import archipelagos.common.data.collection.*;
import archipelagos.common.data.filestore.*;
import archipelagos.common.data.timeseries.*;
import archipelagos.common.platform.*;

import org.apache.http.entity.*;
import org.apache.http.client.utils.*;
import org.apache.http.*;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.*;
import org.apache.http.util.*;

import java.util.List;
import java.util.Map;

/**
 * The entry point for the functionality provided by the platform.
 */
public final class Archipelagos
{
    private static final int HTTP_OK_RESPONSE = 200;
    public static String API_KEY = null;


    /**
     * Builds a URL to be sent to the platform as a HTTP request.
     *
     * @param params The query parameters.
     */
    public static String buildUrl(Map<String, String> params) throws Exception
    {
        // Obtain the URL and port to use when connecting to the platform

        String httpUrl = Resources.getSetting(SettingNames.HTTP_SERVER_URL);

        if (httpUrl == null)
        {
            throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_URL));
        }

        String httpPortStr = Resources.getSetting(SettingNames.HTTP_SERVER_PORT);
        int httpPort = -1;

        if (httpPortStr == null)
        {
            throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_PORT));
        }
        else
        {
            try
            {
                httpPort = Integer.parseInt(httpPortStr);

                if (httpPort < 1)
                {
                    throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
                }
            }
            catch (Exception e)
            {
                throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
            }
        }


        // Return the URL

        return buildUrl(params, httpUrl, httpPort);
    }


    /**
     * Builds a URL to be sent to the platform as a HTTP request.
     *
     * @param params  The query parameters.
     * @param baseUrl The base URL (i.e. minus port number and query parameters e.g. https://www.archipelagos.com).
     * @param port    The port number to use.
     */
    public static String buildUrl(Map<String, String> params, String baseUrl, int port) throws Exception
    {
        // Check the parameters

        if (params == null)
        {
            throw new IllegalArgumentException("No value has been provided for params");
        }

        if (baseUrl == null)
        {
            throw new IllegalArgumentException("No value has been provided for baseUrl");
        }

        if (port < 1)
        {
            throw new IllegalArgumentException("port is not positive");
        }


        // Build the URL for the request

        URIBuilder uriBuilder = new URIBuilder(baseUrl);
        uriBuilder.setPort(port);

        for (Map.Entry<String, String> entry : params.entrySet())
        {
            uriBuilder.addParameter(entry.getKey(), entry.getValue());
        }

        uriBuilder.addParameter(HttpConstants.CLIENT_TYPE_PARAMETER, ClientType.JAVA_CLIENT);

        return uriBuilder.build().toASCIIString();
    }


    /**
     * Sends a HTTP request to the platform.
     *
     * @param request The request to send.
     * @return The response to request.
     */
    public static String send(String request) throws Exception
    {
        try (CloseableHttpClient httpClient = HttpClients.createDefault())
        {
            // Check that an API Key has been provided

            URIBuilder uriBuilder = new URIBuilder(request);
            List<NameValuePair> params = uriBuilder.getQueryParams();
            boolean foundApiKey = false;

            for (NameValuePair pair : params)
            {
                if (HttpConstants.API_KEY_PARAMETER.equals(pair.getName()))
                {
                    if (pair.getValue() != null)
                    {
                        foundApiKey = true;
                    }

                    break;
                }
            }

            if (!foundApiKey)
            {
                if (Archipelagos.API_KEY == null)
                {
                    throw new IllegalArgumentException("Neither request or Archipelagos contains an API Key");
                }
                else
                {
                    uriBuilder.addParameter(HttpConstants.API_KEY_PARAMETER, Archipelagos.API_KEY);
                }
            }


            // Build the http post and send

            HttpPost httpPost = new HttpPost(uriBuilder.build());

            try (CloseableHttpResponse response = httpClient.execute(httpPost))
            {
                // Inspect the status of the response

                StatusLine statusLine = response.getStatusLine();
                int responseCode = statusLine.getStatusCode();

                if (responseCode == HTTP_OK_RESPONSE)
                {
                    // Check that a response object has been provided

                    HttpEntity entity = response.getEntity();

                    if (entity.getContentLength() > 0)
                    {
                        return EntityUtils.toString(entity);
                    }
                    else
                    {
                        String message = "The platform returned a HTTP response but this did contain the expected data";
                        throw new Exception(message);
                    }
                }
                else
                {
                    String reason = statusLine.getReasonPhrase();
                    String message = String.format("The platform returned an unexpected HTTP response code (%s) and reason: \"%s\"; the response received was:\n\n%s", responseCode, reason, response);
                    throw new Exception(message);
                }
            }
        }
    }


    /**
     * Sends a time-series request to the platform.
     *
     * @param request The request to send.
     * @return The response to the request.
     */
    public static TimeSeriesResponse send(TimeSeriesRequest request) throws Exception
    {
        try (CloseableHttpClient httpClient = HttpClients.createDefault())
        {
            // Check that an API Key has been provided

            if (request.getApiKey() == null)
            {
                request.setApiKey(Archipelagos.API_KEY);
            }

            if (request.getApiKey() == null)
            {
                throw new IllegalArgumentException("Neither request or Archipelagos contains an API Key");
            }


            // Obtain the URL and port to use when connecting to the platform

            String httpUrl = Resources.getSetting(SettingNames.HTTP_SERVER_URL);

            if (httpUrl == null)
            {
                throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_URL));
            }

            String httpPortStr = Resources.getSetting(SettingNames.HTTP_SERVER_PORT);
            int httpPort = -1;

            if (httpPortStr == null)
            {
                throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_PORT));
            }
            else
            {
                try
                {
                    httpPort = Integer.parseInt(httpPortStr);

                    if (httpPort < 1)
                    {
                        throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
                    }
                }
                catch (Exception e)
                {
                    throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
                }
            }


            // Build the URL for the request

            URIBuilder uriBuilder = new URIBuilder(httpUrl);
            uriBuilder.setPort(httpPort);
            uriBuilder.addParameter(HttpConstants.FORMAT_PARAMETER, HttpConstants.FORMAT_BINARY);
            uriBuilder.addParameter(HttpConstants.TYPE_PARAMETER, HttpConstants.TIME_SERIES_REQUEST);


            // Build the http post and send

            HttpPost httpPost = new HttpPost(uriBuilder.build());
            byte[] serializedRequest = request.serialize();
            ByteArrayEntity byteArrayEntity = new ByteArrayEntity(serializedRequest);
            httpPost.setEntity(byteArrayEntity);

            try (CloseableHttpResponse response = httpClient.execute(httpPost))
            {
                // Inspect the status of the response

                StatusLine statusLine = response.getStatusLine();
                int responseCode = statusLine.getStatusCode();

                if (responseCode == HTTP_OK_RESPONSE)
                {
                    // Check that a response object has been provided

                    HttpEntity entity = response.getEntity();

                    if (entity.getContentLength() > 0)
                    {
                        byte[] serializedResponse = EntityUtils.toByteArray(entity);
                        return TimeSeriesResponse.deserialize(serializedResponse);
                    }
                    else
                    {
                        String message = "The platform returned a HTTP response but this did contain the expected data";
                        throw new Exception(message);
                    }
                }
                else
                {
                    String reason = statusLine.getReasonPhrase();
                    String message = String.format("The platform returned an unexpected HTTP response code (%s) and reason: \"%s\"; the response received was:\n\n%s", responseCode, reason, response);
                    throw new Exception(message);
                }
            }
        }
    }


    /**
     * Sends a time-series metadata request to the platform.
     *
     * @param request The request to send.
     * @return The response to the request.
     */
    public static TimeSeriesMetadataResponse send(TimeSeriesMetadataRequest request) throws Exception
    {
        try (CloseableHttpClient httpClient = HttpClients.createDefault())
        {
            // Check that an API Key has been provided

            if (request.getApiKey() == null)
            {
                request.setApiKey(Archipelagos.API_KEY);
            }

            if (request.getApiKey() == null)
            {
                throw new IllegalArgumentException("Neither request or Archipelagos contains an API Key");
            }


            // Obtain the URL and port to use when connecting to the platform

            String httpUrl = Resources.getSetting(SettingNames.HTTP_SERVER_URL);

            if (httpUrl == null)
            {
                throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_URL));
            }

            String httpPortStr = Resources.getSetting(SettingNames.HTTP_SERVER_PORT);
            int httpPort = -1;

            if (httpPortStr == null)
            {
                throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_PORT));
            }
            else
            {
                try
                {
                    httpPort = Integer.parseInt(httpPortStr);

                    if (httpPort < 1)
                    {
                        throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
                    }
                }
                catch (Exception e)
                {
                    throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
                }
            }


            // Build the URL for the request

            URIBuilder uriBuilder = new URIBuilder(httpUrl);
            uriBuilder.setPort(httpPort);
            uriBuilder.addParameter(HttpConstants.FORMAT_PARAMETER, HttpConstants.FORMAT_BINARY);
            uriBuilder.addParameter(HttpConstants.TYPE_PARAMETER, HttpConstants.TIME_SERIES_METADATA_REQUEST);


            // Build the http post and send

            HttpPost httpPost = new HttpPost(uriBuilder.build());
            byte[] serializedRequest = request.serialize();
            ByteArrayEntity byteArrayEntity = new ByteArrayEntity(serializedRequest);
            httpPost.setEntity(byteArrayEntity);

            try (CloseableHttpResponse response = httpClient.execute(httpPost))
            {
                // Inspect the status of the response

                StatusLine statusLine = response.getStatusLine();
                int responseCode = statusLine.getStatusCode();

                if (responseCode == HTTP_OK_RESPONSE)
                {
                    // Check that a response object has been provided

                    HttpEntity entity = response.getEntity();

                    if (entity.getContentLength() > 0)
                    {
                        byte[] serializedResponse = EntityUtils.toByteArray(entity);
                        return TimeSeriesMetadataResponse.deserialize(serializedResponse);
                    }
                    else
                    {
                        String message = "The platform returned a HTTP response but this did contain the expected data";
                        throw new Exception(message);
                    }
                }
                else
                {
                    String reason = statusLine.getReasonPhrase();
                    String message = String.format("The platform returned an unexpected HTTP response code (%s) and reason: \"%s\"; the response received was:\n\n%s", responseCode, reason, response);
                    throw new Exception(message);
                }
            }
        }
    }


    /**
     * Sends a collection request to the platform.
     *
     * @param request The request to send.
     * @return The response to the request.
     */
    public static CollectionResponse send(CollectionRequest request) throws Exception
    {
        try (CloseableHttpClient httpClient = HttpClients.createDefault())
        {
            // Check that an API Key has been provided

            if (request.getApiKey() == null)
            {
                request.setApiKey(Archipelagos.API_KEY);
            }

            if (request.getApiKey() == null)
            {
                throw new IllegalArgumentException("Neither request or Archipelagos contains an API Key");
            }


            // Obtain the URL and port to use when connecting to the platform

            String httpUrl = Resources.getSetting(SettingNames.HTTP_SERVER_URL);

            if (httpUrl == null)
            {
                throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_URL));
            }

            String httpPortStr = Resources.getSetting(SettingNames.HTTP_SERVER_PORT);
            int httpPort = -1;

            if (httpPortStr == null)
            {
                throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_PORT));
            }
            else
            {
                try
                {
                    httpPort = Integer.parseInt(httpPortStr);

                    if (httpPort < 1)
                    {
                        throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
                    }
                }
                catch (Exception e)
                {
                    throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
                }
            }


            // Build the URL for the request

            URIBuilder uriBuilder = new URIBuilder(httpUrl);
            uriBuilder.setPort(httpPort);
            uriBuilder.addParameter(HttpConstants.FORMAT_PARAMETER, HttpConstants.FORMAT_BINARY);
            uriBuilder.addParameter(HttpConstants.TYPE_PARAMETER, HttpConstants.COLLECTION_REQUEST);


            // Build the http post and send

            HttpPost httpPost = new HttpPost(uriBuilder.build());
            byte[] serializedRequest = request.serialize();
            ByteArrayEntity byteArrayEntity = new ByteArrayEntity(serializedRequest);
            httpPost.setEntity(byteArrayEntity);

            try (CloseableHttpResponse response = httpClient.execute(httpPost))
            {
                // Inspect the status of the response

                StatusLine statusLine = response.getStatusLine();
                int responseCode = statusLine.getStatusCode();

                if (responseCode == HTTP_OK_RESPONSE)
                {
                    // Check that a response object has been provided

                    HttpEntity entity = response.getEntity();

                    if (entity.getContentLength() > 0)
                    {
                        byte[] serializedResponse = EntityUtils.toByteArray(entity);
                        return CollectionResponse.deserialize(serializedResponse);
                    }
                    else
                    {
                        String message = "The platform returned a HTTP response but this did contain the expected data";
                        throw new Exception(message);
                    }
                }
                else
                {
                    String reason = statusLine.getReasonPhrase();
                    String message = String.format("The platform returned an unexpected HTTP response code (%s) and reason: \"%s\"; the response received was:\n\n%s", responseCode, reason, response);
                    throw new Exception(message);
                }
            }
        }
    }


    /**
     * Sends a collection metadata request to the platform.
     *
     * @param request The request to send.
     * @return The response to the request.
     */
    public static CollectionMetadataResponse send(CollectionMetadataRequest request) throws Exception
    {
        try (CloseableHttpClient httpClient = HttpClients.createDefault())
        {
            // Check that an API Key has been provided

            if (request.getApiKey() == null)
            {
                request.setApiKey(Archipelagos.API_KEY);
            }

            if (request.getApiKey() == null)
            {
                throw new IllegalArgumentException("Neither request or Archipelagos contains an API Key");
            }


            // Obtain the URL and port to use when connecting to the platform

            String httpUrl = Resources.getSetting(SettingNames.HTTP_SERVER_URL);

            if (httpUrl == null)
            {
                throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_URL));
            }

            String httpPortStr = Resources.getSetting(SettingNames.HTTP_SERVER_PORT);
            int httpPort = -1;

            if (httpPortStr == null)
            {
                throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_PORT));
            }
            else
            {
                try
                {
                    httpPort = Integer.parseInt(httpPortStr);

                    if (httpPort < 1)
                    {
                        throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
                    }
                }
                catch (Exception e)
                {
                    throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
                }
            }


            // Build the URL for the request

            URIBuilder uriBuilder = new URIBuilder(httpUrl);
            uriBuilder.setPort(httpPort);
            uriBuilder.addParameter(HttpConstants.FORMAT_PARAMETER, HttpConstants.FORMAT_BINARY);
            uriBuilder.addParameter(HttpConstants.TYPE_PARAMETER, HttpConstants.COLLECTION_METADATA_REQUEST);


            // Build the http post and send

            HttpPost httpPost = new HttpPost(uriBuilder.build());
            byte[] serializedRequest = request.serialize();
            ByteArrayEntity byteArrayEntity = new ByteArrayEntity(serializedRequest);
            httpPost.setEntity(byteArrayEntity);

            try (CloseableHttpResponse response = httpClient.execute(httpPost))
            {
                // Inspect the status of the response

                StatusLine statusLine = response.getStatusLine();
                int responseCode = statusLine.getStatusCode();

                if (responseCode == HTTP_OK_RESPONSE)
                {
                    // Check that a response object has been provided

                    HttpEntity entity = response.getEntity();

                    if (entity.getContentLength() > 0)
                    {
                        byte[] serializedResponse = EntityUtils.toByteArray(entity);
                        return CollectionMetadataResponse.deserialize(serializedResponse);
                    }
                    else
                    {
                        String message = "The platform returned a HTTP response but this did contain the expected data";
                        throw new Exception(message);
                    }
                }
                else
                {
                    String reason = statusLine.getReasonPhrase();
                    String message = String.format("The platform returned an unexpected HTTP response code (%s) and reason: \"%s\"; the response received was:\n\n%s", responseCode, reason, response);
                    throw new Exception(message);
                }
            }
        }
    }


    /**
     * Sends a file store request to the platform.
     *
     * @param request The request to send.
     * @return The response to the request.
     */
    public static FileStoreResponse send(FileStoreRequest request) throws Exception
    {
        try (CloseableHttpClient httpClient = HttpClients.createDefault())
        {
            // Check that an API Key has been provided

            if (request.getApiKey() == null)
            {
                request.setApiKey(Archipelagos.API_KEY);
            }

            if (request.getApiKey() == null)
            {
                throw new IllegalArgumentException("Neither request or Archipelagos contains an API Key");
            }


            // Obtain the URL and port to use when connecting to the platform

            String httpUrl = Resources.getSetting(SettingNames.HTTP_SERVER_URL);

            if (httpUrl == null)
            {
                throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_URL));
            }

            String httpPortStr = Resources.getSetting(SettingNames.HTTP_SERVER_PORT);
            int httpPort = -1;

            if (httpPortStr == null)
            {
                throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_PORT));
            }
            else
            {
                try
                {
                    httpPort = Integer.parseInt(httpPortStr);

                    if (httpPort < 1)
                    {
                        throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
                    }
                }
                catch (Exception e)
                {
                    throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
                }
            }


            // Build the URL for the request

            URIBuilder uriBuilder = new URIBuilder(httpUrl);
            uriBuilder.setPort(httpPort);
            uriBuilder.addParameter(HttpConstants.FORMAT_PARAMETER, HttpConstants.FORMAT_BINARY);
            uriBuilder.addParameter(HttpConstants.TYPE_PARAMETER, HttpConstants.FILE_STORE_REQUEST);


            // Build the http post and send

            HttpPost httpPost = new HttpPost(uriBuilder.build());
            byte[] serializedRequest = request.serialize();
            ByteArrayEntity byteArrayEntity = new ByteArrayEntity(serializedRequest);
            httpPost.setEntity(byteArrayEntity);

            try (CloseableHttpResponse response = httpClient.execute(httpPost))
            {
                // Inspect the status of the response

                StatusLine statusLine = response.getStatusLine();
                int responseCode = statusLine.getStatusCode();

                if (responseCode == HTTP_OK_RESPONSE)
                {
                    // Check that a response object has been provided

                    HttpEntity entity = response.getEntity();

                    if (entity.getContentLength() > 0)
                    {
                        byte[] serializedResponse = EntityUtils.toByteArray(entity);
                        return FileStoreResponse.deserialize(serializedResponse);
                    }
                    else
                    {
                        String message = "The platform returned a HTTP response but this did contain the expected data";
                        throw new Exception(message);
                    }
                }
                else
                {
                    String reason = statusLine.getReasonPhrase();
                    String message = String.format("The platform returned an unexpected HTTP response code (%s) and reason: \"%s\"; the response received was:\n\n%s", responseCode, reason, response);
                    throw new Exception(message);
                }
            }
        }
    }


    /**
     * Sends a file store metadata request to the platform.
     *
     * @param request The request to send.
     * @return The response to the request.
     */
    public static FileStoreMetadataResponse send(FileStoreMetadataRequest request) throws Exception
    {
        try (CloseableHttpClient httpClient = HttpClients.createDefault())
        {
            // Check that an API Key has been provided

            if (request.getApiKey() == null)
            {
                request.setApiKey(Archipelagos.API_KEY);
            }

            if (request.getApiKey() == null)
            {
                throw new IllegalArgumentException("Neither request or Archipelagos contains an API Key");
            }


            // Obtain the URL and port to use when connecting to the platform

            String httpUrl = Resources.getSetting(SettingNames.HTTP_SERVER_URL);

            if (httpUrl == null)
            {
                throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_URL));
            }

            String httpPortStr = Resources.getSetting(SettingNames.HTTP_SERVER_PORT);
            int httpPort = -1;

            if (httpPortStr == null)
            {
                throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_PORT));
            }
            else
            {
                try
                {
                    httpPort = Integer.parseInt(httpPortStr);

                    if (httpPort < 1)
                    {
                        throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
                    }
                }
                catch (Exception e)
                {
                    throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
                }
            }


            // Build the URL for the request

            URIBuilder uriBuilder = new URIBuilder(httpUrl);
            uriBuilder.setPort(httpPort);
            uriBuilder.addParameter(HttpConstants.FORMAT_PARAMETER, HttpConstants.FORMAT_BINARY);
            uriBuilder.addParameter(HttpConstants.TYPE_PARAMETER, HttpConstants.FILE_STORE_METADATA_REQUEST);


            // Build the http post and send

            HttpPost httpPost = new HttpPost(uriBuilder.build());
            byte[] serializedRequest = request.serialize();
            ByteArrayEntity byteArrayEntity = new ByteArrayEntity(serializedRequest);
            httpPost.setEntity(byteArrayEntity);

            try (CloseableHttpResponse response = httpClient.execute(httpPost))
            {
                // Inspect the status of the response

                StatusLine statusLine = response.getStatusLine();
                int responseCode = statusLine.getStatusCode();

                if (responseCode == HTTP_OK_RESPONSE)
                {
                    // Check that a response object has been provided

                    HttpEntity entity = response.getEntity();

                    if (entity.getContentLength() > 0)
                    {
                        byte[] serializedResponse = EntityUtils.toByteArray(entity);
                        return FileStoreMetadataResponse.deserialize(serializedResponse);
                    }
                    else
                    {
                        String message = "The platform returned a HTTP response but this did contain the expected data";
                        throw new Exception(message);
                    }
                }
                else
                {
                    String reason = statusLine.getReasonPhrase();
                    String message = String.format("The platform returned an unexpected HTTP response code (%s) and reason: \"%s\"; the response received was:\n\n%s", responseCode, reason, response);
                    throw new Exception(message);
                }
            }
        }
    }


    /**
     * Sends a file metadata request to the platform.
     *
     * @param request The request to send.
     * @return The response to the request.
     */
    public static FileMetadataResponse send(FileMetadataRequest request) throws Exception
    {
        try (CloseableHttpClient httpClient = HttpClients.createDefault())
        {
            // Check that an API Key has been provided

            if (request.getApiKey() == null)
            {
                request.setApiKey(Archipelagos.API_KEY);
            }

            if (request.getApiKey() == null)
            {
                throw new IllegalArgumentException("Neither request or Archipelagos contains an API Key");
            }


            // Obtain the URL and port to use when connecting to the platform

            String httpUrl = Resources.getSetting(SettingNames.HTTP_SERVER_URL);

            if (httpUrl == null)
            {
                throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_URL));
            }

            String httpPortStr = Resources.getSetting(SettingNames.HTTP_SERVER_PORT);
            int httpPort = -1;

            if (httpPortStr == null)
            {
                throw new IllegalArgumentException(String.format("No value has been provided for the configuration setting \"%s\"", SettingNames.HTTP_SERVER_PORT));
            }
            else
            {
                try
                {
                    httpPort = Integer.parseInt(httpPortStr);

                    if (httpPort < 1)
                    {
                        throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
                    }
                }
                catch (Exception e)
                {
                    throw new IllegalArgumentException(String.format("The value has been provided for the configuration setting \"%s\" is not a positive integer", SettingNames.HTTP_SERVER_PORT));
                }
            }


            // Build the URL for the request

            URIBuilder uriBuilder = new URIBuilder(httpUrl);
            uriBuilder.setPort(httpPort);
            uriBuilder.addParameter(HttpConstants.FORMAT_PARAMETER, HttpConstants.FORMAT_BINARY);
            uriBuilder.addParameter(HttpConstants.TYPE_PARAMETER, HttpConstants.FILE_METADATA_REQUEST);


            // Build the http post and send

            HttpPost httpPost = new HttpPost(uriBuilder.build());
            byte[] serializedRequest = request.serialize();
            ByteArrayEntity byteArrayEntity = new ByteArrayEntity(serializedRequest);
            httpPost.setEntity(byteArrayEntity);

            try (CloseableHttpResponse response = httpClient.execute(httpPost))
            {
                // Inspect the status of the response

                StatusLine statusLine = response.getStatusLine();
                int responseCode = statusLine.getStatusCode();

                if (responseCode == HTTP_OK_RESPONSE)
                {
                    // Check that a response object has been provided

                    HttpEntity entity = response.getEntity();

                    if (entity.getContentLength() > 0)
                    {
                        byte[] serializedResponse = EntityUtils.toByteArray(entity);
                        return FileMetadataResponse.deserialize(serializedResponse);
                    }
                    else
                    {
                        String message = "The platform returned a HTTP response but this did contain the expected data";
                        throw new Exception(message);
                    }
                }
                else
                {
                    String reason = statusLine.getReasonPhrase();
                    String message = String.format("The platform returned an unexpected HTTP response code (%s) and reason: \"%s\"; the response received was:\n\n%s", responseCode, reason, response);
                    throw new Exception(message);
                }
            }
        }
    }
}
