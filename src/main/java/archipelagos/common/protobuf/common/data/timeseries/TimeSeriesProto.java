// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: archipelagos/common/protobuf/common/data/timeseries/TimeSeries.proto

package archipelagos.common.protobuf.common.data.timeseries;

public final class TimeSeriesProto {
  private TimeSeriesProto() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface TimeSeriesOrBuilder extends
      // @@protoc_insertion_point(interface_extends:archipelagos.common.protobuf.common.data.timeseries.TimeSeries)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadata metadata = 1;</code>
     * @return Whether the metadata field is set.
     */
    boolean hasMetadata();
    /**
     * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadata metadata = 1;</code>
     * @return The metadata.
     */
    archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata getMetadata();
    /**
     * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadata metadata = 1;</code>
     */
    archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadataOrBuilder getMetadataOrBuilder();

    /**
     * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesData data = 2;</code>
     * @return Whether the data field is set.
     */
    boolean hasData();
    /**
     * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesData data = 2;</code>
     * @return The data.
     */
    archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData getData();
    /**
     * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesData data = 2;</code>
     */
    archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesDataOrBuilder getDataOrBuilder();
  }
  /**
   * Protobuf type {@code archipelagos.common.protobuf.common.data.timeseries.TimeSeries}
   */
  public static final class TimeSeries extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:archipelagos.common.protobuf.common.data.timeseries.TimeSeries)
      TimeSeriesOrBuilder {
  private static final long serialVersionUID = 0L;
    // Use TimeSeries.newBuilder() to construct.
    private TimeSeries(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private TimeSeries() {
    }

    @java.lang.Override
    @SuppressWarnings({"unused"})
    protected java.lang.Object newInstance(
        UnusedPrivateParameter unused) {
      return new TimeSeries();
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return this.unknownFields;
    }
    private TimeSeries(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata.Builder subBuilder = null;
              if (metadata_ != null) {
                subBuilder = metadata_.toBuilder();
              }
              metadata_ = input.readMessage(archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata.parser(), extensionRegistry);
              if (subBuilder != null) {
                subBuilder.mergeFrom(metadata_);
                metadata_ = subBuilder.buildPartial();
              }

              break;
            }
            case 18: {
              archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData.Builder subBuilder = null;
              if (data_ != null) {
                subBuilder = data_.toBuilder();
              }
              data_ = input.readMessage(archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData.parser(), extensionRegistry);
              if (subBuilder != null) {
                subBuilder.mergeFrom(data_);
                data_ = subBuilder.buildPartial();
              }

              break;
            }
            default: {
              if (!parseUnknownField(
                  input, unknownFields, extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.internal_static_archipelagos_common_protobuf_common_data_timeseries_TimeSeries_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.internal_static_archipelagos_common_protobuf_common_data_timeseries_TimeSeries_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries.class, archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries.Builder.class);
    }

    public static final int METADATA_FIELD_NUMBER = 1;
    private archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata metadata_;
    /**
     * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadata metadata = 1;</code>
     * @return Whether the metadata field is set.
     */
    @java.lang.Override
    public boolean hasMetadata() {
      return metadata_ != null;
    }
    /**
     * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadata metadata = 1;</code>
     * @return The metadata.
     */
    @java.lang.Override
    public archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata getMetadata() {
      return metadata_ == null ? archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata.getDefaultInstance() : metadata_;
    }
    /**
     * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadata metadata = 1;</code>
     */
    @java.lang.Override
    public archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadataOrBuilder getMetadataOrBuilder() {
      return getMetadata();
    }

    public static final int DATA_FIELD_NUMBER = 2;
    private archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData data_;
    /**
     * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesData data = 2;</code>
     * @return Whether the data field is set.
     */
    @java.lang.Override
    public boolean hasData() {
      return data_ != null;
    }
    /**
     * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesData data = 2;</code>
     * @return The data.
     */
    @java.lang.Override
    public archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData getData() {
      return data_ == null ? archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData.getDefaultInstance() : data_;
    }
    /**
     * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesData data = 2;</code>
     */
    @java.lang.Override
    public archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesDataOrBuilder getDataOrBuilder() {
      return getData();
    }

    private byte memoizedIsInitialized = -1;
    @java.lang.Override
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    @java.lang.Override
    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (metadata_ != null) {
        output.writeMessage(1, getMetadata());
      }
      if (data_ != null) {
        output.writeMessage(2, getData());
      }
      unknownFields.writeTo(output);
    }

    @java.lang.Override
    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (metadata_ != null) {
        size += com.google.protobuf.CodedOutputStream
          .computeMessageSize(1, getMetadata());
      }
      if (data_ != null) {
        size += com.google.protobuf.CodedOutputStream
          .computeMessageSize(2, getData());
      }
      size += unknownFields.getSerializedSize();
      memoizedSize = size;
      return size;
    }

    @java.lang.Override
    public boolean equals(final java.lang.Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries)) {
        return super.equals(obj);
      }
      archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries other = (archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries) obj;

      if (hasMetadata() != other.hasMetadata()) return false;
      if (hasMetadata()) {
        if (!getMetadata()
            .equals(other.getMetadata())) return false;
      }
      if (hasData() != other.hasData()) return false;
      if (hasData()) {
        if (!getData()
            .equals(other.getData())) return false;
      }
      if (!unknownFields.equals(other.unknownFields)) return false;
      return true;
    }

    @java.lang.Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptor().hashCode();
      if (hasMetadata()) {
        hash = (37 * hash) + METADATA_FIELD_NUMBER;
        hash = (53 * hash) + getMetadata().hashCode();
      }
      if (hasData()) {
        hash = (37 * hash) + DATA_FIELD_NUMBER;
        hash = (53 * hash) + getData().hashCode();
      }
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries parseFrom(
        java.nio.ByteBuffer data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries parseFrom(
        java.nio.ByteBuffer data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    @java.lang.Override
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    @java.lang.Override
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code archipelagos.common.protobuf.common.data.timeseries.TimeSeries}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:archipelagos.common.protobuf.common.data.timeseries.TimeSeries)
        archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeriesOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.internal_static_archipelagos_common_protobuf_common_data_timeseries_TimeSeries_descriptor;
      }

      @java.lang.Override
      protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.internal_static_archipelagos_common_protobuf_common_data_timeseries_TimeSeries_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries.class, archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries.Builder.class);
      }

      // Construct using archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      @java.lang.Override
      public Builder clear() {
        super.clear();
        if (metadataBuilder_ == null) {
          metadata_ = null;
        } else {
          metadata_ = null;
          metadataBuilder_ = null;
        }
        if (dataBuilder_ == null) {
          data_ = null;
        } else {
          data_ = null;
          dataBuilder_ = null;
        }
        return this;
      }

      @java.lang.Override
      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.internal_static_archipelagos_common_protobuf_common_data_timeseries_TimeSeries_descriptor;
      }

      @java.lang.Override
      public archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries getDefaultInstanceForType() {
        return archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries.getDefaultInstance();
      }

      @java.lang.Override
      public archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries build() {
        archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      @java.lang.Override
      public archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries buildPartial() {
        archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries result = new archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries(this);
        if (metadataBuilder_ == null) {
          result.metadata_ = metadata_;
        } else {
          result.metadata_ = metadataBuilder_.build();
        }
        if (dataBuilder_ == null) {
          result.data_ = data_;
        } else {
          result.data_ = dataBuilder_.build();
        }
        onBuilt();
        return result;
      }

      @java.lang.Override
      public Builder clone() {
        return super.clone();
      }
      @java.lang.Override
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return super.setField(field, value);
      }
      @java.lang.Override
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return super.clearField(field);
      }
      @java.lang.Override
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return super.clearOneof(oneof);
      }
      @java.lang.Override
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, java.lang.Object value) {
        return super.setRepeatedField(field, index, value);
      }
      @java.lang.Override
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return super.addRepeatedField(field, value);
      }
      @java.lang.Override
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries) {
          return mergeFrom((archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries other) {
        if (other == archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries.getDefaultInstance()) return this;
        if (other.hasMetadata()) {
          mergeMetadata(other.getMetadata());
        }
        if (other.hasData()) {
          mergeData(other.getData());
        }
        this.mergeUnknownFields(other.unknownFields);
        onChanged();
        return this;
      }

      @java.lang.Override
      public final boolean isInitialized() {
        return true;
      }

      @java.lang.Override
      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata metadata_;
      private com.google.protobuf.SingleFieldBuilderV3<
          archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata, archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata.Builder, archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadataOrBuilder> metadataBuilder_;
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadata metadata = 1;</code>
       * @return Whether the metadata field is set.
       */
      public boolean hasMetadata() {
        return metadataBuilder_ != null || metadata_ != null;
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadata metadata = 1;</code>
       * @return The metadata.
       */
      public archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata getMetadata() {
        if (metadataBuilder_ == null) {
          return metadata_ == null ? archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata.getDefaultInstance() : metadata_;
        } else {
          return metadataBuilder_.getMessage();
        }
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadata metadata = 1;</code>
       */
      public Builder setMetadata(archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata value) {
        if (metadataBuilder_ == null) {
          if (value == null) {
            throw new NullPointerException();
          }
          metadata_ = value;
          onChanged();
        } else {
          metadataBuilder_.setMessage(value);
        }

        return this;
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadata metadata = 1;</code>
       */
      public Builder setMetadata(
          archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata.Builder builderForValue) {
        if (metadataBuilder_ == null) {
          metadata_ = builderForValue.build();
          onChanged();
        } else {
          metadataBuilder_.setMessage(builderForValue.build());
        }

        return this;
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadata metadata = 1;</code>
       */
      public Builder mergeMetadata(archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata value) {
        if (metadataBuilder_ == null) {
          if (metadata_ != null) {
            metadata_ =
              archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata.newBuilder(metadata_).mergeFrom(value).buildPartial();
          } else {
            metadata_ = value;
          }
          onChanged();
        } else {
          metadataBuilder_.mergeFrom(value);
        }

        return this;
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadata metadata = 1;</code>
       */
      public Builder clearMetadata() {
        if (metadataBuilder_ == null) {
          metadata_ = null;
          onChanged();
        } else {
          metadata_ = null;
          metadataBuilder_ = null;
        }

        return this;
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadata metadata = 1;</code>
       */
      public archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata.Builder getMetadataBuilder() {
        
        onChanged();
        return getMetadataFieldBuilder().getBuilder();
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadata metadata = 1;</code>
       */
      public archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadataOrBuilder getMetadataOrBuilder() {
        if (metadataBuilder_ != null) {
          return metadataBuilder_.getMessageOrBuilder();
        } else {
          return metadata_ == null ?
              archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata.getDefaultInstance() : metadata_;
        }
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadata metadata = 1;</code>
       */
      private com.google.protobuf.SingleFieldBuilderV3<
          archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata, archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata.Builder, archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadataOrBuilder> 
          getMetadataFieldBuilder() {
        if (metadataBuilder_ == null) {
          metadataBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
              archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata, archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadata.Builder, archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.TimeSeriesMetadataOrBuilder>(
                  getMetadata(),
                  getParentForChildren(),
                  isClean());
          metadata_ = null;
        }
        return metadataBuilder_;
      }

      private archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData data_;
      private com.google.protobuf.SingleFieldBuilderV3<
          archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData, archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData.Builder, archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesDataOrBuilder> dataBuilder_;
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesData data = 2;</code>
       * @return Whether the data field is set.
       */
      public boolean hasData() {
        return dataBuilder_ != null || data_ != null;
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesData data = 2;</code>
       * @return The data.
       */
      public archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData getData() {
        if (dataBuilder_ == null) {
          return data_ == null ? archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData.getDefaultInstance() : data_;
        } else {
          return dataBuilder_.getMessage();
        }
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesData data = 2;</code>
       */
      public Builder setData(archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData value) {
        if (dataBuilder_ == null) {
          if (value == null) {
            throw new NullPointerException();
          }
          data_ = value;
          onChanged();
        } else {
          dataBuilder_.setMessage(value);
        }

        return this;
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesData data = 2;</code>
       */
      public Builder setData(
          archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData.Builder builderForValue) {
        if (dataBuilder_ == null) {
          data_ = builderForValue.build();
          onChanged();
        } else {
          dataBuilder_.setMessage(builderForValue.build());
        }

        return this;
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesData data = 2;</code>
       */
      public Builder mergeData(archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData value) {
        if (dataBuilder_ == null) {
          if (data_ != null) {
            data_ =
              archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData.newBuilder(data_).mergeFrom(value).buildPartial();
          } else {
            data_ = value;
          }
          onChanged();
        } else {
          dataBuilder_.mergeFrom(value);
        }

        return this;
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesData data = 2;</code>
       */
      public Builder clearData() {
        if (dataBuilder_ == null) {
          data_ = null;
          onChanged();
        } else {
          data_ = null;
          dataBuilder_ = null;
        }

        return this;
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesData data = 2;</code>
       */
      public archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData.Builder getDataBuilder() {
        
        onChanged();
        return getDataFieldBuilder().getBuilder();
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesData data = 2;</code>
       */
      public archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesDataOrBuilder getDataOrBuilder() {
        if (dataBuilder_ != null) {
          return dataBuilder_.getMessageOrBuilder();
        } else {
          return data_ == null ?
              archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData.getDefaultInstance() : data_;
        }
      }
      /**
       * <code>.archipelagos.common.protobuf.common.data.timeseries.TimeSeriesData data = 2;</code>
       */
      private com.google.protobuf.SingleFieldBuilderV3<
          archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData, archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData.Builder, archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesDataOrBuilder> 
          getDataFieldBuilder() {
        if (dataBuilder_ == null) {
          dataBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
              archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData, archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesData.Builder, archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.TimeSeriesDataOrBuilder>(
                  getData(),
                  getParentForChildren(),
                  isClean());
          data_ = null;
        }
        return dataBuilder_;
      }
      @java.lang.Override
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.setUnknownFields(unknownFields);
      }

      @java.lang.Override
      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.mergeUnknownFields(unknownFields);
      }


      // @@protoc_insertion_point(builder_scope:archipelagos.common.protobuf.common.data.timeseries.TimeSeries)
    }

    // @@protoc_insertion_point(class_scope:archipelagos.common.protobuf.common.data.timeseries.TimeSeries)
    private static final archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries();
    }

    public static archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<TimeSeries>
        PARSER = new com.google.protobuf.AbstractParser<TimeSeries>() {
      @java.lang.Override
      public TimeSeries parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new TimeSeries(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<TimeSeries> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<TimeSeries> getParserForType() {
      return PARSER;
    }

    @java.lang.Override
    public archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto.TimeSeries getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_archipelagos_common_protobuf_common_data_timeseries_TimeSeries_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_archipelagos_common_protobuf_common_data_timeseries_TimeSeries_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\nDarchipelagos/common/protobuf/common/da" +
      "ta/timeseries/TimeSeries.proto\0223archipel" +
      "agos.common.protobuf.common.data.timeser" +
      "ies\032Larchipelagos/common/protobuf/common" +
      "/data/timeseries/TimeSeriesMetadata.prot" +
      "o\032Harchipelagos/common/protobuf/common/d" +
      "ata/timeseries/TimeSeriesData.proto\"\272\001\n\n" +
      "TimeSeries\022Y\n\010metadata\030\001 \001(\0132G.archipela" +
      "gos.common.protobuf.common.data.timeseri" +
      "es.TimeSeriesMetadata\022Q\n\004data\030\002 \001(\0132C.ar" +
      "chipelagos.common.protobuf.common.data.t" +
      "imeseries.TimeSeriesDataBF\n3archipelagos" +
      ".common.protobuf.common.data.timeseriesB" +
      "\017TimeSeriesProtob\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.getDescriptor(),
          archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.getDescriptor(),
        });
    internal_static_archipelagos_common_protobuf_common_data_timeseries_TimeSeries_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_archipelagos_common_protobuf_common_data_timeseries_TimeSeries_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_archipelagos_common_protobuf_common_data_timeseries_TimeSeries_descriptor,
        new java.lang.String[] { "Metadata", "Data", });
    archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto.getDescriptor();
    archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
