package archipelagos.common.settings;

/**
 * The names of settings in a settings file.
 */
public interface SettingNames
{
	// Settings related to the HTTP server

	String HTTP_SERVER_URL = "http.server.url";
	String HTTP_SERVER_PORT = "http.server.port";
}
