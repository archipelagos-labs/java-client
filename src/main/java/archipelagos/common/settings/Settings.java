package archipelagos.common.settings;

import java.util.Map;

/**
 * Settings contained in a settings file.
 */
public final class Settings
{
	private final SettingsFile _settingsFile;
	private final Map<String, String> _settings;


	/**
	 * @param settingsFile The file associated with the settings.
	 * @param settings The settings associated with this.
	 */
	Settings(SettingsFile settingsFile, Map<String, String> settings)
	{
		_settingsFile = settingsFile;
		_settings = settings;
	}


	/**
	 * @return The file associated with the settings.
	 */
	public SettingsFile getConfigurationFile()
	{
		return _settingsFile;
	}


	/**
	 * @return The settings associated with this.
	 */
	public Map<String, String> getSettings()
	{
		return _settings;
	}


	/**
	 * @param setting The setting.
	 * @return True if this contains a specified setting, false otherwise.
	 */
	public boolean hasSetting(String setting)
	{
		return _settings.containsKey(setting);
	}


	/**
	 * @param setting The setting.
	 * @return If hasSetting(setting) the value of a specified setting, null otherwise.
	 */
	public String getSetting(String setting)
	{
		return _settings.get(setting);
	}
}
