package archipelagos.common.settings;

import java.io.*;
import java.util.*;

/**
 * Deals with finding and parsing configuration files for system components.
 */
public final class SettingsFile
{
	private final String _filePath;
	private final Settings _settings;


	/**
	 * @param filePath The full path of the configuration file.
	 * @throws Exception if an unexpected issue occurred when reading or parsing the file (e.g. user does not have permission).
	 */
	private SettingsFile(String filePath) throws Exception
	{
		_filePath = filePath;


		// Read in the file and store its contents

		List<String> lines = new LinkedList<>();

		try (FileReader fileReader = new FileReader(filePath))
		{
			try (BufferedReader bufferedReader = new BufferedReader(fileReader))
			{
				String line;

				do
				{
					line = bufferedReader.readLine();

					if( line != null)
					{
						lines.add(line);
					}
				}
				while (line != null);
			}
		}


		// Parse the contents of the file

		Map<String, String> settings = new HashMap<>();
		int lineCount = 0;

		for (String line : lines)
		{
			++lineCount;

			if (line != null)
			{
				line = line.trim();


				// Check that the line is not empty and is not a comment (which start with #)

				if (!"".equals(line) && !line.startsWith("#"))
				{
					int index = line.indexOf("=");

					if (index == -1)
					{
						throw new IllegalArgumentException("Line number " + lineCount + " contains a line that is not empty and does not contain a valid setting.");
					}
					else
					{
						// Split the line into a setting and it's value

						String setting = line.substring(0, index);
						String value = line.substring(index + 1);


						// Check if we already have this setting

						if( settings.containsKey(setting))
						{
							throw new IllegalArgumentException("Line number " + lineCount + " contains a duplicate setting for \"" + setting + "\"");
						}
						else
						{
							settings.put(setting, value);
						}
					}
				}
			}
		}

		_settings = new Settings(this, settings);
	}


	/**
	 * @return The full file path of the configuration file.
	 */
	public String getFilePath()
	{
		return _filePath;
	}


	/**
	 * @return The settings contained in the configuration file.
	 */
	public Settings getSettings()
	{
		return _settings;
	}


	/**
	 * Used to locate the configuration file in a list of files contained within a directory.
	 *
	 * @param configurationFilename The name of the configuration file.
	 * @param filesInDirectory The files in the directory.
	 * @return The file, or null if it could not be found.
	 */
	private static File locate(String configurationFilename, File[] filesInDirectory)
	{
		File toReturn = null;

		for (int i = 0; i < filesInDirectory.length; i++)
		{
			File file = filesInDirectory[i];

			if (file.isFile())
			{
				String filename = file.getName();

				if( configurationFilename.equals(filename))
				{
					toReturn = file;
					break;
				}
			}
			else if (file.isDirectory())
			{
				toReturn = locate(configurationFilename, file.listFiles());

				if( toReturn != null)
				{
					break;
				}
			}
		}

		return toReturn;
	}


	/**
	 * Used to locate the configuration file for a system component.
	 *
	 * @param configurationFilename The name of the configuration file.
	 * @param searchDirectory The directory in which the search should start.
	 * @return The configuration file or null if it could not be found.
	 */
	public static SettingsFile locate(String configurationFilename, String searchDirectory)
	{
		File searchDirectoryFile = new File(searchDirectory);
		File[] filesInSearchDirectory = searchDirectoryFile.listFiles();
		SettingsFile toReturn = null;

		if (filesInSearchDirectory != null)
		{
			File configurationFile = locate(configurationFilename, filesInSearchDirectory);

			if (configurationFile != null)
			{
				try
				{
					toReturn = new SettingsFile(configurationFile.getAbsolutePath());
				}
				catch(Exception e)
				{
				}
			}
		}

		return toReturn;
	}
}
