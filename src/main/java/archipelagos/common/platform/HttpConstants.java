package archipelagos.common.platform;

/**
 * Constants related to HTTP requests/responses.
 */
public interface HttpConstants
{
   String FORMAT_BINARY = "bin";
   String FORMAT_JSON = "json";
   String FORMAT_XML = "xml";
   String FORMAT_CSV = "csv";

   String TYPE_PARAMETER = "type";
   String SOURCE_PARAMETER = "source";
   String CATEGORY_PARAMETER = "category";
   String LABEL_PARAMETER = "label";
   String API_KEY_PARAMETER = "api_key";
   String START_PARAMETER = "start";
   String END_PARAMETER = "end";
   String FEATURES_PARAMETER = "features";
   String MAX_SIZE_PARAMETER = "max_size";
   String FORMAT_PARAMETER = "format";
   String FLATTEN_PARAMETER = "flatten";
   String ORDER_PARAMETER = "order";
   String FILENAME_PARAMETER = "filename";
   String PATTERN_PARAMETER = "pattern";
   String CLIENT_TYPE_PARAMETER = "archipelagos_client_type";

   String ORDER_ASCENDING = "asc";
   String ORDER_DESCENDING = "desc";

   String TIME_SERIES_REQUEST = "time-series";
   String TIME_SERIES_METADATA_REQUEST = "time-series-metadata";
   String COLLECTION_REQUEST = "collection";
   String COLLECTION_METADATA_REQUEST = "collection-metadata";
   String FILE_STORE_REQUEST = "file-store";
   String FILE_STORE_METADATA_REQUEST = "file-store-metadata";
   String FILE_METADATA_REQUEST = "file-metadata";
}
