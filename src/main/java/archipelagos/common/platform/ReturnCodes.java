package archipelagos.common.platform;

/**
 * The return codes used in the platform.
 */
public interface ReturnCodes
{
    int SUCCESS = 0;
    int UNEXPECTED_SYSTEM_ERROR = 1;
    int UNRECOGNISED_USER = 2;
    int INSUFFICIENT_PERMISSIONS = 3;
    int UNRECOGNISED_TIME_SERIES = 4;
    int UNSPECIFIED_FORMAT = 5;
    int UNRECOGNISED_FORMAT = 6;
    int UNSPECIFIED_SOURCE = 7;
    int UNSPECIFIED_CATEGORY = 8;
    int UNSPECIFIED_LABEL = 9;
    int UNSPECIFIED_API_KEY = 10;
    int INVALID_MAX_SIZE = 11;
    int INVALID_START = 12;
    int INVALID_END = 13;
    int INVALID_FEATURES = 14;
    int INVALID_FLATTEN = 15;
    int INVALID_TYPE = 16;
    int INVALID_ORDER = 17;
    int TOO_MANY_CONCURRENT_REQUESTS = 18;
    int TOO_MANY_REQUESTS = 19;
    int UNRECOGNISED_COLLECTION = 20;
    int INVALID_FILTERS = 21;
    int UNRECOGNISED_FILE_STORE = 22;
    int UNRECOGNISED_FILE = 23;
    int INVALID_PATTERN = 24;
    int INVALID_FILENAME = 25;
}
