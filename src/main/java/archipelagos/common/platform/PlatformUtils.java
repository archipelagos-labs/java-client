package archipelagos.common.platform;

import archipelagos.common.data.DataType;

/**
 * Static utility methods useful when interacting with the platform.
 */
public class PlatformUtils
{
	private final static String URL = "http://wwww.archipelagos-labs.com";


	/**
	 * Generates the website URL for a specified dataset.
	 *
	 * @param dataType The type of the data.
	 * @param source   The source for the data.
	 * @param category The category for the data.
	 * @param label    The label for the data.
	 * @return The URL.
	 */
	public static String getUrl(DataType dataType, String source, String category, String label)
	{
		return URL + "/" + dataType.toString().toLowerCase() + "/" + source + "/" + category+ "/" + label;
	}


	/**
	 * Rounds a value to a specified number of decimal values.
	 *
	 * @param value The value to round.
	 * @param decimalPlaces The number of decimal prices to around the value to(>= 0).
	 * @return The rounded value.
	 */
	public static double round(double value, int decimalPlaces)
	{
		double precision = Math.pow(10, decimalPlaces);

		if( value >= 0)
		{
			return Math.floor(value * precision + .5) / precision;
		}
		else
		{
			return Math.round(value * precision) / precision;
		}
	}
}
