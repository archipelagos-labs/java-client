package archipelagos.common.platform;

/**
 * The types of Client supported in the platform.
 */
public class ClientType
{
    public static String PYTHON_CLIENT = "Python Client";
    public static String JAVA_CLIENT = "Java Client";
    public static String CSHARP_CLIENT = "C# Client";
    public static String HTTP_CLIENT = "HTTP Client";
    public static String HTTP_PYTHON_CLIENT = "HTTP Client (Python)";
    public static String HTTP_JAVA_CLIENT = "HTTP Client (Java)";
    public static String HTTP_CSHARP_CLIENT = "HTTP Client (C#)";
}
