package archipelagos.common.data;

/**
 * The names of common properties that may be present in metadata.
 */
public interface PropertyNames
{
	String LONGITUDE = "Longitude";
	String LATITUDE = "Latitude";
	String CRS = "Crs";
}
