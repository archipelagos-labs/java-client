package archipelagos.common.data;

import java.util.*;

/**
 * The different frequencies that data can occur.
 */
public enum Frequency
{
   NONE(0),
   ONE_MINUTE(1),
   FIVE_MINUTES(2),
   TEN_MINUTES(3),
   FIFTEEN_MINUTES(4),
   THIRTY_MINUTES(5),
   HOURLY(6),
   DAILY(7),
   WEEKLY(8),
   MONTHLY(9),
   QUARTERLY(10),
   ANNUAL(11),
   BIANNUAL(12),
   IRREGULAR(13);


   // Used to hold the unique IDs for instances of this type

   private static Map<Integer, Frequency> _idMappings;
   private final int _id;


   static
   {
      synchronized(Frequency.class)
      {
         initializeMappings();
      }
   }


   /**
    * @param id The ID for the enum value.
    */
   Frequency(int id)
   {
      _id = id;
   }


   /**
    * @param id The ID for the enum value.
    */
   public static Frequency fromId(int id)
   {
      initializeMappings();

      return _idMappings.get(id);
   }


   /**
    * @return The ID for the enum value.
    */
   public int getId()
   {
      return _id;
   }


   /**
    * Initialize the mappings from IDs to enum values.
    */
   private static void initializeMappings()
   {
      if (_idMappings == null)
      {
         _idMappings = new HashMap<>();
         Frequency[] frequencies = Frequency.values();

         for (Frequency frequency : frequencies)
         {
            int id = frequency.getId();

            if (_idMappings.containsKey(id))
            {
               Frequency current = _idMappings.get(id);

               if (current != frequency)
               {
                  throw new IllegalArgumentException("The ID \"" + id + "\" has been used more than once to identify a value within the enum " + Frequency.class.getSimpleName());
               }
            }
            else
            {
               _idMappings.put(id, frequency);
            }
         }
      }
   }
}
