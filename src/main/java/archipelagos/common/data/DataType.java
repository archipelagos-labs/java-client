package archipelagos.common.data;

/**
 * The different types of data that may be stored in the platform.
 */
public enum DataType
{
	TIME_SERIES,
	COLLECTION,
	FILE_STORE
}
