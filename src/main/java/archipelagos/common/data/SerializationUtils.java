package archipelagos.common.data;

import archipelagos.common.protobuf.common.data.AtomicValueProto;
import archipelagos.common.protobuf.common.data.DataEntryProto;
import com.google.protobuf.ByteString;
import com.google.protobuf.Descriptors;

import java.time.Instant;
import java.util.*;

/**
 * Provides a set of static utility methods of use when .
 */
public class SerializationUtils
{
	private static final String IS_NULL_FIELD_NAME = "isNull";
	private static final String DATE_FIELD_NAME = "dateValue";


	/**
	 * Used to obtain the Protobuf object to represent a feature value that is of an atomic data type.
	 *
	 * @param featureValue The feature value.
	 * @return The Protobuf object.
	 */
	private static AtomicValueProto.AtomicValue getAtomicFeatureValue(Object featureValue)
	{
		if (featureValue == null)
		{
			return AtomicValueProto.AtomicValue.newBuilder().setIsNull(true).build();
		}
		else
		{
			if (featureValue instanceof String)
			{
				return AtomicValueProto.AtomicValue.newBuilder().setStringValue((String) featureValue).build();
			}
			else if (featureValue instanceof Integer)
			{
				return AtomicValueProto.AtomicValue.newBuilder().setIntValue((Integer) featureValue).build();
			}
			else if (featureValue instanceof Long)
			{
				return AtomicValueProto.AtomicValue.newBuilder().setLongValue((Long) featureValue).build();
			}
			else if (featureValue instanceof Float)
			{
				return AtomicValueProto.AtomicValue.newBuilder().setFloatValue((Float) featureValue).build();
			}
			else if (featureValue instanceof Double)
			{
				return AtomicValueProto.AtomicValue.newBuilder().setDoubleValue((Double) featureValue).build();
			}
			else if (featureValue instanceof Boolean)
			{
				return AtomicValueProto.AtomicValue.newBuilder().setBoolValue((Boolean) featureValue).build();
			}
			else if (featureValue instanceof Date)
			{
				return AtomicValueProto.AtomicValue.newBuilder().setDateValue(((Date) featureValue).getTime()).build();
			}
			else if (featureValue instanceof byte[])
			{
				return AtomicValueProto.AtomicValue.newBuilder().setBytesValue(ByteString.copyFrom((byte[]) featureValue)).build();
			}
			else
			{
				return null;
			}
		}
	}


	/**
	 * Determines if the values of two features containing atomic values are equal.
	 *
	 * @param value1 The 1st feature value.
	 * @param value2 The 2nd feature value.
	 * @return true if the feature values are the same and atomic, false otherwise.
	 */
	private static boolean atomicFeatureValueEquals(Object value1, Object value2)
	{
		if (value1 instanceof byte[])
		{
			if (value2 instanceof byte[])
			{
				return Arrays.equals((byte[]) value1, (byte[]) value2);
			}
			else
			{
				return false;
			}
		}
		else if (value1 == null)
		{
			return value2 == null;
		}
		else if ((value1 instanceof String) || (value1 instanceof Integer) || (value1 instanceof Long) || (value1 instanceof Boolean) ||
				(value1 instanceof Float) || (value1 instanceof Double) || (value1 instanceof Date))
		{
			return value1.equals(value2);
		}
		else
		{
			return false;
		}
	}


	/**
	 * Used to obtain the value for a feature from a Protobuf object representing an atomic value.
	 *
	 * @param featureValueProto The protobuf object.
	 * @return The value for the feature, or null if it has no value.
	 */
	private static Object getAtomicFeatureValue(AtomicValueProto.AtomicValue featureValueProto)
	{
		Object featureValue = null;

		for (Map.Entry<Descriptors.FieldDescriptor, Object> entry : featureValueProto.getAllFields().entrySet())
		{
			if (featureValueProto.hasField(entry.getKey()))
			{
				String fieldName = entry.getKey().getName();
				featureValue = entry.getValue();

				if (IS_NULL_FIELD_NAME.equals(fieldName) && Boolean.TRUE.equals(featureValue))
				{
					featureValue = null;
				}
				else if (DATE_FIELD_NAME.equals(fieldName))
				{
					featureValue = Date.from(Instant.EPOCH.plusMillis((long) featureValue));
				}
				else
				{
					if (featureValue instanceof ByteString)
					{
						featureValue = ((ByteString) featureValue).toByteArray();
					}
				}
			}
		}

		return featureValue;
	}


	/**
	 * Used to obtain the value for a feature from a Protobuf object.
	 *
	 * @param featureValueProto The protobuf object.
	 * @return The value for the feature, or null if it has no value.
	 */
	public static Object getFeatureValue(DataEntryProto.DataEntry featureValueProto)
	{
		if (featureValueProto.getIsMap())
		{
			Map<String, Object> featureValueMap = new HashMap<>();

			for (Map.Entry<String, DataEntryProto.DataEntry> mapValueEntry : featureValueProto.getMapValueMap().entrySet())
			{
				featureValueMap.put(mapValueEntry.getKey(), getFeatureValue(mapValueEntry.getValue()));
			}

			return featureValueMap;
		}
		else if (featureValueProto.getIsList())
		{
			List<Object> featureValueList = new ArrayList<>();

			for (DataEntryProto.DataEntry listEntry : featureValueProto.getListValueList())
			{
				featureValueList.add(getFeatureValue(listEntry));
			}

			return featureValueList;
		}
		else if (featureValueProto.getIsAtomic())
		{
			return getAtomicFeatureValue(featureValueProto.getAtomicValue());
		}
		else
		{
			throw new IllegalArgumentException("Unrecognised entry type");
		}
	}


	/**
	 * Used to obtain the Protobuf object to represent a feature value.
	 *
	 * @param featureValue The feature value.
	 * @return The Protobuf object.
	 */
	public static DataEntryProto.DataEntry getFeatureValue(Object featureValue)
	{
		if (featureValue instanceof List)
		{
			List<?> featureValueList = (List<?>) featureValue;
			List<DataEntryProto.DataEntry> listValue = new ArrayList<>(featureValueList.size());

			for (Object listEntry : featureValueList)
			{
				listValue.add(getFeatureValue(listEntry));
			}

			return DataEntryProto.DataEntry.newBuilder().setIsMap(false).setIsList(true).setIsAtomic(false).addAllListValue(listValue).build();
		}
		else if (featureValue instanceof Map)
		{
			Map<?, ?> featureValueMap = (Map<?, ?>) featureValue;
			Map<String, DataEntryProto.DataEntry> mapValue = new HashMap<>(featureValueMap.size());

			for (Map.Entry<?, ?> entry : featureValueMap.entrySet())
			{
				String key = (String) entry.getKey();
				mapValue.put(key, getFeatureValue(featureValueMap.get(key)));
			}

			return DataEntryProto.DataEntry.newBuilder().setIsMap(true).setIsList(false).setIsAtomic(false).putAllMapValue(mapValue).build();
		}
		else
		{
			AtomicValueProto.AtomicValue atomicValue = getAtomicFeatureValue(featureValue);

			if (atomicValue == null)
			{
				throw new IllegalArgumentException("featureValue is not a valid value for a feature");
			}
			else
			{
				return DataEntryProto.DataEntry.newBuilder().setIsMap(false).setIsList(false).setIsAtomic(true).setAtomicValue(atomicValue).build();
			}
		}
	}


	/**
	 * Used to determine if a feature has a valid value.
	 *
	 * @param featureValue The value of a feature.
	 * @return true if featureValue is valid value for a feature, false otherwise.
	 */
	public static boolean validFeatureValue(Object featureValue)
	{
		try
		{
			getFeatureValue(featureValue);
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}


	/**
	 * Determines if the values of two features are equal.
	 *
	 * @param value1 The 1st feature value.
	 * @param value2 The 2nd feature value.
	 * @return true if the feature values are the same, false otherwise.
	 */
	public static boolean featureValueEquals(Object value1, Object value2)
	{
		if (value1 instanceof List)
		{
			if (value2 instanceof List)
			{
				List<?> list1 = (List<?>) value1;
				List<?> list2 = (List<?>) value2;
				int list1Size = list1.size();

				if (list1Size != list2.size())
				{
					return false;
				}
				else
				{
					for (int j = 0; j < list1Size; j++)
					{
						if (!featureValueEquals(list1.get(j), list2.get(j)))
						{
							return false;
						}
					}

					return true;
				}
			}
			else
			{
				return false;
			}
		}
		else if (value1 instanceof Map)
		{
			if (value2 instanceof Map)
			{
				Map<?, ?> map1 = (Map<?, ?>) value1;
				Map<?, ?> map2 = (Map<?, ?>) value2;

				if (map1.size() != map2.size())
				{
					return false;
				}
				else
				{
					for (Map.Entry<?, ?> map1Entry : map1.entrySet())
					{
						String map1EntryKey = (String) map1Entry.getKey();

						if (!map2.containsKey(map1EntryKey))
						{
							return false;
						}
						else
						{
							if (!featureValueEquals(map1.get(map1EntryKey), map2.get(map1EntryKey)))
							{
								return false;
							}
						}
					}

					return true;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return atomicFeatureValueEquals(value1, value2);
		}
	}
}
