package archipelagos.common.data;

/**
 * The (optional) properties that a dataset (time-series, collection, file store...) may have.
 */
public final class DataProperties
{
	public final static String WEBSITE = "Website";
	public final static String LICENCE = "Licence";
	public final static String LICENCE_URL = "Licence-URL";
	public final static String INGESTED = "Ingested";
	public final static String COPYRIGHT = "Copyright";
	public final static String COPYRIGHT_URL = "Copyright-URL";
}
