package archipelagos.common.data.timeseries;

import archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto;

/**
 * Holds data about a subset of a time-series.
 */
public final class TimeSeries
{
   private TimeSeriesData _data;
   private TimeSeriesMetadata _metadata;


   /**
    * @param metadata The metadata for the time-series.
    * @param data     The data.
    * @return The time-series.
    */
   public static TimeSeries of(TimeSeriesMetadata metadata, TimeSeriesData data)
   {
      // Check the arguments

      if (metadata == null)
      {
         throw new IllegalArgumentException("metadata is null");
      }

      if (data == null)
      {
         throw new IllegalArgumentException("data is null");
      }


      // Create the time-series and return

      TimeSeries timeSeries = new TimeSeries();
      timeSeries._metadata = metadata;
      timeSeries._data = data;

      return timeSeries;
   }


   /**
    * Creates an object without initializing any properties.
    */
   TimeSeries()
   {
   }


   /**
    * @return The metadata for the time-series.
    */
   public TimeSeriesMetadata getMetadata()
   {
      return _metadata;
   }


   /**
    * @return The data.
    */
   public TimeSeriesData getData()
   {
      return _data;
   }


   /**
    * @see Object
    */
   @Override
   public boolean equals(Object o)
   {
      if (o instanceof TimeSeries)
      {
         TimeSeries timeSeries = (TimeSeries) o;

         if (_metadata == null)
         {
            if (timeSeries._metadata != null)
            {
               return false;
            }
         }
         else
         {
            if (!_metadata.equals(timeSeries._metadata))
            {
               return false;
            }
         }

         if (_data == null)
         {
            return timeSeries._data == null;
         }
         else
         {
            return _data.equals(timeSeries._data);
         }
      }
      else
      {
         return false;
      }
   }


   /**
    * Sets the state of this from a provided Protobuf object.
    *
    * @param timeSeries The Protobuf object to set the state from.
    */
   void readProto(TimeSeriesProto.TimeSeries timeSeries)
   {
      _metadata = new TimeSeriesMetadata();
      _metadata.readProto(timeSeries.getMetadata());

      _data = new TimeSeriesData();
      _data.readProto(timeSeries.getData());
   }


   /**
    * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
    *
    * @param serialized The serialized state of an object.
    * @throws Exception If an error happened during the deserialization process.
    */
   private void readProto(byte[] serialized) throws Exception
   {
      TimeSeriesProto.TimeSeries timeSeries = TimeSeriesProto.TimeSeries.parseFrom(serialized);
      readProto(timeSeries);
   }


   /**
    * Creates a new instance using a byte[] created by serialize().
    *
    * @param serialized The byte[].
    * @throws Exception If there were any issues when deserializing the object.
    */
   public static TimeSeries deserialize(byte[] serialized) throws Exception
   {
      TimeSeries timeSeries =  new TimeSeries();
      timeSeries.readProto(serialized);

      return timeSeries;
   }


   /**
    * @return A Protobuf object containing the serialised state of this.
    */
   TimeSeriesProto.TimeSeries writeProtoObject()
   {
      return TimeSeriesProto.TimeSeries.newBuilder().setMetadata(_metadata.writeProtoObject()).setData(_data.writeProtoObject()).build();
   }


   /**
    * @return A byte[] containing the state of this serialised.
    */
   public byte[] serialize()
   {
      TimeSeriesProto.TimeSeries timeSeries = writeProtoObject();

      return timeSeries.toByteArray();
   }
}
