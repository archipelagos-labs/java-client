package archipelagos.common.data.timeseries;

import archipelagos.common.data.DataUtils;
import archipelagos.common.data.SerializationUtils;
import archipelagos.common.protobuf.common.TimestampProto;
import archipelagos.common.protobuf.common.data.DataEntryProto;
import archipelagos.common.protobuf.common.data.timeseries.TimeSeriesDataProto;

import java.time.*;
import java.util.*;

/**
 * Contains the data for a time-series.
 */
public final class TimeSeriesData
{
    private SortedMap<LocalDateTime, List<Map<String, Object>>> _data;


    /**
     * @param data The data.
     * @return The time-series data.
     */
    public static TimeSeriesData of(SortedMap<LocalDateTime, List<Map<String, Object>>> data)
    {
        if (data == null)
        {
            throw new IllegalArgumentException("data is null");
        }
        else
        {
            // Check that the contents of data are valid

            for (Map.Entry<LocalDateTime, List<Map<String, Object>>> dataEntry : data.entrySet())
            {
                // Check the time for the entry is not null

                LocalDateTime dataEntryKey = dataEntry.getKey();

                if (dataEntryKey == null)
                {
                    throw new IllegalArgumentException("data contains a null key");
                }


                // Check that the list of maps representing features is not null

                List<Map<String, Object>> dataEntryList = dataEntry.getValue();

                if (dataEntryList == null)
                {
                    throw new IllegalArgumentException("data contain a null value");
                }


                // Check that the maps representing features

                for (Map<String, Object> featuresMap : dataEntryList)
                {
                    if (featuresMap == null)
                    {
                        throw new IllegalArgumentException(String.format("data is invalid; the List for %s contains a null value", dataEntryKey));
                    }
                    else
                    {
                        for (Map.Entry<String, Object> feature : featuresMap.entrySet())
                        {
                            // Check the feature name is valid

                            String featureName = feature.getKey();

                            if (featureName == null)
                            {
                                throw new IllegalArgumentException(String.format("data is invalid; the List for %s contains a Map with a null key", dataEntryKey));
                            }
                            else
                            {
                                if (!DataUtils.validToken(featureName))
                                {
                                    throw new IllegalArgumentException(String.format("data is invalid; the List for %s contains a Map with an invalid key (%s)", dataEntryKey, featureName));
                                }
                            }


                            // Check the feature value is valid (null is valid)

                            Object featureValue = feature.getValue();

                            if (!SerializationUtils.validFeatureValue(featureValue))
                            {
                                throw new IllegalArgumentException(String.format("data is invalid; the List for %s contains a Map with an invalid value (%s) for the feature \"%s\")", dataEntryKey, featureValue, featureName));
                            }
                        }
                    }
                }
            }


            // Create the data object and return

            TimeSeriesData timeSeriesData = new TimeSeriesData();
            timeSeriesData._data = data;

            return timeSeriesData;
        }
    }


    /**
     * Creates an object without initializing any properties.
     */
    TimeSeriesData()
    {
    }


    /**
     * @return The data.
     */
    public SortedMap<LocalDateTime, List<Map<String, Object>>> getData()
    {
        return _data;
    }


    /**
     * Used to 'flatten' (one entry per date/time) the data contained within this.
     *
     * @return The "flattened" data.
     */
    public SortedMap<LocalDateTime, Map<String, Object>> getFlattenedData()
    {
        // Check that the ordering is the same as the unflattened map

        SortedMap<LocalDateTime, Map<String, Object>> flattenedData;

        if (_data.comparator() == null)
        {
            flattenedData = new TreeMap<>();
        }
        else
        {
            flattenedData = new TreeMap<>(_data.comparator());
        }


        // Flatten the data

        for (Map.Entry<LocalDateTime, List<Map<String, Object>>> entry : _data.entrySet())
        {
            List<Map<String, Object>> list = entry.getValue();

            if (list.size() > 0)
            {
                flattenedData.put(entry.getKey(), list.get(0));
            }
            else
            {
                flattenedData.put(entry.getKey(), new HashMap<>());
            }
        }

        return flattenedData;
    }


    /**
     * @see Object
     */
    @Override
    public boolean equals(Object o)
    {
        if (o instanceof TimeSeriesData)
        {
            TimeSeriesData timeSeriesData = (TimeSeriesData) o;

            if (_data.size() != timeSeriesData._data.size())
            {
                return false;
            }
            else
            {
                for (Map.Entry<LocalDateTime, List<Map<String, Object>>> dataMapEntry : _data.entrySet())
                {
                    LocalDateTime dataMapEntryKey = dataMapEntry.getKey();

                    if (!timeSeriesData._data.containsKey(dataMapEntryKey))
                    {
                        return false;
                    }
                    else
                    {
                        List<Map<String, Object>> list1 = dataMapEntry.getValue();
                        List<Map<String, Object>> list2 = timeSeriesData._data.get(dataMapEntryKey);
                        int outerListSize = list1.size();

                        if (outerListSize != list2.size())
                        {
                            return false;
                        }
                        else
                        {
                            for (int i = 0; i < outerListSize; i++)
                            {
                                Map<String, Object> map1 = list1.get(i);
                                Map<String, Object> map2 = list2.get(i);

                                if (map1.size() != map2.size())
                                {
                                    return false;
                                }
                                else
                                {
                                    for (Map.Entry<String, Object> feature : map1.entrySet())
                                    {
                                        String featureName = feature.getKey();

                                        if (!map2.containsKey(featureName))
                                        {
                                            return false;
                                        }
                                        else
                                        {
                                            Object value1 = feature.getValue();
                                            Object value2 = map2.get(featureName);

                                            if (!SerializationUtils.featureValueEquals(value1, value2))
                                            {
                                                return false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return true;
            }
        }
        else
        {
            return false;
        }
    }


    /**
     * Sets the state of this from a provided Protobuf object.
     *
     * @param timeSeriesData The Protobuf object to set the state from.
     */
    void readProto(TimeSeriesDataProto.TimeSeriesData timeSeriesData)
    {
        // Evaluate what order the data is in

        int count = timeSeriesData.getTimestampsCount();

        if (count < 2)
        {
            _data = new TreeMap<>();
        }
        else
        {
            TimestampProto.Timestamp timestamp = timeSeriesData.getTimestamps(0);
            LocalDateTime first = LocalDateTime.ofEpochSecond(timestamp.getEpochSecond(), timestamp.getNanosecond(), ZoneOffset.UTC);

            timestamp = timeSeriesData.getTimestamps(count - 1);
            LocalDateTime last = LocalDateTime.ofEpochSecond(timestamp.getEpochSecond(), timestamp.getNanosecond(), ZoneOffset.UTC);

            if (last.compareTo(first) < 0)
            {
                _data = new TreeMap<>(Collections.reverseOrder());
            }
            else
            {
                _data = new TreeMap<>();
            }
        }


        // Populate the data

        for (int i = 0; i < count; i++)
        {
            // Get the Protobuf data

            TimestampProto.Timestamp timestampProto = timeSeriesData.getTimestamps(i);
            DataEntryProto.DataEntry timeSeriesEntryProto = timeSeriesData.getData(i);


            // Convert the timestamp into a LocalDateTime

            long epochSecond = timestampProto.getEpochSecond();
            int nanos = timestampProto.getNanosecond();
            LocalDateTime datetime = LocalDateTime.ofEpochSecond(epochSecond, nanos, ZoneOffset.UTC);


            // Obtain the features for this entry

            Map<String, Object> featuresMap = new HashMap<>();

            for (Map.Entry<String, DataEntryProto.DataEntry> dataForDataEntry : timeSeriesEntryProto.getMapValueMap().entrySet())
            {
                DataEntryProto.DataEntry featureValueProto = dataForDataEntry.getValue();
                String featureName = dataForDataEntry.getKey();
                Object featureValue = SerializationUtils.getFeatureValue(featureValueProto);
                featuresMap.put(featureName, featureValue);
            }


            // Add the data collected above to that stored within this object

            if (_data.containsKey(datetime))
            {
                List<Map<String, Object>> dataForDatetime = _data.get(datetime);
                dataForDatetime.add(featuresMap);
            }
            else
            {
                List<Map<String, Object>> dataForDatetime = new ArrayList<>();
                dataForDatetime.add(featuresMap);
                _data.put(datetime, dataForDatetime);
            }
        }
    }


    /**
     * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
     *
     * @param serialized The serialized state of an object.
     * @throws Exception If an error happened during the deserialization process.
     */
    private void readProto(byte[] serialized) throws Exception
    {
        TimeSeriesDataProto.TimeSeriesData timeSeriesData = TimeSeriesDataProto.TimeSeriesData.parseFrom(serialized);
        readProto(timeSeriesData);
    }


    /**
     * Creates a new instance using a byte[] created by writeProto().
     *
     * @param serialized The byte[].
     * @throws Exception If there were any issues when deserializing the object.
     */
    public static TimeSeriesData deserialize(byte[] serialized) throws Exception
    {
        TimeSeriesData timeSeriesData = new TimeSeriesData();
        timeSeriesData.readProto(serialized);

        return timeSeriesData;
    }


    /**
     * @return A Protobuf object containing the serialised state of this.
     */
    TimeSeriesDataProto.TimeSeriesData writeProtoObject()
    {
        // Create the lists used below to build the proto objects

        int count = _data.size();
        List<TimestampProto.Timestamp> timestamps = new ArrayList<>(count);
        List<DataEntryProto.DataEntry> data = new ArrayList<>(count);


        // Add the data contained in this to the objects created above

        for (Map.Entry<LocalDateTime, List<Map<String, Object>>> entry : _data.entrySet())
        {
            LocalDateTime dateTime = entry.getKey();
            List<Map<String, Object>> dataForDateTime = entry.getValue();

            for (Map<String, Object> features : dataForDateTime)
            {
                // Create and add the timestamp

                timestamps.add(TimestampProto.Timestamp.newBuilder().setEpochSecond(dateTime.toEpochSecond(ZoneOffset.UTC)).setNanosecond(dateTime.getNano()).build());


                // Create and add the features

                Map<String, DataEntryProto.DataEntry> protoFeatures = new HashMap<>();

                for (Map.Entry<String, Object> feature : features.entrySet())
                {
                    protoFeatures.put(feature.getKey(), SerializationUtils.getFeatureValue(feature.getValue()));
                }

                data.add(DataEntryProto.DataEntry.newBuilder().setIsMap(true).setIsList(false).setIsAtomic(false).putAllMapValue(protoFeatures).build());
            }
        }


        // Return the proto object

        return TimeSeriesDataProto.TimeSeriesData.newBuilder().addAllTimestamps(timestamps).addAllData(data).build();
    }


    /**
     * @return A byte[] containing the state of this serialised.
     */
    public byte[] serialize()
    {
        TimeSeriesDataProto.TimeSeriesData timeSeriesData = writeProtoObject();

        return timeSeriesData.toByteArray();
    }
}
