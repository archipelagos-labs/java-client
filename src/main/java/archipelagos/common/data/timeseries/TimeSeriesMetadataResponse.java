package archipelagos.common.data.timeseries;

import archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto;
import archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataResponseProto;

/**
 * Represents the response to a time-series metadata request.
 */
public final class TimeSeriesMetadataResponse
{
    private boolean _errorOccurred;
    private TimeSeriesMetadata _metadata;
    private int _errorCode;
    private String _errorMessage;


    /**
     *
     */
    TimeSeriesMetadataResponse()
    {
    }


    /**
     * Called when data is to be returned.
     *
     * @param metadata The data to return.
     */
    public static TimeSeriesMetadataResponse of(TimeSeriesMetadata metadata)
    {
        // Check the arguments

        if (metadata == null)
        {
            throw new IllegalArgumentException("metadata is null");
        }


        // Create the response and return

        TimeSeriesMetadataResponse timeSeriesMetadataResponse = new TimeSeriesMetadataResponse();
        timeSeriesMetadataResponse._errorOccurred = false;
        timeSeriesMetadataResponse._metadata = metadata;

        return timeSeriesMetadataResponse;
    }


    /**
     * Called when an error has occurred.
     *
     * @param errorCode    The error code.
     * @param errorMessage The error message (must be non-null).
     */
    public static TimeSeriesMetadataResponse of(int errorCode, String errorMessage)
    {
        // Check the arguments

        if (errorMessage == null)
        {
            throw new IllegalArgumentException("timeSeries is null");
        }


        // Create the response and return

        TimeSeriesMetadataResponse timeSeriesMetadataResponse = new TimeSeriesMetadataResponse();
        timeSeriesMetadataResponse._errorOccurred = true;
        timeSeriesMetadataResponse._errorCode = errorCode;
        timeSeriesMetadataResponse._errorMessage = errorMessage;


        return timeSeriesMetadataResponse;
    }


    /**
     * @return true if an error occurred, false otherwise.
     */
    public boolean errorOccurred()
    {
        return _errorOccurred;
    }


    /**
     * @return If an error did not occur then the data to return, null otherwise.
     */
    public TimeSeriesMetadata getMetadata()
    {
        return _metadata;
    }


    /**
     * @return If an error occurred then the error code, 0 otherwise.
     */
    public int getErrorCode()
    {
        return _errorCode;
    }


    /**
     * @return If an error occurred then the error message, null otherwise.
     */
    public String getErrorMessage()
    {
        return _errorMessage;
    }


    /**
     * Sets the state of this from a provided Protobuf object.
     *
     * @param metadataResponse The Protobuf object to set the state from.
     */
    void readProto(TimeSeriesMetadataResponseProto.TimeSeriesMetadataResponse metadataResponse)
    {
        if (metadataResponse.getErrorHasOccurred())
        {
            _errorOccurred = true;
            _errorCode = metadataResponse.getErrorCode();
            _errorMessage = metadataResponse.getErrorMessage();
        }
        else
        {
            _errorOccurred = false;
            _metadata = new TimeSeriesMetadata();
            _metadata.readProto(metadataResponse.getMetadata());
        }
    }


    /**
     * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
     *
     * @param serialized The serialized state of an object.
     * @throws Exception If an error happened during the deserialization process.
     */
    private void readProto(byte[] serialized) throws Exception
    {
        TimeSeriesMetadataResponseProto.TimeSeriesMetadataResponse response = TimeSeriesMetadataResponseProto.TimeSeriesMetadataResponse.parseFrom(serialized);
        readProto(response);
    }


    /**
     * Creates a new instance using a byte[] created by serialize().
     *
     * @param serialized The byte[].
     * @throws Exception If there were any issues when deserializing the object.
     */
    public static TimeSeriesMetadataResponse deserialize(byte[] serialized) throws Exception
    {
        TimeSeriesMetadataResponse timeSeriesMetadataResponse = new TimeSeriesMetadataResponse();
        timeSeriesMetadataResponse.readProto(serialized);

        return timeSeriesMetadataResponse;
    }


    /**
     * @return A Protobuf object containing the serialised state of this.
     */
    TimeSeriesMetadataResponseProto.TimeSeriesMetadataResponse writeProtoObject()
    {
        if (_errorOccurred)
        {
            return TimeSeriesMetadataResponseProto.TimeSeriesMetadataResponse.newBuilder().setErrorHasOccurred(true).setErrorCode(_errorCode).setErrorMessage(_errorMessage).build();
        }
        else
        {
            TimeSeriesMetadataProto.TimeSeriesMetadata metadata = _metadata.writeProtoObject();
            return TimeSeriesMetadataResponseProto.TimeSeriesMetadataResponse.newBuilder().setErrorHasOccurred(false).setMetadata(metadata).build();
        }
    }


    /**
     * @return A byte[] containing the state of this serialised.
     */
    public byte[] serialize()
    {
        TimeSeriesMetadataResponseProto.TimeSeriesMetadataResponse timeSeriesMetadataResponse = writeProtoObject();

        return timeSeriesMetadataResponse.toByteArray();
    }
}
