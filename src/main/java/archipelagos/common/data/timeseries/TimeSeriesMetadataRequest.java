package archipelagos.common.data.timeseries;

import archipelagos.common.data.DataUtils;
import archipelagos.common.platform.ClientType;
import archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataRequestProto;

/**
 * Represents a time-series metadata request.
 */
public final class TimeSeriesMetadataRequest
{
    private String _apiKey;
    private String _source;
    private String _category;
    private String _label;
    private String _clientType;


    /**
     *
     */
    TimeSeriesMetadataRequest()
    {
    }


    /**
     * @param source    The source for the time-series.
     * @param category  The category for the time-series.
     * @param label     The label for the time-series.
     */
    public static TimeSeriesMetadataRequest of(String source, String category, String label)
    {
        // Check the arguments

        if (source == null)
        {
            throw new IllegalArgumentException("source is null");
        }

        if(!DataUtils.validToken(source))
        {
            throw new IllegalArgumentException("source is not valid");
        }

        if (category == null)
        {
            throw new IllegalArgumentException("category is null");
        }

        if(!DataUtils.validToken(category))
        {
            throw new IllegalArgumentException("category is not valid");
        }

        if (label == null)
        {
            throw new IllegalArgumentException("label is null");
        }

        if(!DataUtils.validToken(label))
        {
            throw new IllegalArgumentException("label is not valid");
        }


        // Create the request and return

        TimeSeriesMetadataRequest timeSeriesMetadataRequest = new TimeSeriesMetadataRequest();
        timeSeriesMetadataRequest._apiKey = null;
        timeSeriesMetadataRequest._source = source;
        timeSeriesMetadataRequest._category = category;
        timeSeriesMetadataRequest._label = label;
        timeSeriesMetadataRequest._clientType = ClientType.JAVA_CLIENT;

        return timeSeriesMetadataRequest;
    }


    /**
     * @param apiKey    The API key.
     * @param source    The source for the time-series.
     * @param category  The category for the time-series.
     * @param label     The label for the time-series.
     */
    public static TimeSeriesMetadataRequest of(String source, String category, String label, String apiKey)
    {
        // Check the arguments

        if (source == null)
        {
            throw new IllegalArgumentException("source is null");
        }

        if(!DataUtils.validToken(source))
        {
            throw new IllegalArgumentException("source is not valid");
        }

        if (category == null)
        {
            throw new IllegalArgumentException("category is null");
        }

        if(!DataUtils.validToken(category))
        {
            throw new IllegalArgumentException("category is not valid");
        }

        if (label == null)
        {
            throw new IllegalArgumentException("label is null");
        }

        if(!DataUtils.validToken(label))
        {
            throw new IllegalArgumentException("label is not valid");
        }

        if (apiKey == null)
        {
            throw new IllegalArgumentException("apiKey is null");
        }

        if(!DataUtils.validToken(apiKey))
        {
            throw new IllegalArgumentException("apiKey is not a valid API Key");
        }


        // Create the request and return

        TimeSeriesMetadataRequest timeSeriesMetadataRequest = new TimeSeriesMetadataRequest();
        timeSeriesMetadataRequest._apiKey = apiKey;
        timeSeriesMetadataRequest._source = source;
        timeSeriesMetadataRequest._category = category;
        timeSeriesMetadataRequest._label = label;
        timeSeriesMetadataRequest._clientType = ClientType.JAVA_CLIENT;

        return timeSeriesMetadataRequest;
    }


    /**
     * @return The API key..
     */
    public String getApiKey()
    {
        return _apiKey;
    }


    /**
     * Sets the API Key
     *
     * @param apiKey The new API key.
     */
    public void setApiKey(String apiKey)
    {
        _apiKey = apiKey;
    }


    /**
     * @return The source for the time-series.
     */
    public String getSource()
    {
        return _source;
    }


    /**
     * @return The category for the time-series.
     */
    public String getCategory()
    {
        return _category;
    }


    /**
     * @return The label for the time-series.
     */
    public String getLabel()
    {
        return _label;
    }


    /**
     * @return The type of client that sent the request.
     */
    public String getClientType()
    {
        return _clientType;
    }


    /**
     * Sets the state of this from a provided Protobuf object.
     *
     * @param timeSeriesMetadataRequest The Protobuf object to set the state from.
     */
    void readProto(TimeSeriesMetadataRequestProto.TimeSeriesMetadataRequest timeSeriesMetadataRequest)
    {
        _apiKey = timeSeriesMetadataRequest.getApiKey();
        _source = timeSeriesMetadataRequest.getSource();
        _category = timeSeriesMetadataRequest.getCategory();
        _label = timeSeriesMetadataRequest.getLabel();
        _clientType = timeSeriesMetadataRequest.getClientType();
    }


    /**
     * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
     *
     * @param serialized The serialized state of an object.
     * @throws Exception If an error happened during the deserialization process.
     */
    private void readProto(byte[] serialized) throws Exception
    {
        TimeSeriesMetadataRequestProto.TimeSeriesMetadataRequest timeSeriesMetadataRequest = TimeSeriesMetadataRequestProto.TimeSeriesMetadataRequest.parseFrom(serialized);
        readProto(timeSeriesMetadataRequest);
    }


    /**
     * Creates a new instance using a byte[] created by serialize().
     *
     * @param serialized The byte[].
     * @throws Exception If there were any issues when deserializing the object.
     */
    public static TimeSeriesMetadataRequest deserialize(byte[] serialized) throws Exception
    {
        TimeSeriesMetadataRequest timeSeriesMetadataRequest = new TimeSeriesMetadataRequest();
        timeSeriesMetadataRequest.readProto(serialized);

        return timeSeriesMetadataRequest;
    }


    /**
     * @return A Protobuf object containing the serialised state of this.
     */
    TimeSeriesMetadataRequestProto.TimeSeriesMetadataRequest writeProtoObject()
    {
        TimeSeriesMetadataRequestProto.TimeSeriesMetadataRequest.Builder requestBuilder = TimeSeriesMetadataRequestProto.TimeSeriesMetadataRequest.newBuilder();
        return requestBuilder.setApiKey(_apiKey).setSource(_source).setCategory(_category).setLabel(_label).setClientType(_clientType).build();
    }


    /**
     * @return A byte[] containing the state of this serialised.
     */
    public byte[] serialize()
    {
        TimeSeriesMetadataRequestProto.TimeSeriesMetadataRequest timeSeriesMetadataRequest = writeProtoObject();

        return timeSeriesMetadataRequest.toByteArray();
    }
}
