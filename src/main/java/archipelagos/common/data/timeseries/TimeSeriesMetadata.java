package archipelagos.common.data.timeseries;

import archipelagos.common.data.DataUtils;
import archipelagos.common.data.Frequency;
import archipelagos.common.protobuf.common.TimestampProto;
import archipelagos.common.protobuf.common.data.timeseries.TimeSeriesMetadataProto;

import java.time.*;
import java.util.*;

/**
 * Represents the metadata about a time-series.
 */
public final class TimeSeriesMetadata
{
   private String _source;
   private String _category;
   private String _label;
   private String _url;
   private String _summary;
   private String _description;
   private Map<String, String> _features;
   private Frequency _frequency;
   private Map<String, String> _properties;
   private boolean _premium;
   private LocalDateTime _created;
   private LocalDateTime _edited;
   private LocalDateTime _refreshed;
   private LocalDateTime _oldestDateTime;
   private LocalDateTime _newestDateTime;
   private int _hashCode;


   /**
    * Should be used when the created/edited date/time is now, and the refresh date/time, oldest and latest entry is null.
    *
    * @param source      The source for the time-series.
    * @param category    The category for the time-series.
    * @param label       The label for the time-series.
    * @param url         The URL for the time-series.
    * @param summary     The summary for the time-series.
    * @param description The description for the time-series.
    * @param features    The time-series' features and their descriptions.
    * @param frequency   The frequency of the time-series.
    * @param properties  Any properties that may be associated with the time-series.
    * @param premium     true if the time-series is premium, false otherwise.
    */
   public static TimeSeriesMetadata of(String source, String category, String label, String url, String summary, String description, Map<String, String> features, Frequency frequency, Map<String, String> properties, boolean premium)
   {
      LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
      return new TimeSeriesMetadata(source, category, label, url, summary, description, features, frequency, properties, premium, now, now, null, null, null);
   }


   /**
    * @param source         The source for the time-series.
    * @param category       The category for the time-series.
    * @param label          The label for the time-series.
    * @param url            The URL for the time-series.
    * @param summary        The summary of the time-series.
    * @param description    The description for the time-series.
    * @param features       The time-series' features and their descriptions.
    * @param frequency      The frequency of the time-series.
    * @param properties     Any properties that may be associated with the time-series.
    * @param premium        true if the time-series is premium, false otherwise.
    * @param created        When (in UTC) the time-series was created.
    * @param edited         When (in UTC) the metadata for the time-series was last edited.
    * @param refreshed      When (in UTC) the data in the time-series was last refreshed; can be null if no data has yet been entered.
    * @param oldestDateTime The oldest date/time in the associated time-series; can be null if the time-series is empty.
    * @param newestDateTime The latest date/time in the associated time-series; can be null if the time-series is empty.
    */
   public static TimeSeriesMetadata of(String source, String category, String label, String url, String summary, String description, Map<String, String> features, Frequency frequency, Map<String, String> properties,
                                       boolean premium, LocalDateTime created, LocalDateTime edited, LocalDateTime refreshed, LocalDateTime oldestDateTime, LocalDateTime newestDateTime)
   {
      return new TimeSeriesMetadata(source, category, label, url, summary, description, features, frequency, properties, premium, created, edited, refreshed, oldestDateTime, newestDateTime);
   }


   /**
    * Creates an object without initializing any properties.
    */
   TimeSeriesMetadata()
   {
      buildHashCode();
   }


   /**
    * @param source         The source for the time-series.
    * @param category       The category for the time-series.
    * @param label          The label for the time-series.
    * @param url            The URL for the time-series.
    * @param summary        The summary of the time-series.
    * @param description    The description for the time-series.
    * @param features       The time-series' features and their descriptions.
    * @param frequency      The frequency of the time-series.
    * @param properties     Any properties that may be associated with the time-series.
    * @param premium        true if the time-series is premium, false otherwise.
    * @param created        The date/time in UTC when this was created.
    * @param edited         The date/time in UTC when this was last edited.
    * @param refreshed      When (in UTC) the data in the time-series was last refreshed; can be null if no data has yet been entered.
    * @param oldestDateTime The oldest date/time in the associated time-series; can be null if the time-series is empty.
    * @param newestDateTime The newest date/time in the associated time-series; can be null if the time-series is empty.
    */
   private TimeSeriesMetadata(String source, String category, String label, String url, String summary, String description, Map<String, String> features, Frequency frequency, Map<String, String> properties,
                              boolean premium, LocalDateTime created, LocalDateTime edited, LocalDateTime refreshed, LocalDateTime oldestDateTime, LocalDateTime newestDateTime)
   {
      // Check the arguments

      if (source == null)
      {
         throw new IllegalArgumentException("source is null");
      }
      else
      {
         if (!DataUtils.validToken(source))
         {
            throw new IllegalArgumentException("source is not valid");
         }
      }

      if (category == null)
      {
         throw new IllegalArgumentException("category is null");
      }
      else
      {
         if (!DataUtils.validToken(category))
         {
            throw new IllegalArgumentException("category is not valid");
         }
      }

      if (label == null)
      {
         throw new IllegalArgumentException("label is null");
      }
      else
      {
         if (!DataUtils.validToken(label))
         {
            throw new IllegalArgumentException("label is not valid");
         }
      }

      if (url == null)
      {
         throw new IllegalArgumentException("url is null");
      }

      if (summary == null)
      {
         throw new IllegalArgumentException("summary is null");
      }

      if (description == null)
      {
         throw new IllegalArgumentException("description is null");
      }

      if (features == null)
      {
         throw new IllegalArgumentException("features is null");
      }
      else
      {
         for (Map.Entry<String, String> feature : features.entrySet())
         {
            if (feature.getKey() == null)
            {
               throw new IllegalArgumentException("features contains one or more null features");
            }
            else
            {
               if (!DataUtils.validToken(feature.getKey()))
               {
                  throw new IllegalArgumentException(String.format("\"%s\" is not a valid name for a feature", feature.getKey()));
               }
            }

            if (feature.getValue() == null)
            {
               throw new IllegalArgumentException("features contains one or more null feature descriptions");
            }
         }
      }

      if (properties == null)
      {
         throw new IllegalArgumentException("properties is null");
      }
      else
      {
         for (Map.Entry<String, String> property : properties.entrySet())
         {
            if (property.getKey() == null)
            {
               throw new IllegalArgumentException("properties contains one or more null properties");
            }
            else
            {
               if (!DataUtils.validToken(property.getKey()))
               {
                  throw new IllegalArgumentException(String.format("\"%s\" is not a valid name for a property", property.getKey()));
               }
            }

            if (property.getValue() == null)
            {
               throw new IllegalArgumentException("properties contains one or more null property descriptions");
            }
         }
      }

      if (created == null)
      {
         throw new IllegalArgumentException("created is null");
      }

      if (edited == null)
      {
         throw new IllegalArgumentException("edited is null");
      }
      else if (edited.compareTo(created) < 0)
      {
         throw new IllegalArgumentException("edited is before created");
      }

      if ((oldestDateTime != null || newestDateTime != null) && refreshed == null)
      {
         throw new IllegalArgumentException("refreshed is null but either oldestEntry or newestEntry is not");
      }

      if ((oldestDateTime == null) && (newestDateTime != null))
      {
         throw new IllegalArgumentException("newestEntry is defined but oldestEntry is not");
      }

      if ((oldestDateTime != null) && (newestDateTime != null) && (newestDateTime.compareTo(oldestDateTime) < 0))
      {
         throw new IllegalArgumentException("newestEntry is before oldestEntry");
      }


      // Store the data

      _source = source;
      _category = category;
      _label = label;
      _url = url;
      _summary = summary;
      _description = description;
      _features = features;
      _frequency = frequency;
      _properties = properties;
      _premium = premium;
      _created = created;
      _edited = edited;
      _refreshed = refreshed;
      _oldestDateTime = oldestDateTime;
      _newestDateTime = newestDateTime;

      buildHashCode();
   }


   /**
    * Builds and caches the hash code associated with this.
    */
   private void buildHashCode()
   {
      int prime = 31, result = 1;

      result = prime * result + ((_source == null) ? 0 : _source.hashCode());
      result = prime * result + ((_category == null) ? 0 : _category.hashCode());
      result = prime * result + ((_label == null) ? 0 : _label.hashCode());

      _hashCode = result;
   }


   /**
    * @return The source for the time-series.
    */
   public String getSource()
   {
      return _source;
   }


   /**
    * @return The category for the time-series.
    */
   public String getCategory()
   {
      return _category;
   }


   /**
    * @return The label for the time-series.
    */
   public String getLabel()
   {
      return _label;
   }


   /**
    * @return The URL for the time-series.
    */
   public String getUrl()
   {
      return _url;
   }


   /**
    * @return The summary for the time-series.
    */
   public String getSummary()
   {
      return _summary;
   }


   /**
    * @return The description for the time-series.
    */
   public String getDescription()
   {
      return _description;
   }


   /**
    * @return The time-series' features and their descriptions.
    */
   public Map<String, String> getFeatures()
   {
      return _features;
   }


   /**
    * @return The frequency of the time-series.
    */
   public Frequency getFrequency()
   {
      return _frequency;
   }


   /**
    * @return Any properties that may be associated with the time-series.
    */
   public Map<String, String> getProperties()
   {
      return _properties;
   }


   /**
    * @return true if the time-series is premium, false otherwise.
    */
   public boolean isPremium()
   {
      return _premium;
   }


   /**
    * @return The date/time in UTC when this was created.
    */
   public LocalDateTime getCreated()
   {
      return _created;
   }


   /**
    * @return The date/time in UTC when this was last edited.
    */
   public LocalDateTime getEdited()
   {
      return _edited;
   }


   /**
    * @return When (in UTC) the data in the time-series was last refreshed; can be null if no data has yet been entered.
    */
   public LocalDateTime getRefreshed()
   {
      return _refreshed;
   }


   /**
    * @return The oldest date/time in the associated time-series; can be null if the time-series is empty.
    */
   public LocalDateTime getOldestDateTime()
   {
      return _oldestDateTime;
   }


   /**
    * @return The newest date/time in the associated time-series; can be null if the time-series is empty.
    */
   public LocalDateTime getNewestDateTime()
   {
      return _newestDateTime;
   }


   /**
    * @see Object
    */
   @Override
   public boolean equals(Object o)
   {
      // Check that the object is non-null and of the correct type

      if (o instanceof TimeSeriesMetadata)
      {
         TimeSeriesMetadata metadata = (TimeSeriesMetadata) o;


         // Evaluate the source

         if (_source == null)
         {
            if (metadata._source != null)
            {
               return false;
            }
         }
         else
         {
            if (!_source.equals(metadata._source))
            {
               return false;
            }
         }


         // Evaluate the category

         if (_category == null)
         {
            if (metadata._category != null)
            {
               return false;
            }
         }
         else
         {
            if (!_category.equals(metadata._category))
            {
               return false;
            }
         }


         // Evaluate the label

         if (_label == null)
         {
            if (metadata._label != null)
            {
               return false;
            }
         }
         else
         {
            if (!_label.equals(metadata._label))
            {
               return false;
            }
         }


         // Everything matches so return true

         return true;
      }
      else
      {
         return false;
      }
   }


   /**
    * @see Object
    */
   @Override
   public int hashCode()
   {
      return _hashCode;
   }


   /**
    * @see Object
    */
   @Override
   public String toString()
   {
      StringBuilder toReturn = new StringBuilder();

      toReturn.append("Source = \"");
      toReturn.append(_source);
      toReturn.append("\", Category = \"");
      toReturn.append(_category);
      toReturn.append("\", Label = \"");
      toReturn.append(_label);
      toReturn.append("\", URL = \"");
      toReturn.append(_url);
      toReturn.append("\", Summary = \"");
      toReturn.append(_summary);
      toReturn.append("\", Description = \"");
      toReturn.append(_description);
      toReturn.append("\", Frequency = \"");
      toReturn.append(_frequency.toString());
      toReturn.append("\", Premium = ");
      toReturn.append(_premium);
      toReturn.append(", Features = {");
      boolean isFirst = true;

      for (Map.Entry<String, String> feature : _features.entrySet())
      {
         if (isFirst)
         {
            isFirst = false;
         }
         else
         {
            toReturn.append(", ");
         }

         toReturn.append(feature.getKey());
         toReturn.append("='");
         toReturn.append(feature.getValue());
         toReturn.append("'");
      }

      toReturn.append(", Properties = {");
      isFirst = true;

      for (Map.Entry<String, String> property : _properties.entrySet())
      {
         if (isFirst)
         {
            isFirst = false;
         }
         else
         {
            toReturn.append(", ");
         }

         toReturn.append(property.getKey());
         toReturn.append("=");
         toReturn.append(property.getValue());
      }

      toReturn.append("}, Created = ");
      toReturn.append(DataUtils.getDateTime(_created));
      toReturn.append(", Edited = ");
      toReturn.append(DataUtils.getDateTime(_edited));
      toReturn.append(", Refreshed = ");
      toReturn.append(DataUtils.getDateTime(_refreshed));
      toReturn.append(", Oldest Date/Time = ");
      toReturn.append(DataUtils.getDateTime(_oldestDateTime));
      toReturn.append(", Latest Date/Time = ");
      toReturn.append(DataUtils.getDateTime(_newestDateTime));

      return toReturn.toString();
   }


   /**
    * Used to convert a Protobuf timestamp to a LocalDateTime.
    *
    * @param timestamp The Protobuf timestamp (which may be null).
    * @return The date/time for timestamp, or null if timestamp is null.
    */
   private static LocalDateTime getLocalDateTime(TimestampProto.Timestamp timestamp)
   {
      if (timestamp == null)
      {
         return null;
      }
      else
      {
         return LocalDateTime.ofEpochSecond(timestamp.getEpochSecond(), timestamp.getNanosecond(), ZoneOffset.UTC);
      }
   }


   /**
    * Sets the state of this from a provided Protobuf object.
    *
    * @param timeSeriesMetadata The Protobuf object to set the state from.
    */
   void readProto(TimeSeriesMetadataProto.TimeSeriesMetadata timeSeriesMetadata)
   {
      // Set any simple attributes

      _source = timeSeriesMetadata.getSource();
      _category = timeSeriesMetadata.getCategory();
      _label = timeSeriesMetadata.getLabel();
      _url = timeSeriesMetadata.getUrl();
      _summary = timeSeriesMetadata.getSummary();
      _description = timeSeriesMetadata.getDescription();
      _frequency = Frequency.valueOf(timeSeriesMetadata.getFrequency());
      _premium = timeSeriesMetadata.getPremium();


      // Add any features and properties

      _features = new HashMap<>();
      _features.putAll(timeSeriesMetadata.getFeaturesMap());

      _properties = new HashMap<>();
      _properties.putAll(timeSeriesMetadata.getPropertiesMap());


      // Get any date/times

      _created = getLocalDateTime(timeSeriesMetadata.getCreated());
      _edited = getLocalDateTime(timeSeriesMetadata.getEdited());

      if (timeSeriesMetadata.getRefreshedSet())
      {
         _refreshed = getLocalDateTime(timeSeriesMetadata.getRefreshed());
      }
      else
      {
         _refreshed = null;
      }

      if (timeSeriesMetadata.getOldestDateTimeSet())
      {
         _oldestDateTime = getLocalDateTime(timeSeriesMetadata.getOldestDateTime());
         _newestDateTime = getLocalDateTime(timeSeriesMetadata.getNewestDateTime());
      }
      else
      {
         _oldestDateTime = null;
         _newestDateTime = null;
      }


      // Rebuild the hashcode

      buildHashCode();
   }


   /**
    * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
    *
    * @param serialized The serialized state of an object.
    * @throws Exception If an error happened during the deserialization process.
    */
   private void readProto(byte[] serialized) throws Exception
   {
      TimeSeriesMetadataProto.TimeSeriesMetadata timeSeriesMetadata = TimeSeriesMetadataProto.TimeSeriesMetadata.parseFrom(serialized);
      readProto(timeSeriesMetadata);
   }


   /**
    * Creates a new instance using a byte[] created by serialize().
    *
    * @param serialized The byte[].
    * @throws Exception If there were any issues when deserializing the object.
    */
   public static TimeSeriesMetadata deserialize(byte[] serialized) throws Exception
   {
      TimeSeriesMetadata timeSeriesMetadata = new TimeSeriesMetadata();
      timeSeriesMetadata.readProto(serialized);

      return timeSeriesMetadata;
   }


   /**
    * Used to convert a Protobuf timestamp to a LocalDateTime.
    *
    * @param dateTime The date/time.
    * @return The timestamp for date/time.
    */
   private static TimestampProto.Timestamp getTimestamp(LocalDateTime dateTime)
   {
      return TimestampProto.Timestamp.newBuilder().setEpochSecond(dateTime.toEpochSecond(ZoneOffset.UTC)).setNanosecond(dateTime.getNano()).build();
   }


   /**
    * @return A Protobuf object containing the serialised state of this.
    */
   TimeSeriesMetadataProto.TimeSeriesMetadata writeProtoObject()
   {
      TimeSeriesMetadataProto.TimeSeriesMetadata.Builder builder;
      builder = TimeSeriesMetadataProto.TimeSeriesMetadata.newBuilder();
      builder.setSource(_source).setCategory(_category).setLabel(_label).setUrl(_url).setSummary(_summary).setDescription(_description).setFrequency(_frequency.name()).setPremium(_premium);
      builder.putAllFeatures(_features);
      builder.putAllProperties(_properties);
      builder.setCreated(getTimestamp(_created)).build();
      builder.setEdited(getTimestamp(_edited)).build();

      if (_refreshed == null)
      {
         builder.setRefreshedSet(false);
      }
      else
      {
         builder.setRefreshedSet(true);
         builder.setRefreshed(getTimestamp(_refreshed)).build();
      }

      if (_oldestDateTime == null)
      {
         builder.setOldestDateTimeSet(false);
      }
      else
      {
         builder.setOldestDateTimeSet(true);
         builder.setOldestDateTime(getTimestamp(_oldestDateTime)).build();
         builder.setNewestDateTime(getTimestamp(_newestDateTime)).build();
      }

      return builder.build();
   }


   /**
    * @return A byte[] containing the state of this serialised.
    */
   public byte[] serialize()
   {
      TimeSeriesMetadataProto.TimeSeriesMetadata timeSeriesMetadata = writeProtoObject();

      return timeSeriesMetadata.toByteArray();
   }
}
