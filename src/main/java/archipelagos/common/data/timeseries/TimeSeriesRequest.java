package archipelagos.common.data.timeseries;

import archipelagos.common.data.DataUtils;
import archipelagos.common.data.filter.FilterUtils;
import archipelagos.common.platform.ClientType;
import archipelagos.common.protobuf.common.TimestampProto;
import archipelagos.common.protobuf.common.data.timeseries.TimeSeriesRequestProto;
import com.google.protobuf.ProtocolStringList;

import java.time.*;
import java.util.*;

/**
 * Represents a time-series request.
 */
public final class TimeSeriesRequest
{
    private String _apiKey;
    private String _source;
    private String _category;
    private String _label;
    private LocalDateTime _start;
    private LocalDateTime _end;
    private List<String> _features;
    private long _maxSize;
    private boolean _ascending;
    private Map<String, String> _filters;
    private String _clientType;


    /**
     *
     */
    TimeSeriesRequest()
    {
    }


    /**
     * @param source    The source for the time-series.
     * @param category  The category for the time-series.
     * @param label     The label for the time-series.
     * @param start     The (optional) start date/time, or null.
     * @param end       The (optional) end date/time, or null.
     * @param features  The (optional) list of features, or null.
     * @param maxSize   The maximum number of items that can be returned, or -1 if there should be no limit.
     * @param ascending true if the data should be returned in ascending order, false otherwise.
     * @param filters   The filters.
     */
    public static TimeSeriesRequest of(String source, String category, String label, LocalDateTime start, LocalDateTime end, List<String> features, long maxSize, boolean ascending, Map<String, String> filters)
    {
        // Check the arguments

        if (source == null)
        {
            throw new IllegalArgumentException("source is null");
        }

        if (!DataUtils.validToken(source))
        {
            throw new IllegalArgumentException("source is not valid");
        }

        if (category == null)
        {
            throw new IllegalArgumentException("category is null");
        }

        if (!DataUtils.validToken(category))
        {
            throw new IllegalArgumentException("category is not valid");
        }

        if (label == null)
        {
            throw new IllegalArgumentException("label is null");
        }

        if (!DataUtils.validToken(label))
        {
            throw new IllegalArgumentException("label is not valid");
        }

        if (features != null)
        {
            for(String feature : features)
            {
                if (!DataUtils.validToken(feature))
                {
                    throw new IllegalArgumentException(String.format("\"%s\" is not a valid name for a feature", feature));
                }
            }
        }

        if (maxSize < -1)
        {
            throw new IllegalArgumentException("maxSize is less than -1");
        }

        if (filters == null)
        {
            throw new IllegalArgumentException("filters is null");
        }

        for(Map.Entry<String, String> filter : filters.entrySet())
        {
            String featureName = filter.getKey();

            if (!DataUtils.validToken(featureName))
            {
                throw new IllegalArgumentException(String.format("filters is invalid; \"%s\" is not a valid name for a feature", featureName));
            }
        }


        // Create the request and return

        TimeSeriesRequest timeSeriesRequest = new TimeSeriesRequest();
        timeSeriesRequest._apiKey = null;
        timeSeriesRequest._source = source;
        timeSeriesRequest._category = category;
        timeSeriesRequest._label = label;
        timeSeriesRequest._start = start;
        timeSeriesRequest._end = end;
        timeSeriesRequest._features = features;
        timeSeriesRequest._maxSize = maxSize;
        timeSeriesRequest._ascending = ascending;
        timeSeriesRequest._filters = filters;
        timeSeriesRequest._clientType = ClientType.JAVA_CLIENT;

        return timeSeriesRequest;
    }


    /**
     * @param source    The source for the time-series.
     * @param category  The category for the time-series.
     * @param label     The label for the time-series.
     * @param start     The (optional) start date/time, or null.
     * @param end       The (optional) end date/time, or null.
     * @param features  The (optional) list of features, or null.
     * @param maxSize   The maximum number of items that can be returned, or -1 if there should be no limit.
     * @param ascending true if the data should be returned in ascending order, false otherwise.
     * @param filters   The filters.
     * @param apiKey    The API key.
     */
    public static TimeSeriesRequest of(String source, String category, String label, LocalDateTime start, LocalDateTime end, List<String> features, long maxSize, boolean ascending, Map<String, String> filters, String apiKey)
    {
        // Check the arguments

        if (source == null)
        {
            throw new IllegalArgumentException("source is null");
        }

        if (!DataUtils.validToken(source))
        {
            throw new IllegalArgumentException("source is not valid");
        }

        if (category == null)
        {
            throw new IllegalArgumentException("category is null");
        }

        if (!DataUtils.validToken(category))
        {
            throw new IllegalArgumentException("category is not valid");
        }

        if (label == null)
        {
            throw new IllegalArgumentException("label is null");
        }

        if (!DataUtils.validToken(label))
        {
            throw new IllegalArgumentException("label is not valid");
        }

        if (features != null)
        {
            for(String feature : features)
            {
                if (!DataUtils.validToken(feature))
                {
                    throw new IllegalArgumentException(String.format("\"%s\" is not a valid name for a feature", feature));
                }
            }
        }

        if (maxSize < -1)
        {
            throw new IllegalArgumentException("maxSize is less than -1");
        }

        if (filters == null)
        {
            throw new IllegalArgumentException("filters is null");
        }

        for(Map.Entry<String, String> filter : filters.entrySet())
        {
            String featureName = filter.getKey();

            if (!FilterUtils.isValidFeature(featureName))
            {
                throw new IllegalArgumentException(String.format("filters is invalid; \"%s\" is not a valid name for a feature", featureName));
            }
        }

        if (apiKey == null)
        {
            throw new IllegalArgumentException("apiKey is null");
        }

        if (!DataUtils.validToken(apiKey))
        {
            throw new IllegalArgumentException("apiKey is not a valid API Key");
        }


        // Create the request and return

        TimeSeriesRequest timeSeriesRequest = new TimeSeriesRequest();
        timeSeriesRequest._apiKey = apiKey;
        timeSeriesRequest._source = source;
        timeSeriesRequest._category = category;
        timeSeriesRequest._label = label;
        timeSeriesRequest._start = start;
        timeSeriesRequest._end = end;
        timeSeriesRequest._features = features;
        timeSeriesRequest._maxSize = maxSize;
        timeSeriesRequest._ascending = ascending;
        timeSeriesRequest._filters = filters;
        timeSeriesRequest._clientType = ClientType.JAVA_CLIENT;

        return timeSeriesRequest;
    }


    /**
     * @return The API key.
     */
    public String getApiKey()
    {
        return _apiKey;
    }


    /**
     * Sets the API Key
     *
     * @param apiKey The new API key.
     */
    public void setApiKey(String apiKey)
    {
        _apiKey = apiKey;
    }


    /**
     * @return The source for the time-series.
     */
    public String getSource()
    {
        return _source;
    }


    /**
     * @return The category for the time-series.
     */
    public String getCategory()
    {
        return _category;
    }


    /**
     * @return The label for the time-series.
     */
    public String getLabel()
    {
        return _label;
    }


    /**
     * @return The (optional) start date/time, or null.
     */
    public LocalDateTime getStart()
    {
        return _start;
    }


    /**
     * @return The (optional) end date/time, or null.
     */
    public LocalDateTime getEnd()
    {
        return _end;
    }


    /**
     * @return The (optional) list of features, or null.
     */
    public List<String> getFeatures()
    {
        return _features;
    }


    /**
     * @return The maximum number of items that can be returned, or -1 if there should be no limit.
     */
    public long getMaxSize()
    {
        return _maxSize;
    }


    /**
     * @return true if the data should be returned in ascending order, false otherwise.
     */
    public boolean getAscending()
    {
        return _ascending;
    }


    /**
     * @return The filters.
     */
    public Map<String, String> getFilters()
    {
        return _filters;
    }


    /**
     * @return The type of client that sent the request.
     */
    public String getClientType()
    {
        return _clientType;
    }


    /**
     * Used to convert a Protobuf timestamp to a LocalDateTime.
     *
     * @param timestamp The Protobuf timestamp (which may be null).
     * @return The date/time for timestamp, or null if timestamp is null.
     */
    private static LocalDateTime getLocalDateTime(TimestampProto.Timestamp timestamp)
    {
        if (timestamp == null)
        {
            return null;
        }
        else
        {
            return LocalDateTime.ofEpochSecond(timestamp.getEpochSecond(), timestamp.getNanosecond(), ZoneOffset.UTC);
        }
    }


    /**
     * Sets the state of this from a provided Protobuf object.
     *
     * @param timeSeriesRequest The Protobuf object to set the state from.
     */
    void readProto(TimeSeriesRequestProto.TimeSeriesRequest timeSeriesRequest)
    {
        // Get the atomic properties

        _apiKey = timeSeriesRequest.getApiKey();
        _source = timeSeriesRequest.getSource();
        _category = timeSeriesRequest.getCategory();
        _label = timeSeriesRequest.getLabel();

        if (timeSeriesRequest.getStartSet())
        {
            _start = getLocalDateTime(timeSeriesRequest.getStart());
        }
        else
        {
            _start = null;
        }

        if (timeSeriesRequest.getEndSet())
        {
            _end = getLocalDateTime(timeSeriesRequest.getEnd());
        }
        else
        {
            _end = null;
        }

        _maxSize = timeSeriesRequest.getMaxSize();
        _ascending= timeSeriesRequest.getAscending();
        _clientType = timeSeriesRequest.getClientType();
        _filters = timeSeriesRequest.getFiltersMap();


        // Build the features list

        if (timeSeriesRequest.getFeaturesSet())
        {
            ProtocolStringList stringList = timeSeriesRequest.getFeaturesList();
            _features = new ArrayList<>();

            if (!stringList.isEmpty())
            {
                _features.addAll(stringList);
            }
        }
        else
        {
            _features = null;
        }
    }


    /**
     * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
     *
     * @param serialized The serialized state of an object.
     * @throws Exception If an error happened during the deserialization process.
     */
    private void readProto(byte[] serialized) throws Exception
    {
        TimeSeriesRequestProto.TimeSeriesRequest timeSeriesRequest = TimeSeriesRequestProto.TimeSeriesRequest.parseFrom(serialized);
        readProto(timeSeriesRequest);
    }


    /**
     * Creates a new instance using a byte[] created by serialize().
     *
     * @param serialized The byte[].
     * @throws Exception If there were any issues when deserializing the object.
     */
    public static TimeSeriesRequest deserialize(byte[] serialized) throws Exception
    {
        TimeSeriesRequest timeSeriesRequest = new TimeSeriesRequest();
        timeSeriesRequest.readProto(serialized);

        return timeSeriesRequest;
    }


    /**
     * @return A Protobuf object containing the serialised state of this.
     */
    TimeSeriesRequestProto.TimeSeriesRequest writeProtoObject()
    {
        TimeSeriesRequestProto.TimeSeriesRequest.Builder requestBuilder = TimeSeriesRequestProto.TimeSeriesRequest.newBuilder();
        requestBuilder.setApiKey(_apiKey).setSource(_source).setCategory(_category).setLabel(_label).setMaxSize(_maxSize).setAscending(_ascending);

        if (_start != null)
        {
            TimestampProto.Timestamp.Builder timestampBuilder = TimestampProto.Timestamp.newBuilder();
            timestampBuilder.setEpochSecond(_start.toEpochSecond(ZoneOffset.UTC));
            timestampBuilder.setNanosecond(_start.getNano());
            requestBuilder.setStart(timestampBuilder.build());
            requestBuilder.setStartSet(true);
        }
        else
        {
            requestBuilder.setStartSet(false);
        }

        if (_end != null)
        {
            TimestampProto.Timestamp.Builder timestampBuilder = TimestampProto.Timestamp.newBuilder();
            timestampBuilder.setEpochSecond(_end.toEpochSecond(ZoneOffset.UTC));
            timestampBuilder.setNanosecond(_end.getNano());
            requestBuilder.setEnd(timestampBuilder.build());
            requestBuilder.setEndSet(true);
        }
        else
        {
            requestBuilder.setEndSet(false);
        }

        if (_features == null)
        {
            requestBuilder.setFeaturesSet(false);
        }
        else
        {
            requestBuilder.addAllFeatures(_features);
            requestBuilder.setFeaturesSet(true);
        }

        requestBuilder.putAllFilters(_filters);
        requestBuilder.setClientType(_clientType);

        return requestBuilder.build();
    }


    /**
     * @return A byte[] containing the state of this serialised.
     */
    public byte[] serialize()
    {
        TimeSeriesRequestProto.TimeSeriesRequest timeSeriesRequest = writeProtoObject();

        return timeSeriesRequest.toByteArray();
    }
}
