package archipelagos.common.data.timeseries;

import archipelagos.common.protobuf.common.data.timeseries.TimeSeriesProto;
import archipelagos.common.protobuf.common.data.timeseries.TimeSeriesResponseProto;

/**
 * Represents the response to a time-series request.
 */
public final class TimeSeriesResponse
{
    private boolean _errorOccurred;
    private TimeSeries _timeSeries;
    private int _errorCode;
    private String _errorMessage;


    /**
     *
     */
    TimeSeriesResponse()
    {
    }


    /**
     * Called when data is to be returned.
     *
     * @param timeSeries The data to return.
     */
    public static TimeSeriesResponse of(TimeSeries timeSeries)
    {
        // Check the arguments

        if (timeSeries == null)
        {
            throw new IllegalArgumentException("timeSeries is null");
        }


        // Create the response and return

        TimeSeriesResponse timeSeriesResponse = new TimeSeriesResponse();
        timeSeriesResponse._errorOccurred = false;
        timeSeriesResponse._timeSeries = timeSeries;

        return timeSeriesResponse;
    }


    /**
     * Called when an error has occurred.
     *
     * @param errorCode    The error code.
     * @param errorMessage The error message (must be non-null).
     */
    public static TimeSeriesResponse of(int errorCode, String errorMessage)
    {
        // Check the arguments

        if (errorMessage == null)
        {
            throw new IllegalArgumentException("errorMessage is null");
        }


        // Create the response and return

        TimeSeriesResponse timeSeriesResponse = new TimeSeriesResponse();
        timeSeriesResponse._errorOccurred = true;
        timeSeriesResponse._errorCode = errorCode;
        timeSeriesResponse._errorMessage = errorMessage;

        return timeSeriesResponse;
    }


    /**
     * @return true if an error occurred, false otherwise.
     */
    public boolean errorOccurred()
    {
        return _errorOccurred;
    }


    /**
     * @return if an error did not occur then the data to return, null otherwise.
     */
    public TimeSeries getTimeSeries()
    {
        return _timeSeries;
    }


    /**
     * @return if an error occurred then the error code, 0 otherwise.
     */
    public int getErrorCode()
    {
        return _errorCode;
    }


    /**
     * @return if an error occurred then the error message, null otherwise.
     */
    public String getErrorMessage()
    {
        return _errorMessage;
    }


    /**
     * Sets the state of this from a provided Protobuf object.
     *
     * @param timeSeriesResponse The Protobuf object to set the state from.
     */
    void readProto(TimeSeriesResponseProto.TimeSeriesResponse timeSeriesResponse)
    {
        if (timeSeriesResponse.getErrorHasOccurred())
        {
            _errorOccurred = true;
            _errorCode = timeSeriesResponse.getErrorCode();
            _errorMessage = timeSeriesResponse.getErrorMessage();
        }
        else
        {
            _errorOccurred = false;
            _timeSeries = new TimeSeries();
            _timeSeries.readProto(timeSeriesResponse.getTimeSeries());
        }
    }


    /**
     * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
     *
     * @param serialized The serialized state of an object.
     * @throws Exception If an error happened during the deserialization process.
     */
    private void readProto(byte[] serialized) throws Exception
    {
        TimeSeriesResponseProto.TimeSeriesResponse timeSeriesResponse = TimeSeriesResponseProto.TimeSeriesResponse.parseFrom(serialized);
        readProto(timeSeriesResponse);
    }


    /**
     * Creates a new instance using a byte[] created by serialize().
     *
     * @param serialized The byte[].
     * @throws Exception If there were any issues when deserializing the object.
     */
    public static TimeSeriesResponse deserialize(byte[] serialized) throws Exception
    {
        TimeSeriesResponse timeSeriesResponse = new TimeSeriesResponse();
        timeSeriesResponse.readProto(serialized);

        return timeSeriesResponse;
    }


    /**
     * @return A Protobuf object containing the serialised state of this.
     */
    TimeSeriesResponseProto.TimeSeriesResponse writeProtoObject()
    {
        if (_errorOccurred)
        {
            return TimeSeriesResponseProto.TimeSeriesResponse.newBuilder().setErrorHasOccurred(true).setErrorCode(_errorCode).setErrorMessage(_errorMessage).build();
        }
        else
        {
            TimeSeriesProto.TimeSeries timeSeries = _timeSeries.writeProtoObject();
            return TimeSeriesResponseProto.TimeSeriesResponse.newBuilder().setErrorHasOccurred(false).setTimeSeries(timeSeries).build();
        }
    }


    /**
     * @return A byte[] containing the state of this serialised.
     */
    public byte[] serialize()
    {
        TimeSeriesResponseProto.TimeSeriesResponse timeSeriesResponse = writeProtoObject();

        return timeSeriesResponse.toByteArray();
    }
}
