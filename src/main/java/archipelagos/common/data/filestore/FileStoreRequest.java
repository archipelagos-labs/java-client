package archipelagos.common.data.filestore;

import archipelagos.common.data.DataUtils;
import archipelagos.common.platform.ClientType;
import archipelagos.common.protobuf.common.data.filestore.FileStoreRequestProto;

/**
 * Represents a file store request.
 */
public final class FileStoreRequest
{
	private String _apiKey;
	private String _source;
	private String _category;
	private String _label;
	private String _name;
	private String _clientType;


	/**
	 *
	 */
	FileStoreRequest()
	{
	}


	/**
	 * @param source   The source for the file store.
	 * @param category The category for the file store.
	 * @param label    The label for the file store.
	 * @param name     The name of the file.
	 */
	public static FileStoreRequest of(String source, String category, String label, String name)
	{
		// Check the arguments

		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}

		if (!DataUtils.validToken(source))
		{
			throw new IllegalArgumentException("source is not valid");
		}

		if (category == null)
		{
			throw new IllegalArgumentException("category is null");
		}

		if (!DataUtils.validToken(category))
		{
			throw new IllegalArgumentException("category is not valid");
		}

		if (label == null)
		{
			throw new IllegalArgumentException("label is null");
		}

		if (!DataUtils.validToken(label))
		{
			throw new IllegalArgumentException("label is not valid");
		}

		if (name == null)
		{
			throw new IllegalArgumentException("name is null");
		}


		// Create the request and return

		FileStoreRequest request = new FileStoreRequest();
		request._apiKey = null;
		request._source = source;
		request._category = category;
		request._label = label;
		request._name = name;
		request._clientType = ClientType.JAVA_CLIENT;

		return request;
	}


	/**
	 * @param source   The source for the collection.
	 * @param category The category for the collection.
	 * @param label    The label for the collection.
	 * @param name     The name of the file.
	 * @param apiKey   The API key.
	 */
	public static FileStoreRequest of(String source, String category, String label, String name, String apiKey)
	{
		// Check the arguments

		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}

		if (!DataUtils.validToken(source))
		{
			throw new IllegalArgumentException("source is not valid");
		}

		if (category == null)
		{
			throw new IllegalArgumentException("category is null");
		}

		if (!DataUtils.validToken(category))
		{
			throw new IllegalArgumentException("category is not valid");
		}

		if (label == null)
		{
			throw new IllegalArgumentException("label is null");
		}

		if (!DataUtils.validToken(label))
		{
			throw new IllegalArgumentException("label is not valid");
		}

		if (name == null)
		{
			throw new IllegalArgumentException("name is null");
		}

		if (apiKey == null)
		{
			throw new IllegalArgumentException("apiKey is null");
		}

		if (!DataUtils.validToken(apiKey))
		{
			throw new IllegalArgumentException("apiKey is not a valid API Key");
		}


		// Create the request and return

		FileStoreRequest request = new FileStoreRequest();
		request._apiKey = apiKey;
		request._source = source;
		request._category = category;
		request._label = label;
		request._name = name;
		request._clientType = ClientType.JAVA_CLIENT;

		return request;
	}


	/**
	 * @return The API key.
	 */
	public String getApiKey()
	{
		return _apiKey;
	}


	/**
	 * Sets the API Key
	 *
	 * @param apiKey The new API key.
	 */
	public void setApiKey(String apiKey)
	{
		_apiKey = apiKey;
	}


	/**
	 * @return The source for the file.
	 */
	public String getSource()
	{
		return _source;
	}


	/**
	 * @return The category for the file.
	 */
	public String getCategory()
	{
		return _category;
	}


	/**
	 * @return The label for the file.
	 */
	public String getLabel()
	{
		return _label;
	}


	/**
	 * @return The name of the file.
	 */
	public String getName()
	{
		return _name;
	}


	/**
	 * @return The type of client that sent the request.
	 */
	public String getClientType()
	{
		return _clientType;
	}


	/**
	 * Sets the state of this from a provided Protobuf object.
	 *
	 * @param request The Protobuf object to set the state from.
	 */
	void readProto(FileStoreRequestProto.FileStoreRequest request)
	{
		_apiKey = request.getApiKey();
		_source = request.getSource();
		_category = request.getCategory();
		_label = request.getLabel();
		_name = request.getName();
		_clientType = request.getClientType();
	}


	/**
	 * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
	 *
	 * @param serialized The serialized state of an object.
	 * @throws Exception If an error happened during the deserialization process.
	 */
	private void readProto(byte[] serialized) throws Exception
	{
		FileStoreRequestProto.FileStoreRequest request = FileStoreRequestProto.FileStoreRequest.parseFrom(serialized);
		readProto(request);
	}


	/**
	 * Creates a new instance using a byte[] created by serialize().
	 *
	 * @param serialized The byte[].
	 * @throws Exception If there were any issues when deserializing the object.
	 */
	public static FileStoreRequest deserialize(byte[] serialized) throws Exception
	{
		FileStoreRequest request = new FileStoreRequest();
		request.readProto(serialized);

		return request;
	}


	/**
	 * @return A Protobuf object containing the serialised state of this.
	 */
	FileStoreRequestProto.FileStoreRequest writeProtoObject()
	{
		FileStoreRequestProto.FileStoreRequest.Builder requestBuilder = FileStoreRequestProto.FileStoreRequest.newBuilder();
		requestBuilder.setApiKey(_apiKey).setSource(_source).setCategory(_category).setLabel(_label).setName(_name).setClientType(_clientType);

		return requestBuilder.build();
	}


	/**
	 * @return A byte[] containing the state of this serialised.
	 */
	public byte[] serialize()
	{
		FileStoreRequestProto.FileStoreRequest request = writeProtoObject();

		return request.toByteArray();
	}
}
