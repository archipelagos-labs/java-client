package archipelagos.common.data.filestore;

import archipelagos.common.data.DataUtils;
import archipelagos.common.protobuf.common.TimestampProto;
import archipelagos.common.protobuf.common.data.filestore.FileMetadataProto;

import java.time.*;

/**
 * Represents the metadata about a file store file.
 */
public final class FileMetadata
{
	private String _source;
	private String _category;
	private String _label;
	private String _name;
	private long _size;
	private boolean _premium;
	private LocalDateTime _created;
	private int _hashCode;


	/**
	 * Should be used when the created date/time is now.
	 *
	 * @param source   The source for the file.
	 * @param category The category for the file.
	 * @param label    The label for the file.
	 * @param name     The name of the file.
	 * @param size     The size (in bytes) of the file.
	 * @param premium  true if the associated file store is premium, false otherwise.
	 */
	public static FileMetadata of(String source, String category, String label, String name, long size, boolean premium)
	{
		LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
		return new FileMetadata(source, category, label, name, size, premium, now);
	}


	/**
	 * @param source   The source for the file.
	 * @param category The category for the file.
	 * @param label    The label for the file.
	 * @param name     The name of the file.
	 * @param size     The size (in bytes) of the file.
	 * @param premium  true if the associated file store is premium, false otherwise.
	 * @param created  When (in UTC) the file was created.
	 */
	public static FileMetadata of(String source, String category, String label, String name, long size, boolean premium, LocalDateTime created)
	{
		return new FileMetadata(source, category, label, name, size, premium, created);
	}


	/**
	 * Creates an object without initializing any properties.
	 */
	FileMetadata()
	{
		buildHashCode();
	}


	/**
	 * @param source   The source for the file.
	 * @param category The category for the file.
	 * @param label    The label for the file.
	 * @param name     The name of the file.
	 * @param size     The size (in bytes) of the file.
	 * @param premium  true if the associated file store is premium, false otherwise.
	 * @param created  The date/time in UTC when this was created.
	 */
	private FileMetadata(String source, String category, String label, String name, long size, boolean premium, LocalDateTime created)
	{
		// Check the arguments

		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}
		else
		{
			if (!DataUtils.validToken(source))
			{
				throw new IllegalArgumentException("source is not valid");
			}
		}

		if (category == null)
		{
			throw new IllegalArgumentException("category is null");
		}
		else
		{
			if (!DataUtils.validToken(category))
			{
				throw new IllegalArgumentException("category is not valid");
			}
		}

		if (label == null)
		{
			throw new IllegalArgumentException("label is null");
		}
		else
		{
			if (!DataUtils.validToken(label))
			{
				throw new IllegalArgumentException("label is not valid");
			}
		}

		if (name == null)
		{
			throw new IllegalArgumentException("name is null");
		}

		if (created == null)
		{
			throw new IllegalArgumentException("created is null");
		}


		// Store the data

		_source = source;
		_category = category;
		_label = label;
		_name = name;
		_size = size;
		_premium = premium;
		_created = created;

		buildHashCode();
	}


	/**
	 * Builds and caches the hash code associated with this.
	 */
	private void buildHashCode()
	{
		int prime = 31, result = 1;

		result = prime * result + ((_source == null) ? 0 : _source.hashCode());
		result = prime * result + ((_category == null) ? 0 : _category.hashCode());
		result = prime * result + ((_label == null) ? 0 : _label.hashCode());
		result = prime * result + ((_name == null) ? 0 : _name.hashCode());

		_hashCode = result;
	}


	/**
	 * @return The source for the file.
	 */
	public String getSource()
	{
		return _source;
	}


	/**
	 * @return The category for the file.
	 */
	public String getCategory()
	{
		return _category;
	}


	/**
	 * @return The label for the file.
	 */
	public String getLabel()
	{
		return _label;
	}


	/**
	 * @return The name of the file.
	 */
	public String getName()
	{
		return _name;
	}


	/**
	 * @return The size (in bytes) of the file.
	 */
	public long getSize()
	{
		return _size;
	}


	/**
	 * @return true if the associated file store is premium, false otherwise.
	 */
	public boolean isPremium()
	{
		return _premium;
	}


	/**
	 * @return The date/time in UTC when this was created.
	 */
	public LocalDateTime getCreated()
	{
		return _created;
	}


	/**
	 * @see Object
	 */
	@Override
	public boolean equals(Object o)
	{
		// Check that the object is non-null and of the correct type

		if (o instanceof FileMetadata)
		{
			FileMetadata metadata = (FileMetadata) o;


			// Evaluate the source

			if (_source == null)
			{
				if (metadata._source != null)
				{
					return false;
				}
			}
			else
			{
				if (!_source.equals(metadata._source))
				{
					return false;
				}
			}


			// Evaluate the category

			if (_category == null)
			{
				if (metadata._category != null)
				{
					return false;
				}
			}
			else
			{
				if (!_category.equals(metadata._category))
				{
					return false;
				}
			}


			// Evaluate the label

			if (_label == null)
			{
				if (metadata._label != null)
				{
					return false;
				}
			}
			else
			{
				if (!_label.equals(metadata._label))
				{
					return false;
				}
			}


			// Evaluate the name

			if (_name == null)
			{
				if (metadata._name != null)
				{
					return false;
				}
			}
			else
			{
				if (!_name.equals(metadata._name))
				{
					return false;
				}
			}


			// Everything matches so return true

			return true;
		}
		else
		{
			return false;
		}
	}


	/**
	 * @see Object
	 */
	@Override
	public int hashCode()
	{
		return _hashCode;
	}


	/**
	 * @see Object
	 */
	@Override
	public String toString()
	{
		String toReturn = "Source = \"";
		toReturn += _source;
		toReturn += "\", Category = \"";
		toReturn += _category;
		toReturn += "\", Label = \"";
		toReturn += _label;
		toReturn += "\", Name = \"";
		toReturn += _name;
		toReturn += "\", Size = ";
		toReturn += _size;
		toReturn += ", Premium = ";
		toReturn += _premium;
		toReturn += ", Created = ";
		toReturn += DataUtils.getDateTime(_created);

		return toReturn;
	}


	/**
	 * Used to convert a Protobuf timestamp to a Date.
	 *
	 * @param timestamp The Protobuf timestamp (which may be null).
	 * @return The date/time for timestamp, or null if timestamp is null.
	 */
	private static LocalDateTime getDate(TimestampProto.Timestamp timestamp)
	{
		if (timestamp == null)
		{
			return null;
		}
		else
		{
			return LocalDateTime.ofEpochSecond(timestamp.getEpochSecond(), timestamp.getNanosecond(), ZoneOffset.UTC);
		}
	}


	/**
	 * Sets the state of this from a provided Protobuf object.
	 *
	 * @param metadata The Protobuf object to set the state from.
	 */
	void readProto(FileMetadataProto.FileMetadata metadata)
	{
		// Set any simple attributes

		_source = metadata.getSource();
		_category = metadata.getCategory();
		_label = metadata.getLabel();
		_name = metadata.getName();
		_size = metadata.getSize();
		_premium = metadata.getPremium();


		// Get any date/times

		_created = getDate(metadata.getCreated());


		// Rebuild the hashcode

		buildHashCode();
	}


	/**
	 * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
	 *
	 * @param serialized The serialized state of an object.
	 * @throws Exception If an error happened during the deserialization process.
	 */
	private void readProto(byte[] serialized) throws Exception
	{
		FileMetadataProto.FileMetadata metadata = FileMetadataProto.FileMetadata.parseFrom(serialized);
		readProto(metadata);
	}


	/**
	 * Creates a new instance using a byte[] created by serialize().
	 *
	 * @param serialized The byte[].
	 * @throws Exception If there were any issues when deserializing the object.
	 */
	public static FileMetadata deserialize(byte[] serialized) throws Exception
	{
		FileMetadata metadata = new FileMetadata();
		metadata.readProto(serialized);

		return metadata;
	}


	/**
	 * Used to convert a Protobuf timestamp to a LocalDateTime.
	 *
	 * @param dateTime The date/time.
	 * @return The timestamp for date/time.
	 */
	private static TimestampProto.Timestamp getTimestamp(LocalDateTime dateTime)
	{
		return TimestampProto.Timestamp.newBuilder().setEpochSecond(dateTime.toEpochSecond(ZoneOffset.UTC)).setNanosecond(dateTime.getNano()).build();
	}


	/**
	 * @return A Protobuf object containing the serialised state of this.
	 */
	FileMetadataProto.FileMetadata writeProtoObject()
	{
		FileMetadataProto.FileMetadata.Builder builder;
		builder = FileMetadataProto.FileMetadata.newBuilder();
		builder.setSource(_source).setCategory(_category).setLabel(_label).setName(_name).setSize(_size).setPremium(_premium);
		builder.setCreated(getTimestamp(_created)).build();

		return builder.build();
	}


	/**
	 * @return A byte[] containing the state of this serialised.
	 */
	public byte[] serialize()
	{
		FileMetadataProto.FileMetadata metadata = writeProtoObject();

		return metadata.toByteArray();
	}
}
