package archipelagos.common.data.filestore;

import archipelagos.common.data.DataUtils;
import archipelagos.common.platform.ClientType;
import archipelagos.common.protobuf.common.data.filestore.FileStoreMetadataRequestProto;

/**
 * Represents a file store metadata request.
 */
public final class FileStoreMetadataRequest
{
    private String _apiKey;
    private String _source;
    private String _category;
    private String _label;
    private String _clientType;


    /**
     *
     */
    FileStoreMetadataRequest()
    {
    }


    /**
     * @param source   The source for the file store.
     * @param category The code for the file store.
     * @param label    The ID for the file store.
     */
    public static FileStoreMetadataRequest of(String source, String category, String label)
    {
        // Check the arguments

        if (source == null)
        {
            throw new IllegalArgumentException("source is null");
        }

        if(!DataUtils.validToken(source))
        {
            throw new IllegalArgumentException("source is not valid");
        }

        if (category == null)
        {
            throw new IllegalArgumentException("category is null");
        }

        if(!DataUtils.validToken(category))
        {
            throw new IllegalArgumentException("category is not valid");
        }

        if (label == null)
        {
            throw new IllegalArgumentException("label is null");
        }

        if(!DataUtils.validToken(label))
        {
            throw new IllegalArgumentException("label is not valid");
        }


        // Create the request and return

        FileStoreMetadataRequest metadataRequest = new FileStoreMetadataRequest();
        metadataRequest._apiKey = null;
        metadataRequest._source = source;
        metadataRequest._category = category;
        metadataRequest._label = label;
        metadataRequest._clientType = ClientType.JAVA_CLIENT;

        return metadataRequest;
    }


    /**
     * @param source   The source for the file store.
     * @param category The category for the file store.
     * @param label    The label for the file store.
     * @param apiKey   The API key.
     */
    public static FileStoreMetadataRequest of(String source, String category, String label, String apiKey)
    {
        // Check the arguments

        if (source == null)
        {
            throw new IllegalArgumentException("source is null");
        }

        if(!DataUtils.validToken(source))
        {
            throw new IllegalArgumentException("source is not valid");
        }

        if (category == null)
        {
            throw new IllegalArgumentException("category is null");
        }

        if(!DataUtils.validToken(category))
        {
            throw new IllegalArgumentException("category is not valid");
        }

        if (label == null)
        {
            throw new IllegalArgumentException("label is null");
        }

        if(!DataUtils.validToken(label))
        {
            throw new IllegalArgumentException("label is not valid");
        }

        if (apiKey == null)
        {
            throw new IllegalArgumentException("apiKey is null");
        }

        if(!DataUtils.validToken(apiKey))
        {
            throw new IllegalArgumentException("apiKey is not a valid API Key");
        }


        // Create the request and return

        FileStoreMetadataRequest metadataRequest = new FileStoreMetadataRequest();
        metadataRequest._apiKey = apiKey;
        metadataRequest._source = source;
        metadataRequest._category = category;
        metadataRequest._label = label;
        metadataRequest._clientType = ClientType.JAVA_CLIENT;

        return metadataRequest;
    }


    /**
     * @return The API key.
     */
    public String getApiKey()
    {
        return _apiKey;
    }


    /**
     * Sets the API Key
     *
     * @param apiKey The new API key.
     */
    public void setApiKey(String apiKey)
    {
        _apiKey = apiKey;
    }


    /**
     * @return The source for the file store.
     */
    public String getSource()
    {
        return _source;
    }


    /**
     * @return The category for the file store.
     */
    public String getCategory()
    {
        return _category;
    }


    /**
     * @return The label for the file store.
     */
    public String getLabel()
    {
        return _label;
    }


    /**
     * @return The type of client that sent the request.
     */
    public String getClientType()
    {
        return _clientType;
    }


    /**
     * Sets the state of this from a provided Protobuf object.
     *
     * @param request The Protobuf object to set the state from.
     */
    void readProto(FileStoreMetadataRequestProto.FileStoreMetadataRequest request)
    {
        _apiKey = request.getApiKey();
        _source = request.getSource();
        _category = request.getCategory();
        _label = request.getLabel();
        _clientType = request.getClientType();
    }


    /**
     * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
     *
     * @param serialized The serialized state of an object.
     * @throws Exception If an error happened during the deserialization process.
     */
    private void readProto(byte[] serialized) throws Exception
    {
        FileStoreMetadataRequestProto.FileStoreMetadataRequest request = FileStoreMetadataRequestProto.FileStoreMetadataRequest.parseFrom(serialized);
        readProto(request);
    }


    /**
     * Creates a new instance using a byte[] created by serialize().
     *
     * @param serialized The byte[].
     * @throws Exception If there were any issues when deserializing the object.
     */
    public static FileStoreMetadataRequest deserialize(byte[] serialized) throws Exception
    {
        FileStoreMetadataRequest fileStoreMetadataRequest = new FileStoreMetadataRequest();
        fileStoreMetadataRequest.readProto(serialized);

        return fileStoreMetadataRequest;
    }


    /**
     * @return A Protobuf object containing the serialised state of this.
     */
    FileStoreMetadataRequestProto.FileStoreMetadataRequest writeProtoObject()
    {
        FileStoreMetadataRequestProto.FileStoreMetadataRequest.Builder requestBuilder = FileStoreMetadataRequestProto.FileStoreMetadataRequest.newBuilder();
        return requestBuilder.setApiKey(_apiKey).setSource(_source).setCategory(_category).setLabel(_label).setClientType(_clientType).build();
    }


    /**
     * @return A byte[] containing the state of this serialised.
     */
    public byte[] serialize()
    {
        FileStoreMetadataRequestProto.FileStoreMetadataRequest request = writeProtoObject();

        return request.toByteArray();
    }
}
