package archipelagos.common.data.filestore;

import archipelagos.common.protobuf.common.data.filestore.FileStoreMetadataProto;
import archipelagos.common.protobuf.common.data.filestore.FileStoreMetadataResponseProto;

/**
 * Represents the response to a file store metadata request.
 */
public final class FileStoreMetadataResponse
{
    private boolean _errorOccurred;
    private FileStoreMetadata _metadata;
    private int _errorCode;
    private String _errorMessage;


    /**
     *
     */
    FileStoreMetadataResponse()
    {
    }


    /**
     * Called when data is to be returned.
     *
     * @param metadata The data to return.
     */
    public static FileStoreMetadataResponse of(FileStoreMetadata metadata)
    {
        // Check the arguments

        if (metadata == null)
        {
            throw new IllegalArgumentException("metadata is null");
        }


        // Create the response and return

        FileStoreMetadataResponse metadataResponse = new FileStoreMetadataResponse();
        metadataResponse._errorOccurred = false;
        metadataResponse._metadata = metadata;

        return metadataResponse;
    }


    /**
     * Called when an error has occurred.
     *
     * @param errorCode    The error code.
     * @param errorMessage The error message (must be non-null).
     */
    public static FileStoreMetadataResponse of(int errorCode, String errorMessage)
    {
        // Check the arguments

        if (errorMessage == null)
        {
            throw new IllegalArgumentException("metadata is null");
        }


        // Create the response and return

        FileStoreMetadataResponse metadataResponse = new FileStoreMetadataResponse();
        metadataResponse._errorOccurred = true;
        metadataResponse._errorCode = errorCode;
        metadataResponse._errorMessage = errorMessage;

        return metadataResponse;
    }


    /**
     * @return true if an error occurred, false otherwise.
     */
    public boolean errorOccurred()
    {
        return _errorOccurred;
    }


    /**
     * @return If an error did not occur then the data to return, null otherwise.
     */
    public FileStoreMetadata getMetadata()
    {
        return _metadata;
    }


    /**
     * @return If an error occurred then the error code, 0 otherwise.
     */
    public int getErrorCode()
    {
        return _errorCode;
    }


    /**
     * @return If an error occurred then the error message, null otherwise.
     */
    public String getErrorMessage()
    {
        return _errorMessage;
    }


    /**
     * Sets the state of this from a provided Protobuf object.
     *
     * @param metadataResponse The Protobuf object to set the state from.
     */
    void readProto(FileStoreMetadataResponseProto.FileStoreMetadataResponse metadataResponse)
    {
        if (metadataResponse.getErrorHasOccurred())
        {
            _errorOccurred = true;
            _errorCode = metadataResponse.getErrorCode();
            _errorMessage = metadataResponse.getErrorMessage();
        }
        else
        {
            _errorOccurred = false;
            _metadata = new FileStoreMetadata();
            _metadata.readProto(metadataResponse.getMetadata());
        }
    }


    /**
     * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
     *
     * @param serialized The serialized state of an object.
     * @throws Exception If an error happened during the deserialization process.
     */
    private void readProto(byte[] serialized) throws Exception
    {
        FileStoreMetadataResponseProto.FileStoreMetadataResponse response = FileStoreMetadataResponseProto.FileStoreMetadataResponse.parseFrom(serialized);
        readProto(response);
    }


    /**
     * Creates a new instance using a byte[] created by serialize().
     *
     * @param serialized The byte[].
     * @throws Exception If there were any issues when deserializing the object.
     */
    public static FileStoreMetadataResponse deserialize(byte[] serialized) throws Exception
    {
        FileStoreMetadataResponse metadataResponse = new FileStoreMetadataResponse();
        metadataResponse.readProto(serialized);

        return metadataResponse;
    }


    /**
     * @return A Protobuf object containing the serialised state of this.
     */
    FileStoreMetadataResponseProto.FileStoreMetadataResponse writeProtoObject()
    {
        if (_errorOccurred)
        {
            return FileStoreMetadataResponseProto.FileStoreMetadataResponse.newBuilder().setErrorHasOccurred(true).setErrorCode(_errorCode).setErrorMessage(_errorMessage).build();
        }
        else
        {
            FileStoreMetadataProto.FileStoreMetadata metadata = _metadata.writeProtoObject();
            return FileStoreMetadataResponseProto.FileStoreMetadataResponse.newBuilder().setErrorHasOccurred(false).setMetadata(metadata).build();
        }
    }


    /**
     * @return A byte[] containing the state of this serialised.
     */
    public byte[] serialize()
    {
        FileStoreMetadataResponseProto.FileStoreMetadataResponse response = writeProtoObject();

        return response.toByteArray();
    }
}
