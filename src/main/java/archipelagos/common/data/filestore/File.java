package archipelagos.common.data.filestore;

/**
 * Represents a file store file.
 */
public final class File
{
	private final byte[] _contents;
	private final FileMetadata _metadata;


	/**
	 * @param contents The contents of the file.
	 * @param metadata The metadata for the file.
	 */
	public static File of(byte[] contents, FileMetadata metadata)
	{
		return new File(contents, metadata);
	}


	/**
	 * @param contents The contents of the file.
	 * @param metadata The metadata for the file.
	 */
	private File(byte[] contents, FileMetadata metadata)
	{
		// Check the arguments

		if (contents == null)
		{
			throw new IllegalArgumentException("contents is null");
		}

		if (metadata == null)
		{
			throw new IllegalArgumentException("metadata is null");
		}


		// Store the data

		_contents = contents;
		_metadata = metadata;
	}


	/**
	 * @return The contents of the file.
	 */
	public byte[] getContents()
	{
		return _contents;
	}


	/**
	 * @return The metadata for the file.
	 */
	public FileMetadata getMetadata()
	{
		return _metadata;
	}
}
