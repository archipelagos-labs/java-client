package archipelagos.common.data.filestore;

import archipelagos.common.data.DataUtils;
import archipelagos.common.platform.ClientType;
import archipelagos.common.protobuf.common.data.filestore.FileMetadataRequestProto;

/**
 * Represents a request to retrieve the metadata for files in a specified File Store that match a given pattern.
 */
public final class FileMetadataRequest
{
	private String _apiKey;
	private String _source;
	private String _category;
	private String _label;
	private String _pattern;
	private String _clientType;


	/**
	 *
	 */
	FileMetadataRequest()
	{
	}


	/**
	 * @param source   The source for the file store.
	 * @param category The category for the file store.
	 * @param label    The label for the file store.
	 * @param pattern  The pattern that the name of a file whose metadata is returned should match; null will match all files.
	 */
	public static FileMetadataRequest of(String source, String category, String label, String pattern)
	{
		return of(source, category, label, pattern, null);
	}


	/**
	 * @param source   The source for the file store.
	 * @param category The category for the file store.
	 * @param label    The label for the file store.
	 * @param pattern  The pattern that the name of a file whose metadata is returned should match; null will match all files.
	 * @param apiKey   The API key.
	 */
	public static FileMetadataRequest of(String source, String category, String label, String pattern, String apiKey)
	{
		// Check the arguments

		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}

		if (!DataUtils.validToken(source))
		{
			throw new IllegalArgumentException("source is not valid");
		}

		if (category == null)
		{
			throw new IllegalArgumentException("category is null");
		}

		if (!DataUtils.validToken(category))
		{
			throw new IllegalArgumentException("category is not valid");
		}

		if (label == null)
		{
			throw new IllegalArgumentException("label is null");
		}

		if (!DataUtils.validToken(label))
		{
			throw new IllegalArgumentException("label is not valid");
		}


		// Create the request and return

		FileMetadataRequest metadataRequest = new FileMetadataRequest();
		metadataRequest._apiKey = null;
		metadataRequest._source = source;
		metadataRequest._category = category;
		metadataRequest._label = label;
		metadataRequest._pattern = pattern;
		metadataRequest._apiKey = apiKey;
		metadataRequest._clientType = ClientType.JAVA_CLIENT;

		return metadataRequest;
	}


	/**
	 * @return The API key.
	 */
	public String getApiKey()
	{
		return _apiKey;
	}


	/**
	 * Sets the API Key
	 *
	 * @param apiKey The new API key.
	 */
	public void setApiKey(String apiKey)
	{
		_apiKey = apiKey;
	}


	/**
	 * @return The source for the file store.
	 */
	public String getSource()
	{
		return _source;
	}


	/**
	 * @return The category for the file store.
	 */
	public String getCategory()
	{
		return _category;
	}


	/**
	 * @return The label for the file store.
	 */
	public String getLabel()
	{
		return _label;
	}


	/**
	 * @return The pattern that the name of a file whose metadata is returned should match; null will match all files.
	 */
	public String getPattern()
	{
		return _pattern;
	}


	/**
	 * @return The type of client that sent the request.
	 */
	public String getClientType()
	{
		return _clientType;
	}


	/**
	 * Sets the state of this from a provided Protobuf object.
	 *
	 * @param request The Protobuf object to set the state from.
	 */
	void readProto(FileMetadataRequestProto.FileMetadataRequest request)
	{
		_apiKey = request.getApiKey();
		_source = request.getSource();
		_category = request.getCategory();
		_label = request.getLabel();

		if (request.getPatternSet())
		{
			_pattern = request.getPattern();
		}
		else
		{
			_pattern = null;
		}

		_clientType = request.getClientType();
	}


	/**
	 * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
	 *
	 * @param serialized The serialized state of an object.
	 * @throws Exception If an error happened during the deserialization process.
	 */
	private void readProto(byte[] serialized) throws Exception
	{
		FileMetadataRequestProto.FileMetadataRequest request = FileMetadataRequestProto.FileMetadataRequest.parseFrom(serialized);
		readProto(request);
	}


	/**
	 * Creates a new instance using a byte[] created by serialize().
	 *
	 * @param serialized The byte[].
	 * @throws Exception If there were any issues when deserializing the object.
	 */
	public static FileMetadataRequest deserialize(byte[] serialized) throws Exception
	{
		FileMetadataRequest request = new FileMetadataRequest();
		request.readProto(serialized);

		return request;
	}


	/**
	 * @return A Protobuf object containing the serialised state of this.
	 */
	FileMetadataRequestProto.FileMetadataRequest writeProtoObject()
	{
		FileMetadataRequestProto.FileMetadataRequest.Builder requestBuilder = FileMetadataRequestProto.FileMetadataRequest.newBuilder();

		if(_pattern == null)
		{
			return requestBuilder.setApiKey(_apiKey).setSource(_source).setCategory(_category).setLabel(_label).setPatternSet(false).setClientType(_clientType).build();
		}
		else
		{
			return requestBuilder.setApiKey(_apiKey).setSource(_source).setCategory(_category).setLabel(_label).setPatternSet(true).setPattern(_pattern).setClientType(_clientType).build();
		}
	}


	/**
	 * @return A byte[] containing the state of this serialised.
	 */
	public byte[] serialize()
	{
		FileMetadataRequestProto.FileMetadataRequest request = writeProtoObject();

		return request.toByteArray();
	}
}
