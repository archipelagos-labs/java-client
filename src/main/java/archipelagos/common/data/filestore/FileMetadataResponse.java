package archipelagos.common.data.filestore;

import archipelagos.common.protobuf.common.data.filestore.FileMetadataProto;
import archipelagos.common.protobuf.common.data.filestore.FileMetadataResponseProto;

import java.util.*;

/**
 * Represents the response to a file store metadata request.
 */
public final class FileMetadataResponse
{
	private boolean _errorOccurred;
	private List<FileMetadata> _metadata;
	private int _errorCode;
	private String _errorMessage;


	/**
	 *
	 */
	FileMetadataResponse()
	{
	}


	/**
	 * Called when data is to be returned.
	 *
	 * @param metadata The data to return.
	 */
	public static FileMetadataResponse of(List<FileMetadata> metadata)
	{
		// Check the arguments

		if (metadata == null)
		{
			throw new IllegalArgumentException("metadata is null");
		}


		// Create the response and return

		FileMetadataResponse metadataResponse = new FileMetadataResponse();
		metadataResponse._errorOccurred = false;
		metadataResponse._metadata = metadata;

		return metadataResponse;
	}


	/**
	 * Called when an error has occurred.
	 *
	 * @param errorCode    The error code.
	 * @param errorMessage The error message (must be non-null).
	 */
	public static FileMetadataResponse of(int errorCode, String errorMessage)
	{
		// Check the arguments

		if (errorMessage == null)
		{
			throw new IllegalArgumentException("metadata is null");
		}


		// Create the response and return

		FileMetadataResponse metadataResponse = new FileMetadataResponse();
		metadataResponse._errorOccurred = true;
		metadataResponse._errorCode = errorCode;
		metadataResponse._errorMessage = errorMessage;

		return metadataResponse;
	}


	/**
	 * @return true if an error occurred, false otherwise.
	 */
	public boolean errorOccurred()
	{
		return _errorOccurred;
	}


	/**
	 * @return If an error did not occur then the data to return, null otherwise.
	 */
	public List<FileMetadata> getMetadata()
	{
		return _metadata;
	}


	/**
	 * @return If an error occurred then the error code, 0 otherwise.
	 */
	public int getErrorCode()
	{
		return _errorCode;
	}


	/**
	 * @return If an error occurred then the error message, null otherwise.
	 */
	public String getErrorMessage()
	{
		return _errorMessage;
	}


	/**
	 * Sets the state of this from a provided Protobuf object.
	 *
	 * @param metadataResponse The Protobuf object to set the state from.
	 */
	void readProto(FileMetadataResponseProto.FileMetadataResponse metadataResponse)
	{
		if (metadataResponse.getErrorHasOccurred())
		{
			_errorOccurred = true;
			_errorCode = metadataResponse.getErrorCode();
			_errorMessage = metadataResponse.getErrorMessage();
		}
		else
		{
			_errorOccurred = false;
			_metadata = new ArrayList<>();

			for (FileMetadataProto.FileMetadata fileMetadataProto : metadataResponse.getMetadataList())
			{
				FileMetadata fileMetadata = new FileMetadata();
				fileMetadata.readProto(fileMetadataProto);
				_metadata.add(fileMetadata);
			}
		}
	}


	/**
	 * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
	 *
	 * @param serialized The serialized state of an object.
	 * @throws Exception If an error happened during the deserialization process.
	 */
	private void readProto(byte[] serialized) throws Exception
	{
		FileMetadataResponseProto.FileMetadataResponse response = FileMetadataResponseProto.FileMetadataResponse.parseFrom(serialized);
		readProto(response);
	}


	/**
	 * Creates a new instance using a byte[] created by serialize().
	 *
	 * @param serialized The byte[].
	 * @throws Exception If there were any issues when deserializing the object.
	 */
	public static FileMetadataResponse deserialize(byte[] serialized) throws Exception
	{
		FileMetadataResponse metadataResponse = new FileMetadataResponse();
		metadataResponse.readProto(serialized);

		return metadataResponse;
	}


	/**
	 * @return A Protobuf object containing the serialised state of this.
	 */
	FileMetadataResponseProto.FileMetadataResponse writeProtoObject()
	{
		if (_errorOccurred)
		{
			return FileMetadataResponseProto.FileMetadataResponse.newBuilder().setErrorHasOccurred(true).setErrorCode(_errorCode).setErrorMessage(_errorMessage).build();
		}
		else
		{
			List<FileMetadataProto.FileMetadata> metadata = new ArrayList<>();

			for (FileMetadata fileMetadata : getMetadata())
			{
				metadata.add(fileMetadata.writeProtoObject());
			}

			if( metadata.size() == 0)
			{
				return FileMetadataResponseProto.FileMetadataResponse.newBuilder().setErrorHasOccurred(false).build();
			}
			else
			{
				return FileMetadataResponseProto.FileMetadataResponse.newBuilder().setErrorHasOccurred(false).addAllMetadata(metadata).build();
			}
		}
	}


	/**
	 * @return A byte[] containing the state of this serialised.
	 */
	public byte[] serialize()
	{
		FileMetadataResponseProto.FileMetadataResponse response = writeProtoObject();

		return response.toByteArray();
	}
}
