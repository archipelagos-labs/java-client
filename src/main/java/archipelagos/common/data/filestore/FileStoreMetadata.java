package archipelagos.common.data.filestore;

import archipelagos.common.data.DataUtils;
import archipelagos.common.protobuf.common.TimestampProto;
import archipelagos.common.protobuf.common.data.filestore.FileStoreMetadataProto;

import java.time.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents the metadata about a file store.
 */
public final class FileStoreMetadata
{
   private String _source;
   private String _category;
   private String _label;
   private String _url;
   private String _summary;
   private String _description;
   private Map<String, String> _properties;
   private boolean _premium;
   private LocalDateTime _created;
   private LocalDateTime _edited;
   private int _hashCode;


   /**
    * Should be used when the created/edited date/time is now.
    *
    * @param source      The source for the file store.
    * @param category    The category for the file store.
    * @param label       The label for the file store.
    * @param url         The URL for the file store.
    * @param summary     The summary for the file store.
    * @param properties  Any properties that may be associated with the file store.
    * @param description The description for the file store.
    * @param premium     true if the collection is premium, false otherwise.
    */
   public static FileStoreMetadata of(String source, String category, String label, String url, String summary, String description, Map<String, String> properties, boolean premium)
   {
      LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
      return new FileStoreMetadata(source, category, label, url, summary, description, properties, premium, now, now);
   }


   /**
    * @param source      The source for the file store.
    * @param category    The category for the file store.
    * @param label       The label for the file store.
    * @param url         The URL for the file store.
    * @param summary     The summary of the file store.
    * @param description The description for the file store.
    * @param properties  Any properties that may be associated with the file store.
    * @param premium     true if the file store is premium, false otherwise.
    * @param created     When (in UTC) the file store was created.
    * @param edited      When (in UTC) the metadata for the file store was last edited.
    */
   public static FileStoreMetadata of(String source, String category, String label, String url, String summary, String description, Map<String, String> properties, boolean premium, LocalDateTime created, LocalDateTime edited)
   {
      return new FileStoreMetadata(source, category, label, url, summary, description, properties, premium, created, edited);
   }


   /**
    * Creates an object without initializing any properties.
    */
   FileStoreMetadata()
   {
      buildHashCode();
   }


   /**
    * @param source      The source for the file store.
    * @param category    The category for the file store.
    * @param label       The label for the file store.
    * @param url         The URL for the file store.
    * @param summary     The summary for the file store.
    * @param description The description for the file store.
    * @param properties  Any properties that may be associated with the file store.
    * @param premium     true if the file store is premium, false otherwise.
    * @param created     The date/time in UTC when this was created.
    * @param edited      The date/time in UTC when this was last edited.
    */
   private FileStoreMetadata(String source, String category, String label, String url, String summary, String description, Map<String, String> properties, boolean premium, LocalDateTime created, LocalDateTime edited)
   {
      // Check the arguments

      if (source == null)
      {
         throw new IllegalArgumentException("source is null");
      }
      else
      {
         if (!DataUtils.validToken(source))
         {
            throw new IllegalArgumentException("source is not valid");
         }
      }

      if (category == null)
      {
         throw new IllegalArgumentException("category is null");
      }
      else
      {
         if (!DataUtils.validToken(category))
         {
            throw new IllegalArgumentException("category is not valid");
         }
      }

      if (label == null)
      {
         throw new IllegalArgumentException("label is null");
      }
      else
      {
         if (!DataUtils.validToken(label))
         {
            throw new IllegalArgumentException("label is not valid");
         }
      }

      if (url == null)
      {
         throw new IllegalArgumentException("url is null");
      }

      if (summary == null)
      {
         throw new IllegalArgumentException("summary is null");
      }

      if (description == null)
      {
         throw new IllegalArgumentException("description is null");
      }

      if (properties == null)
      {
         throw new IllegalArgumentException("properties is null");
      }
      else
      {
         for (Map.Entry<String, String> property : properties.entrySet())
         {
            if (property.getKey() == null)
            {
               throw new IllegalArgumentException("properties contains one or more null properties");
            }
            else
            {
               if (!DataUtils.validToken(property.getKey()))
               {
                  throw new IllegalArgumentException(String.format("\"%s\" is not a valid name for a property", property.getKey()));
               }
            }

            if (property.getValue() == null)
            {
               throw new IllegalArgumentException("properties contains one or more null property descriptions");
            }
         }
      }

      if (created == null)
      {
         throw new IllegalArgumentException("created is null");
      }

      if (edited == null)
      {
         throw new IllegalArgumentException("edited is null");
      }
      else if (edited.compareTo(created) < 0)
      {
         throw new IllegalArgumentException("edited is before created");
      }


      // Store the data

      _source = source;
      _category = category;
      _label = label;
      _url = url;
      _summary = summary;
      _description = description;
      _properties = properties;
      _premium = premium;
      _created = created;
      _edited = edited;

      buildHashCode();
   }


   /**
    * Builds and caches the hash code associated with this.
    */
   private void buildHashCode()
   {
      int prime = 31, result = 1;

      result = prime * result + ((_source == null) ? 0 : _source.hashCode());
      result = prime * result + ((_category == null) ? 0 : _category.hashCode());
      result = prime * result + ((_label == null) ? 0 : _label.hashCode());

      _hashCode = result;
   }


   /**
    * @return The source for the file store.
    */
   public String getSource()
   {
      return _source;
   }


   /**
    * @return The category for the file store.
    */
   public String getCategory()
   {
      return _category;
   }


   /**
    * @return The label for the file store.
    */
   public String getLabel()
   {
      return _label;
   }


   /**
    * @return The URL for the file store.
    */
   public String getUrl()
   {
      return _url;
   }


   /**
    * @return The summary for the file store.
    */
   public String getSummary()
   {
      return _summary;
   }


   /**
    * @return The description for the file store.
    */
   public String getDescription()
   {
      return _description;
   }


   /**
    * @return Any properties that may be associated with the file store.
    */
   public Map<String, String> getProperties()
   {
      return _properties;
   }


   /**
    * @return true if the file store is premium, false otherwise.
    */
   public boolean isPremium()
   {
      return _premium;
   }


   /**
    * @return The date/time in UTC when this was created.
    */
   public LocalDateTime getCreated()
   {
      return _created;
   }


   /**
    * @return The date/time in UTC when this was last edited.
    */
   public LocalDateTime getEdited()
   {
      return _edited;
   }


   /**
    * @see Object
    */
   @Override
   public boolean equals(Object o)
   {
      // Check that the object is non-null and of the correct type

      if (o instanceof FileStoreMetadata)
      {
         FileStoreMetadata metadata = (FileStoreMetadata) o;


         // Evaluate the source

         if (_source == null)
         {
            if (metadata._source != null)
            {
               return false;
            }
         }
         else
         {
            if (!_source.equals(metadata._source))
            {
               return false;
            }
         }


         // Evaluate the category

         if (_category == null)
         {
            if (metadata._category != null)
            {
               return false;
            }
         }
         else
         {
            if (!_category.equals(metadata._category))
            {
               return false;
            }
         }


         // Evaluate the label

         if (_label == null)
         {
            if (metadata._label != null)
            {
               return false;
            }
         }
         else
         {
            if (!_label.equals(metadata._label))
            {
               return false;
            }
         }


         // Everything matches so return true

         return true;
      }
      else
      {
         return false;
      }
   }


   /**
    * @see Object
    */
   @Override
   public int hashCode()
   {
      return _hashCode;
   }


   /**
    * @see Object
    */
   @Override
   public String toString()
   {
      StringBuilder toReturn = new StringBuilder();

      toReturn.append("Source = \"");
      toReturn.append(_source);
      toReturn.append("\", Category = \"");
      toReturn.append(_category);
      toReturn.append("\", Label = \"");
      toReturn.append(_label);
      toReturn.append("\", URL = \"");
      toReturn.append(_url);
      toReturn.append("\", Summary = \"");
      toReturn.append(_summary);
      toReturn.append("\", Description = \"");
      toReturn.append(_description);
      toReturn.append("\", Properties = {");
      boolean isFirst = true;

      for (Map.Entry<String, String> property : _properties.entrySet())
      {
         if (isFirst)
         {
            isFirst = false;
         }
         else
         {
            toReturn.append(", ");
         }

         toReturn.append(property.getKey());
         toReturn.append("=");
         toReturn.append(property.getValue());
      }

      toReturn.append("}, Premium = ");
      toReturn.append(_premium);
      toReturn.append(", Created = ");
      toReturn.append(DataUtils.getDateTime(_created));
      toReturn.append(", Edited = ");
      toReturn.append(DataUtils.getDateTime(_edited));

      return toReturn.toString();
   }


   /**
    * Used to convert a Protobuf timestamp to a LocalDateTime.
    *
    * @param timestamp The Protobuf timestamp (which may be null).
    * @return The date/time for timestamp, or null if timestamp is null.
    */
   private static LocalDateTime getLocalDateTime(TimestampProto.Timestamp timestamp)
   {
      if (timestamp == null)
      {
         return null;
      }
      else
      {
         return LocalDateTime.ofEpochSecond(timestamp.getEpochSecond(), timestamp.getNanosecond(), ZoneOffset.UTC);
      }
   }


   /**
    * Sets the state of this from a provided Protobuf object.
    *
    * @param metadata The Protobuf object to set the state from.
    */
   void readProto(FileStoreMetadataProto.FileStoreMetadata metadata)
   {
      // Set any simple attributes

      _source = metadata.getSource();
      _category = metadata.getCategory();
      _label = metadata.getLabel();
      _url = metadata.getUrl();
      _summary = metadata.getSummary();
      _description = metadata.getDescription();
      _premium = metadata.getPremium();
      _properties = new HashMap<>();
      _properties.putAll(metadata.getPropertiesMap());


      // Get any date/times

      _created = getLocalDateTime(metadata.getCreated());
      _edited = getLocalDateTime(metadata.getEdited());


      // Rebuild the hashcode

      buildHashCode();
   }


   /**
    * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
    *
    * @param serialized The serialized state of an object.
    * @throws Exception If an error happened during the deserialization process.
    */
   private void readProto(byte[] serialized) throws Exception
   {
      FileStoreMetadataProto.FileStoreMetadata metadata = FileStoreMetadataProto.FileStoreMetadata.parseFrom(serialized);
      readProto(metadata);
   }


   /**
    * Creates a new instance using a byte[] created by serialize().
    *
    * @param serialized The byte[].
    * @throws Exception If there were any issues when deserializing the object.
    */
   public static FileStoreMetadata deserialize(byte[] serialized) throws Exception
   {
      FileStoreMetadata metadata = new FileStoreMetadata();
      metadata.readProto(serialized);

      return metadata;
   }


   /**
    * Used to convert a Protobuf timestamp to a LocalDateTime.
    *
    * @param dateTime The date/time.
    * @return The timestamp for date/time.
    */
   private static TimestampProto.Timestamp getTimestamp(LocalDateTime dateTime)
   {
      return TimestampProto.Timestamp.newBuilder().setEpochSecond(dateTime.toEpochSecond(ZoneOffset.UTC)).setNanosecond(dateTime.getNano()).build();
   }


   /**
    * @return A Protobuf object containing the serialised state of this.
    */
   FileStoreMetadataProto.FileStoreMetadata writeProtoObject()
   {
      FileStoreMetadataProto.FileStoreMetadata.Builder builder;
      builder = FileStoreMetadataProto.FileStoreMetadata.newBuilder();
      builder.setSource(_source).setCategory(_category).setLabel(_label).setUrl(_url).setSummary(_summary).setDescription(_description).setPremium(_premium);
      builder.setCreated(getTimestamp(_created)).build();
      builder.setEdited(getTimestamp(_edited)).build();
      builder.putAllProperties(_properties);

      return builder.build();
   }


   /**
    * @return A byte[] containing the state of this serialised.
    */
   public byte[] serialize()
   {
      FileStoreMetadataProto.FileStoreMetadata metadata = writeProtoObject();

      return metadata.toByteArray();
   }
}
