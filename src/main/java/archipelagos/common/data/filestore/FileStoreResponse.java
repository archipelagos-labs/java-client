package archipelagos.common.data.filestore;

import archipelagos.common.protobuf.common.data.filestore.FileStoreResponseProto;
import com.google.protobuf.ByteString;

/**
 * Represents the response to a file store request.
 */
public final class FileStoreResponse
{
	private boolean _errorOccurred;
	private byte[] _file;
	private FileMetadata _metadata;
	private int _errorCode;
	private String _errorMessage;


	/**
	 *
	 */
	FileStoreResponse()
	{
	}


	/**
	 * Called when data is to be returned.
	 *
	 * @param file     The file to return.
	 * @param metadata The metadata for the file.
	 */
	public static FileStoreResponse of(byte[] file, FileMetadata metadata)
	{
		// Check the arguments

		if (file == null)
		{
			throw new IllegalArgumentException("file is null");
		}

		if (metadata == null)
		{
			throw new IllegalArgumentException("metadata is null");
		}


		// Create the response and return

		FileStoreResponse response = new FileStoreResponse();
		response._errorOccurred = false;
		response._file = file;
		response._metadata = metadata;

		return response;
	}


	/**
	 * Called when an error has occurred.
	 *
	 * @param errorCode    The error code.
	 * @param errorMessage The error message (must be non-null).
	 */
	public static FileStoreResponse of(int errorCode, String errorMessage)
	{
		// Check the arguments

		if (errorMessage == null)
		{
			throw new IllegalArgumentException("errorMessage is null");
		}


		// Create the response and return

		FileStoreResponse response = new FileStoreResponse();
		response._errorOccurred = true;
		response._errorCode = errorCode;
		response._errorMessage = errorMessage;

		return response;
	}


	/**
	 * @return true if an error occurred, false otherwise.
	 */
	public boolean errorOccurred()
	{
		return _errorOccurred;
	}


	/**
	 * @return If an error did not occur then the file to return, null otherwise.
	 */
	public byte[] getFile()
	{
		return _file;
	}


	/**
	 * @return If an error did not occur then the metadata for the file, null otherwise.
	 */
	public FileMetadata getMetadata()
	{
		return _metadata;
	}


	/**
	 * @return if an error occurred then the error code, 0 otherwise.
	 */
	public int getErrorCode()
	{
		return _errorCode;
	}


	/**
	 * @return if an error occurred then the error message, null otherwise.
	 */
	public String getErrorMessage()
	{
		return _errorMessage;
	}


	/**
	 * Sets the state of this from a provided Protobuf object.
	 *
	 * @param response The Protobuf object to set the state from.
	 */
	void readProto(FileStoreResponseProto.FileStoreResponse response)
	{
		if (response.getErrorHasOccurred())
		{
			_errorOccurred = true;
			_errorCode = response.getErrorCode();
			_errorMessage = response.getErrorMessage();
		}
		else
		{
			_errorOccurred = false;
			_file = response.getFile().toByteArray();
			_metadata = new FileMetadata();
			_metadata.readProto(response.getMetadata());
		}
	}


	/**
	 * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
	 *
	 * @param serialized The serialized state of an object.
	 * @throws Exception If an error happened during the deserialization process.
	 */
	private void readProto(byte[] serialized) throws Exception
	{
		FileStoreResponseProto.FileStoreResponse response = FileStoreResponseProto.FileStoreResponse.parseFrom(serialized);
		readProto(response);
	}


	/**
	 * Creates a new instance using a byte[] created by serialize().
	 *
	 * @param serialized The byte[].
	 * @throws Exception If there were any issues when deserializing the object.
	 */
	public static FileStoreResponse deserialize(byte[] serialized) throws Exception
	{
		FileStoreResponse response = new FileStoreResponse();
		response.readProto(serialized);

		return response;
	}


	/**
	 * @return A Protobuf object containing the serialised state of this.
	 */
	FileStoreResponseProto.FileStoreResponse writeProtoObject()
	{
		if (_errorOccurred)
		{
			return FileStoreResponseProto.FileStoreResponse.newBuilder().setErrorHasOccurred(true).setErrorCode(_errorCode).setErrorMessage(_errorMessage).build();
		}
		else
		{
			ByteString byteString = ByteString.copyFrom(_file);
			return FileStoreResponseProto.FileStoreResponse.newBuilder().setErrorHasOccurred(false).setFile(byteString).setMetadata(_metadata.writeProtoObject()).build();
		}
	}


	/**
	 * @return A byte[] containing the state of this serialised.
	 */
	public byte[] serialize()
	{
		FileStoreResponseProto.FileStoreResponse response = writeProtoObject();

		return response.toByteArray();
	}
}
