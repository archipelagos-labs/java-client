package archipelagos.common.data.filter;

import archipelagos.common.data.filter.constants.FeatureReference;
import archipelagos.common.data.filter.expressions.BooleanExpression;

/**
 * Provides support for parsing, validating and processing filters.
 */
public class FilterUtils
{
	/**
	 * Determines if a reference to a feature if of a valid format.
	 *
	 * @param feature The feature to be evaluated.
	 * @return true if the feature is valid, false otherwise.
	 */
	public static boolean isValidFeature(String feature)
	{
		try
		{
			LexicalParser lexicalParser = new LexicalParser(feature);
			Symbol featureSymbol = lexicalParser.getNextSymbol();
			Symbol nextSymbol = lexicalParser.getNextSymbol();

			return (featureSymbol instanceof FeatureReference) && (nextSymbol == null);
		}
		catch (Exception e)
		{
			return false;
		}
	}


	/**
	 * Evaluates if a filter is valid or not.
	 *
	 * @param filter The filter to evaluate.
	 * @return If invalid then an error message is returned, null otherwise.
	 */
	public static String isValidFilter(String filter)
	{
		if (filter == null)
		{
			return null;
		}
		else
		{
			try
			{
				new FilterParser(filter).build();
				return null;
			}
			catch (IllegalArgumentException e)
			{
				return e.getMessage();
			}
		}
	}


	/**
	 * Parses a filter; assumes isValid(filter).
	 *
	 * @param filter The filter to parse.
	 * @return The result of parsing the filter.
	 */
	public static BooleanExpression parseFilter(String filter)
	{
		return new FilterParser(filter).build();
	}
}
