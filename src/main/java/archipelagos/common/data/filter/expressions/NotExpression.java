package archipelagos.common.data.filter.expressions;

import archipelagos.common.data.filter.operators.LogicalOperator;
import archipelagos.common.data.filter.operators.OperatorType;

/**
 * Represents a NOT expression.
 */
public class NotExpression implements BooleanExpression
{
	private final LogicalOperator _operator;
	private final BooleanExpression _expression;


	/**
	 * @param operator   The not operator.
	 * @param expression The Boolean expression that this negates.
	 */
	public NotExpression(LogicalOperator operator, BooleanExpression expression)
	{
		if (!OperatorType.NOT.equals(operator.getOperatorType()))
		{
			throw new IllegalArgumentException(String.format("operator is not of type \"%s\"", OperatorType.NOT));
		}

		_operator = operator;
		_expression = expression;
	}


	/**
	 * @return The not operator.
	 */
	public LogicalOperator getOperator()
	{
		return _operator;
	}


	/**
	 * @return The Boolean expression that this negates.
	 */
	public BooleanExpression getExpression()
	{
		return _expression;
	}
}
