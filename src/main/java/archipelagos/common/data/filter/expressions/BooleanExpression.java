package archipelagos.common.data.filter.expressions;

/**
 * Should be implemented by all expressions that result in Booleans.
 */
public interface BooleanExpression extends Expression
{
}
