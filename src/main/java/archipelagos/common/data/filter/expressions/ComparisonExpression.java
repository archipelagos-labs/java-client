package archipelagos.common.data.filter.expressions;

import archipelagos.common.data.filter.constants.Constant;
import archipelagos.common.data.filter.operators.ComparisonOperator;
import archipelagos.common.data.filter.operators.Operator;

/**
 * Represents an expression composed of a comparison operator followed by a constant.
 */
public class ComparisonExpression implements BooleanExpression
{
	private final ComparisonOperator _operator;
	private final Constant _constant;


	/**
	 * @param operator The operator that the expression is composed of.
	 * @param constant The constant that the expression is composed of.
	 */
	public ComparisonExpression(ComparisonOperator operator, Constant constant)
	{
		_operator = operator;
		_constant = constant;
	}


	/**
	 * @return The operator that the expression is composed of.
	 */
	public Operator getOperator()
	{
		return _operator;
	}


	/**
	 * @return The operator that the expression is composed of.
	 */
	public Constant getConstant()
	{
		return _constant;
	}
}
