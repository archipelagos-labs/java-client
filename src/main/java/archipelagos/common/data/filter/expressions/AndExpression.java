package archipelagos.common.data.filter.expressions;

import archipelagos.common.data.filter.operators.LogicalOperator;

/**
 * Represents an AND expression.
 */
public class AndExpression implements BooleanExpression
{
	private final BooleanExpression _left;
	private final LogicalOperator _operator;
	private final BooleanExpression _right;


	/**
	 * @param left     The left-hand logical expression that the expression is composed of.
	 * @param operator The operator that the expression is composed of.
	 * @param right    The right-hand logical expression that the expression is composed of.
	 */
	public AndExpression(BooleanExpression left, LogicalOperator operator, BooleanExpression right)
	{
		_left = left;
		_operator = operator;
		_right = right;
	}


	/**
	 * @return The left-hand logical expression that the expression is composed of.
	 */
	public BooleanExpression getLeftExpression()
	{
		return _left;
	}


	/**
	 * @return The operator that the expression is composed of.
	 */
	public LogicalOperator getOperator()
	{
		return _operator;
	}


	/**
	 * @return The right-hand logical expression that the expression is composed of.
	 */
	public BooleanExpression getRightExpression()
	{
		return _right;
	}
}
