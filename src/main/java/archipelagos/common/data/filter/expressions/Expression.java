package archipelagos.common.data.filter.expressions;

/**
 * Should be implemented by all expressions.
 */
public interface Expression
{
}
