package archipelagos.common.data.filter;

import archipelagos.common.data.filter.brackets.LeftBracket;
import archipelagos.common.data.filter.brackets.RightBracket;
import archipelagos.common.data.filter.constants.Constant;
import archipelagos.common.data.filter.constants.ConstantType;
import archipelagos.common.data.filter.constants.FeatureReference;
import archipelagos.common.data.filter.expressions.*;
import archipelagos.common.data.filter.operators.ComparisonOperator;
import archipelagos.common.data.filter.operators.LogicalOperator;
import archipelagos.common.data.filter.operators.OperatorType;

import java.util.*;

/**
 * Used to parse a filter.
 */
class FilterParser
{
	private final static Map<OperatorType, Set<ConstantType>> OPERATOR_ARGUMENT_TYPES;

	static
	{
		OPERATOR_ARGUMENT_TYPES = new HashMap<>();
		OPERATOR_ARGUMENT_TYPES.put(OperatorType.EQUALS, Set.of(ConstantType.DOUBLE, ConstantType.INT, ConstantType.STRING, ConstantType.DATE_TIME, ConstantType.BOOL, ConstantType.FEATURE));
		OPERATOR_ARGUMENT_TYPES.put(OperatorType.NOT_EQUALS, Set.of(ConstantType.DOUBLE, ConstantType.INT, ConstantType.STRING, ConstantType.DATE_TIME, ConstantType.BOOL, ConstantType.FEATURE));
		OPERATOR_ARGUMENT_TYPES.put(OperatorType.GREATER_THAN, Set.of(ConstantType.DOUBLE, ConstantType.INT, ConstantType.DATE_TIME, ConstantType.FEATURE));
		OPERATOR_ARGUMENT_TYPES.put(OperatorType.GREATER_THAN_OR_EQUALS, Set.of(ConstantType.DOUBLE, ConstantType.INT, ConstantType.DATE_TIME, ConstantType.FEATURE));
		OPERATOR_ARGUMENT_TYPES.put(OperatorType.LESS_THAN, Set.of(ConstantType.DOUBLE, ConstantType.INT, ConstantType.DATE_TIME, ConstantType.FEATURE));
		OPERATOR_ARGUMENT_TYPES.put(OperatorType.LESS_THAN_OR_EQUALS, Set.of(ConstantType.DOUBLE, ConstantType.INT, ConstantType.DATE_TIME, ConstantType.FEATURE));
		OPERATOR_ARGUMENT_TYPES.put(OperatorType.LIKE, Set.of(ConstantType.STRING));
		OPERATOR_ARGUMENT_TYPES.put(OperatorType.UNLIKE, Set.of(ConstantType.STRING));
	}

	private final Stack<BooleanExpression> _expressionStack;
	private final LexicalParser _lexicalParser;
	private Symbol _symbol;


	/**
	 * @param filter The filter to parse.
	 */
	FilterParser(String filter)
	{
		_lexicalParser = new LexicalParser(filter);
		_symbol = _lexicalParser.getNextSymbol();
		_expressionStack = new Stack<>();
	}


	/**
	 * Build a Boolean expression from the input given.
	 */
	BooleanExpression build()
	{
		booleanExpression();

		if ((_symbol != null) || (_expressionStack.size() != 1))
		{
			throw new IllegalArgumentException("no valid Boolean expression was found");
		}
		else
		{
			return _expressionStack.pop();
		}
	}


	/**
	 * Moves the lexical parser to the next symbol.
	 */
	private void nextSymbol()
	{
		_symbol = _lexicalParser.getNextSymbol();
	}


	/**
	 * Parses a Boolean expression.
	 */
	private void booleanExpression()
	{
		booleanTerm();

		while ((_symbol instanceof LogicalOperator) && OperatorType.OR.equals(((LogicalOperator) _symbol).getOperatorType()))
		{
			if (!_expressionStack.isEmpty())
			{
				BooleanExpression left = _expressionStack.pop();
				LogicalOperator orSymbol = (LogicalOperator) _symbol;

				nextSymbol();
				booleanTerm();

				if (!_expressionStack.isEmpty())
				{
					_expressionStack.push(new OrExpression(left, orSymbol, _expressionStack.pop()));
				}
				else
				{
					throw new IllegalArgumentException("the OR operator must be followed by a Boolean expression");
				}
			}
			else
			{
				throw new IllegalArgumentException("a Boolean expression was expected after the OR operator");
			}
		}
	}


	/**
	 * Parses a Boolean term.
	 */
	private void booleanTerm()
	{
		booleanFactor();

		while ((_symbol instanceof LogicalOperator) && OperatorType.AND.equals(((LogicalOperator) _symbol).getOperatorType()))
		{
			if (!_expressionStack.isEmpty())
			{
				BooleanExpression left = _expressionStack.pop();
				LogicalOperator andSymbol = (LogicalOperator) _symbol;

				nextSymbol();
				booleanFactor();

				if (!_expressionStack.isEmpty())
				{
					_expressionStack.push(new AndExpression(left, andSymbol, _expressionStack.pop()));
				}
				else
				{
					throw new IllegalArgumentException("the AND operator must be followed by a Boolean expression");
				}
			}
			else
			{
				throw new IllegalArgumentException("a Boolean expression was expected after the AND operator");
			}
		}
	}


	/**
	 * Parses a comparison expression.
	 */
	private void comparisonExpression()
	{
		if (_symbol instanceof ComparisonOperator)
		{
			ComparisonOperator operator = (ComparisonOperator) _symbol;
			OperatorType operatorType = operator.getOperatorType();

			if (OperatorType.EQUALS.equals(operatorType) ||
					OperatorType.NOT_EQUALS.equals(operatorType) ||
					OperatorType.GREATER_THAN_OR_EQUALS.equals(operatorType) ||
					OperatorType.GREATER_THAN.equals(operatorType) ||
					OperatorType.LESS_THAN_OR_EQUALS.equals(operatorType) ||
					OperatorType.LESS_THAN.equals(operatorType) ||
					OperatorType.LIKE.equals(operatorType) ||
					OperatorType.UNLIKE.equals(operatorType))
			{
				nextSymbol();

				if (_symbol instanceof Constant)
				{
					Constant constant = (Constant) _symbol;

					if (OPERATOR_ARGUMENT_TYPES.containsKey(operatorType) && OPERATOR_ARGUMENT_TYPES.get(operatorType).contains(constant.getConstantType()))
					{
						nextSymbol();
						ComparisonOperator comparisonOperator = new ComparisonOperator(operator.toString(), operator.getOperatorType());
						_expressionStack.push(new ComparisonExpression(comparisonOperator, constant));
					}
					else
					{
						if (constant instanceof FeatureReference)
						{
							throw new IllegalArgumentException(String.format("the operator \"%s\" cannot be used with the feature reference '%s'", operator, constant));
						}
						else
						{
							throw new IllegalArgumentException(String.format("the operator \"%s\" cannot be used with the constant '%s'", operator, constant));
						}
					}
				}
				else
				{
					throw new IllegalArgumentException(String.format("a constant or feature reference is expected after the operator ''%s''", operator));
				}
			}
			else
			{
				throw new IllegalArgumentException(String.format("an unexpected comparison operator '%s' was found", operator));
			}
		}
		else
		{
			throw new IllegalArgumentException("a comparison operator was expected but none was found");
		}
	}


	/**
	 * Parses a Boolean factor.
	 */
	private void booleanFactor()
	{
		if (_symbol != null)
		{
			if (_symbol instanceof ComparisonOperator)
			{
				comparisonExpression();
			}
			else if ((_symbol instanceof LogicalOperator) && OperatorType.NOT.equals(((LogicalOperator) _symbol).getOperatorType()))
			{
				LogicalOperator notSymbol = (LogicalOperator) _symbol;
				nextSymbol();
				booleanFactor();

				if (!_expressionStack.isEmpty())
				{
					_expressionStack.push(new NotExpression(notSymbol, _expressionStack.pop()));
				}
				else
				{
					throw new IllegalArgumentException("the NOT operator must negate a Boolean expression");
				}
			}
			else if (_symbol instanceof LeftBracket)
			{
				nextSymbol();
				booleanExpression();

				if (!_expressionStack.isEmpty() && (_symbol instanceof RightBracket))
				{
					nextSymbol();
				}
				else
				{
					throw new IllegalArgumentException("the NOT operator must negate a Boolean expression");
				}
			}
			else
			{
				throw new IllegalArgumentException("unable to find a valid comparison operator");
			}
		}
	}
}
