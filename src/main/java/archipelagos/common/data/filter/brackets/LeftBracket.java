package archipelagos.common.data.filter.brackets;

import archipelagos.common.data.filter.Symbol;

/**
 * Represents a left bracket (parenthesis).
 */
public class LeftBracket implements Symbol
{
	/**
	 * @see Object
	 */
	@Override
	public String toString()
	{
		return "(";
	}
}
