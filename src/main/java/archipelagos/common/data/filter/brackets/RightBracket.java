package archipelagos.common.data.filter.brackets;

import archipelagos.common.data.filter.Symbol;

/**
 * Represents a right bracket (parenthesis).
 */
public class RightBracket implements Symbol
{
	/**
	 * @see Object
	 */
	@Override
	public String toString()
	{
		return ")";
	}
}
