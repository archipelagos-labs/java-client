package archipelagos.common.data.filter.constants;

/**
 * Represents a String constant.
 */
public class StringConstant implements Constant
{
	private final String _value;


	/**
	 * @param value The value of the constant.
	 */
	public StringConstant(String value)
	{
		_value = value;
	}


	/**
	 * @see Constant
	 */
	public Object getValue()
	{
		return _value;
	}


	/**
	 * @see Constant
	 */
	public String toString()
	{
		return _value;
	}


	/**
	 * @see Constant
	 */
	public ConstantType getConstantType()
	{
		return ConstantType.STRING;
	}
}
