package archipelagos.common.data.filter.constants;

import archipelagos.common.data.filter.Symbol;

/**
 * Should be implemented by all constants.
 */
public interface Constant extends Symbol
{
	/**
	 * @return The value of the constant.
	 */
	Object getValue();


	/**
	 * @return The type of the constant.
	 */
	ConstantType getConstantType();
}
