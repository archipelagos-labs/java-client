package archipelagos.common.data.filter.constants;

/**
 * Represents a double constant.
 */
public class DoubleConstant implements Constant
{
	private final double _value;
	private final String _text;


	/**
	 * @param text  The value provided in the filter.
	 * @param value The value of the constant.
	 */
	public DoubleConstant(String text, double value)
	{
		_text = text;
		_value = value;
	}


	/**
	 * @see Constant
	 */
	public Object getValue()
	{
		return _value;
	}


	/**
	 * @see Constant
	 */
	public String toString()
	{
		return _text;
	}


	/**
	 * @see Constant
	 */
	public ConstantType getConstantType()
	{
		return ConstantType.DOUBLE;
	}
}
