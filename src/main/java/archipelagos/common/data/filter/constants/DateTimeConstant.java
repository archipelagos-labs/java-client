package archipelagos.common.data.filter.constants;

import java.util.Date;

/**
 * Represents a date/time constant.
 */
public class DateTimeConstant implements Constant
{
	private final Date _value;
	private final String _text;


	/**
	 * @param text  The value provided in the filter.
	 * @param value The value of the constant.
	 */
	public DateTimeConstant(String text, Date value)
	{
		_text = text;
		_value = value;
	}


	/**
	 * @see Constant
	 */
	public Object getValue()
	{
		return _value;
	}


	/**
	 * @see Constant
	 */
	public String toString()
	{
		return _text;
	}


	/**
	 * @see Constant
	 */
	public ConstantType getConstantType()
	{
		return ConstantType.DATE_TIME;
	}
}
