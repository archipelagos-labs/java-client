package archipelagos.common.data.filter.constants;

/**
 * The different types of arguments that may be present in a simple filter.
 */
public enum ConstantType
{
	STRING,
	BOOL,
	INT,
	DOUBLE,
	DATE_TIME,
	FEATURE
}
