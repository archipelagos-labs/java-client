package archipelagos.common.data.filter.constants;

/**
 * Represents an integer constant.
 */
public class IntegerConstant implements Constant
{
	private final int _value;
	private final String _text;


	/**
	 * @param text  The value provided in the filter.
	 * @param value The value of the constant.
	 */
	public IntegerConstant(String text, int value)
	{
		_text = text;
		_value = value;
	}


	/**
	 * @see Constant
	 */
	public Object getValue()
	{
		return _value;
	}


	/**
	 * @see Constant
	 */
	public String toString()
	{
		return _text;
	}


	/**
	 * @see Constant
	 */
	public ConstantType getConstantType()
	{
		return ConstantType.INT;
	}
}
