package archipelagos.common.data.filter.constants;

/**
 * Represents a Boolean constant.
 */
public class BooleanConstant implements Constant
{
	private final boolean _value;
	private final String _text;


	/**
	 * @param text  The value provided in the filter.
	 * @param value The value of the constant.
	 */
	public BooleanConstant(String text, boolean value)
	{
		_text = text;
		_value = value;
	}


	/**
	 * @see Constant
	 */
	public Object getValue()
	{
		return _value;
	}


	/**
	 * @see Constant
	 */
	public String toString()
	{
		return _text;
	}


	/**
	 * @see Constant
	 */
	public ConstantType getConstantType()
	{
		return ConstantType.BOOL;
	}
}
