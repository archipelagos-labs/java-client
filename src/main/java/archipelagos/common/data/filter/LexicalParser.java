package archipelagos.common.data.filter;

import archipelagos.common.data.DataUtils;
import archipelagos.common.data.filter.brackets.LeftBracket;
import archipelagos.common.data.filter.brackets.RightBracket;
import archipelagos.common.data.filter.constants.*;
import archipelagos.common.data.filter.operators.ComparisonOperator;
import archipelagos.common.data.filter.operators.LogicalOperator;
import archipelagos.common.data.filter.operators.OperatorType;

import java.util.Date;

/**
 * The lexical parser for filters.
 */
class LexicalParser
{
	private final static String TRUE_CONSTANT = "true";
	private final static String FALSE_CONSTANT = "false";
	private final static String LEFT_BRACKET = "(";
	private final static String RIGHT_BRACKET = ")";

	private String _remainder;


	/**
	 * Represents the result of processing a potential symbol.
	 */
	private static class ProcessingResult
	{
		boolean errorOccurred;
		String errorMessage;
		ConstantType constantType;
		String text;
		String remainder;


		/**
		 * Called if an error occurred when processing a symbol.
		 *
		 * @param errorMessage The error message.
		 */
		ProcessingResult(String errorMessage)
		{
			this.errorOccurred = true;
			this.errorMessage = errorMessage;
		}


		/**
		 * @param constantType The type of constant found.
		 * @param text         The constant.
		 * @param remainder    The remainder of the input left to process.
		 */
		ProcessingResult(ConstantType constantType, String text, String remainder)
		{
			this.errorOccurred = false;
			this.constantType = constantType;
			this.text = text;
			this.remainder = remainder;
		}


		/**
		 * @param text         The constant.
		 * @param remainder    The remainder of the input left to process.
		 */
		ProcessingResult(String text, String remainder)
		{
			this.errorOccurred = false;
			this.text = text;
			this.remainder = remainder;
		}
	}


	/**
	 * @param input The input to parse.
	 */
	LexicalParser(String input)
	{
		// Check the input

		if (input == null || input.length() == 0)
		{
			throw new IllegalArgumentException("no input has been given");
		}


		// Store the input

		_remainder = input;
	}


	/**
	 * Process a feature reference symbol.
	 *
	 * @param remainder The remainder of the input.
	 * @return The result of processing remainder.
	 */
	private static ProcessingResult processFeatureReference(String remainder)
	{
		// Read in all the features (that can be separated by '.')

		StringBuilder feature = new StringBuilder();
		int remainderLen = remainder.length();
		int pos = 0;

		do
		{
			// See if we need to add a period

			if ((pos < remainderLen) && (remainder.charAt(pos) == '.'))
			{
				feature.append(".");
				++pos;
			}


			// Read in a token

			StringBuilder token = new StringBuilder();

			while ((pos < remainderLen) && (Character.isAlphabetic(remainder.charAt(pos)) ||
					Character.isDigit(remainder.charAt(pos)) || (remainder.charAt(pos) == '_') ||
					(remainder.charAt(pos) == '-')))
			{
				token.append(remainder.charAt(pos));
				++pos;
			}


			// Check that the token is valid

			if (!DataUtils.validToken(token.toString()))
			{
				String errorMessage = "the filter contains an unrecognised constant";
				return new ProcessingResult(errorMessage);
			}
			else
			{
				feature.append(token);
			}
		}
		while ((pos < remainderLen) && (remainder.charAt(pos) == '.'));


		// Return the result

		ConstantType constantType = ConstantType.FEATURE;
		String argument = feature.toString();
		int argumentLen = argument.length();
		remainder = argumentLen < remainderLen ? remainder.substring(argumentLen) : "";

		return new ProcessingResult(constantType, argument, remainder);
	}


	/**
	 * Process a date/time symbol.
	 *
	 * @param remainder The remainder of the input.
	 * @return The result of processing remainder.
	 */
	private static ProcessingResult processDateTime(String remainder)
	{
		// Read in the year part

		StringBuilder dateTime = new StringBuilder();
		StringBuilder yearPart = new StringBuilder();
		int remainderLen = remainder.length();
		int pos = 0;

		while ((pos < remainderLen) && Character.isDigit(remainder.charAt(pos)))
		{
			dateTime.append(remainder.charAt(pos));
			yearPart.append(remainder.charAt(pos));
			++pos;
		}


		// Date/times must started with 4 digits (the year) and a hyphen

		if ((pos < remainderLen) && (yearPart.length() == 4) && (remainder.charAt(pos) == '-'))
		{
			dateTime.append('-');
			++pos;


			// Read in the month part

			StringBuilder monthPart = new StringBuilder();

			while ((pos < remainderLen) && Character.isDigit(remainder.charAt(pos)))
			{
				dateTime.append(remainder.charAt(pos));
				monthPart.append(remainder.charAt(pos));
				++pos;
			}


			// The year part must be followed by a 1 or 2 digit month

			if ((pos < remainderLen) && (monthPart.length() > 0 && monthPart.length() < 3) && (remainder.charAt(pos) == '-'))
			{
				dateTime.append('-');
				++pos;

				// Check the month number is valid

				int month = Integer.parseInt(monthPart.toString());

				if (month >= 1 && month <= 12)
				{
					// Read in the day part

					StringBuilder dayPart = new StringBuilder();

					while ((pos < remainderLen) && Character.isDigit(remainder.charAt(pos)))
					{
						dateTime.append(remainder.charAt(pos));
						dayPart.append(remainder.charAt(pos));
						++pos;
					}


					// The month part must be followed by a 1 or 2 digit day

					if (dayPart.length() > 0 && dayPart.length() < 3)
					{
						int day = Integer.parseInt(monthPart.toString());

						if (day >= 1 && day <= 31)
						{
							// Check if we just have a date part, or a time part as well

							if ((pos < remainderLen) && (remainder.charAt(pos) == 'T'))
							{
								dateTime.append('T');
								++pos;

								// Read in the hour part

								StringBuilder hourPart = new StringBuilder();

								while ((pos < remainderLen) && Character.isDigit(remainder.charAt(pos)))
								{
									dateTime.append(remainder.charAt(pos));
									hourPart.append(remainder.charAt(pos));
									++pos;
								}


								// Hour parts must be 1 or 2 digits followed by a colon

								if ((pos < remainderLen) && (hourPart.length() == 1 || hourPart.length() == 2) && (remainder.charAt(pos) == ':'))
								{
									dateTime.append(':');
									++pos;


									// Check the minute number is valid

									int hour = Integer.parseInt(hourPart.toString());

									if (hour >= 0 && hour <= 23)
									{
										// Read in the minute part

										StringBuilder minutePart = new StringBuilder();

										while ((pos < remainderLen) && Character.isDigit(remainder.charAt(pos)))
										{
											dateTime.append(remainder.charAt(pos));
											minutePart.append(remainder.charAt(pos));
											++pos;
										}


										// The minute part must be 1 or 2 digits and followed by a colon

										if ((pos < remainderLen) && (minutePart.length() > 0 && minutePart.length() < 3) && (remainder.charAt(pos) == ':'))
										{
											dateTime.append(':');
											++pos;

											// Check the minute number is valid

											int minute = Integer.parseInt(minutePart.toString());

											if (minute >= 0 && minute <= 59)
											{
												// Read in the second part

												StringBuilder secondPart = new StringBuilder();

												while ((pos < remainderLen) && Character.isDigit(remainder.charAt(pos)))
												{
													dateTime.append(remainder.charAt(pos));
													secondPart.append(remainder.charAt(pos));
													++pos;
												}


												// The second part must be 1 or 2 digits

												if (secondPart.length() > 0 && secondPart.length() < 3)
												{
													int second = Integer.parseInt(secondPart.toString());

													if (second >= 0 && second <= 59)
													{
														// Check if we have a nanosecond part as well

														if ((pos < remainderLen) && (remainder.charAt(pos) == '.'))
														{
															dateTime.append('.');
															++pos;

															// Read in the nanosecond part

															StringBuilder nanosecondPart = new StringBuilder();

															while ((pos < remainderLen) && Character.isDigit(remainder.charAt(pos)))
															{
																dateTime.append(remainder.charAt(pos));
																nanosecondPart.append(remainder.charAt(pos));
																++pos;
															}


															// The nanosecond part can be up to9 digits

															if (nanosecondPart.length() > 0 && nanosecondPart.length() < 10)
															{
																ConstantType constantType = ConstantType.DATE_TIME;
																String argument = dateTime.toString();
																int argumentLen = argument.length();
																remainder = argumentLen < remainderLen ? remainder.substring(argumentLen) : "";

																return new ProcessingResult(constantType, argument, remainder);
															}
															else
															{
																String errorMessage = "date/times must have nanoseconds between 0 and 999999999";
																return new ProcessingResult(errorMessage);
															}
														}
														else
														{
															ConstantType constantType = ConstantType.DATE_TIME;
															String argument = dateTime.toString();
															int argumentLen = argument.length();
															remainder = argumentLen < remainderLen ? remainder.substring(argumentLen) : "";

															return new ProcessingResult(constantType, argument, remainder);
														}
													}
													else
													{
														String errorMessage = "date/times must have seconds between 0 and 59";
														return new ProcessingResult(errorMessage);
													}
												}
												else
												{
													String errorMessage = "the second part of a date/time must be 1 or 2 digits";
													return new ProcessingResult(errorMessage);
												}
											}
											else
											{
												String errorMessage = "date/times must have minutes between 0 and 59";
												return new ProcessingResult(errorMessage);
											}
										}
										else
										{
											String errorMessage = "the minute part of a date/time must be 1 or 2 digits followed by a colon";
											return new ProcessingResult(errorMessage);
										}
									}
									else
									{
										String errorMessage = "date/times must have hours between 0 and 23";
										return new ProcessingResult(errorMessage);
									}
								}
								else
								{
									String errorMessage = "the hour part of a date/time must be 1 or 2 digits followed by a colon";
									return new ProcessingResult(errorMessage);
								}
							}
							else
							{
								ConstantType constantType = ConstantType.DATE_TIME;
								String argument = dateTime.toString();
								int argumentLen = argument.length();
								remainder = argumentLen < remainderLen ? remainder.substring(argumentLen) : "";

								return new ProcessingResult(constantType, argument, remainder);
							}
						}
						else
						{
							String errorMessage = "date/times must have days between 1 and 31";
							return new ProcessingResult(errorMessage);
						}
					}
					else
					{
						String errorMessage = "the day of month in a date/time must be of 1 or 2 digits and follow a hyphen";
						return new ProcessingResult(errorMessage);
					}
				}
				else
				{
					String errorMessage = "date/times must have months between 1 and 12";
					return new ProcessingResult(errorMessage);
				}
			}
			else
			{
				String errorMessage = "date/times must start with a four digit year followed by a hyphen and a one or two digit month";
				return new ProcessingResult(errorMessage);
			}
		}
		else
		{
			String errorMessage = "date/times must start with a four digit year followed by a hyphen";
			return new ProcessingResult(errorMessage);
		}
	}


	/**
	 * Process a number symbol.
	 *
	 * @param remainder The remainder of the input.
	 * @return The result of processing remainder.
	 */
	private static ProcessingResult processNumberArgument(String remainder)
	{
		// Read in the integer part

		StringBuilder number = new StringBuilder();
		int remainderLen = remainder.length();
		int pos = 0;

		while ((pos < remainderLen) && (Character.isDigit(remainder.charAt(pos)) || (remainder.charAt(pos) == '_')))
		{
			if (remainder.charAt(pos) == '_')
			{
				int numberLen = number.length();

				if (numberLen == 0)
				{
					String errorMessage = "numbers cannot start with an underscore";
					return new ProcessingResult(errorMessage);
				}
				else
				{
					char lastChar = number.charAt(numberLen - 1);

					if (!Character.isDigit(lastChar) && (lastChar != '_'))
					{
						String errorMessage = "underscores can only follow digits or underscores in numbers";
						return new ProcessingResult(errorMessage);
					}
					else if( pos == remainderLen -1)
					{
						String errorMessage = "numbers cannot end with an underscore";
						return new ProcessingResult(errorMessage);
					}
				}
			}

			number.append(remainder.charAt(pos));
			++pos;
		}


		// See if we have a fractional part and/or scientific notation

		if (pos == remainderLen)
		{
			ConstantType constantType = ConstantType.INT;
			String argument = number.toString();
			int argumentLen = argument.length();
			remainder = argumentLen < remainderLen ? remainder.substring(argumentLen) : "";

			return new ProcessingResult(constantType, argument, remainder);
		}
		else if ((pos < remainderLen) && (remainder.charAt(pos) == '.'))
		{
			if (remainder.charAt(pos - 1) == '_')
			{
				String errorMessage = "underscores cannot appear directly before the decimal point in a number";
				return new ProcessingResult(errorMessage);
			}

			number.append('.');
			++pos;


			// Read in the fractional part (which can just be a '.')

			while ((pos < remainderLen) && (Character.isDigit(remainder.charAt(pos)) || (remainder.charAt(pos) == '_')))
			{
				if (remainder.charAt(pos) == '_')
				{
					int numberLen = number.length();
					char lastChar = number.charAt(numberLen - 1);

					if (!Character.isDigit(lastChar) && (lastChar != '_'))
					{
						String errorMessage = "underscores can only follow digits or underscores in numbers";
						return new ProcessingResult(errorMessage);
					}
					else if (pos == remainderLen - 1)
					{
						String errorMessage = "numbers cannot end with an underscore";
						return new ProcessingResult(errorMessage);
					}
				}

				number.append(remainder.charAt(pos));
				++pos;
			}


			// Check if we have scientific notation (which must be an integer, possibly starting with '-')

			if ((pos < remainderLen) && (remainder.charAt(pos) == 'e' || remainder.charAt(pos) == 'E'))
			{
				if (remainder.charAt(pos - 1) == '_')
				{
					String errorMessage = "underscores cannot appear directly before an 'e' or 'E' in a number";
					return new ProcessingResult(errorMessage);
				}

				number.append(remainder.charAt(pos));
				++pos;


				// Check if we have a negative exponent

				if ((pos < remainderLen) && (remainder.charAt(pos) == '-'))
				{
					number.append("-");
					++pos;
				}


				// Check that an integer appears after the 'e' or 'E' character

				if (pos >= remainderLen)
				{
					String errorMessage = "scientific notation requires an integer to appear after the 'e' or 'E'";
					return new ProcessingResult(errorMessage);
				}
				else if (!Character.isDigit(remainder.charAt(pos)))
				{
					String errorMessage = "scientific notation requires an integer to appear after the 'e' or 'E'";
					return new ProcessingResult(errorMessage);
				}
				else
				{
					// Read in the exponent

					while ((pos < remainderLen) && (Character.isDigit(remainder.charAt(pos)) || (remainder.charAt(pos) == '_')))
					{
						if (remainder.charAt(pos) == '_')
						{
							int numberLen = number.length();
							char lastChar = number.charAt(numberLen - 1);

							if (!Character.isDigit(lastChar) && (lastChar != '_'))
							{
								String errorMessage = "underscores can only follow digits or underscores in numbers";
								return new ProcessingResult(errorMessage);
							}
							else if (pos == remainderLen - 1)
							{
								String errorMessage = "numbers cannot end with an underscore";
								return new ProcessingResult(errorMessage);
							}
						}

						number.append(remainder.charAt(pos));
						++pos;
					}


					// Return the result

					ConstantType constantType = ConstantType.DOUBLE;
					String argument = number.toString();
					int argumentLen = argument.length();
					remainder = argumentLen < remainderLen ? remainder.substring(argumentLen) : "";

					return new ProcessingResult(constantType, argument, remainder);
				}
			}
			else
			{
				ConstantType constantType = ConstantType.DOUBLE;
				String argument = number.toString();
				int argumentLen = argument.length();
				remainder = argumentLen < remainder.length() ? remainder.substring(argumentLen) : "";

				return new ProcessingResult(constantType, argument, remainder);
			}
		}
		else if ((pos < remainderLen) && (remainder.charAt(pos) == 'e' || (remainder.charAt(pos) == 'E')))
		{
			if (remainder.charAt(pos - 1) == '_')
			{
				String errorMessage = "underscores cannot appear directly before an 'e' or 'E' in a number";
				return new ProcessingResult(errorMessage);
			}

			number.append(remainder.charAt(pos));
			++pos;


			// Check if we have a negative exponent

			if ((pos < remainderLen) && (remainder.charAt(pos) == '-'))
			{
				number.append("-");
				++pos;
			}


			// Check that an integer does appear after the 'e' or 'E' character

			if (pos >= remainderLen)
			{
				String errorMessage = "scientific notation requires an integer to appear after the 'e' or 'E'";
				return new ProcessingResult(errorMessage);
			}
			else if (!Character.isDigit(remainder.charAt(pos)))
			{
				String errorMessage = "scientific notation requires an integer to appear after the 'e' or 'E'";
				return new ProcessingResult(errorMessage);
			}
			else
			{
				// Read in the exponent

				while ((pos < remainderLen) && (Character.isDigit(remainder.charAt(pos)) || (remainder.charAt(pos) == '_')))
				{
					if (remainder.charAt(pos) == '_')
					{
						int numberLen = number.length();
						char lastChar = number.charAt(numberLen - 1);

						if (!Character.isDigit(lastChar) && (lastChar != '_'))
						{
							String errorMessage = "underscores can only follow digits or underscores in numbers";
							return new ProcessingResult(errorMessage);
						}
						else if (pos == remainderLen - 1)
						{
							String errorMessage = "numbers cannot end with an underscore";
							return new ProcessingResult(errorMessage);
						}
					}

					number.append(remainder.charAt(pos));
					++pos;
				}


				// Return the result

				ConstantType constantType = ConstantType.DOUBLE;
				String argument = number.toString();
				int argumentLen = argument.length();
				remainder = argumentLen < remainderLen ? remainder.substring(argumentLen) : "";

				return new ProcessingResult(constantType, argument, remainder);
			}
		}
		else
		{
			ConstantType constantType = ConstantType.INT;
			String argument = number.toString();
			int argumentLen = argument.length();
			remainder = argumentLen < remainderLen ? remainder.substring(argumentLen) : "";

			return new ProcessingResult(constantType, argument, remainder);
		}
	}


	/**
	 * Process a symbol that could either be a number or a date/time.
	 *
	 * @param remainder The remainder of the input.
	 * @return The result of processing remainder.
	 */
	private static ProcessingResult processNumberOrDateTimeArgument(String remainder)
	{
		// Read in the integer part

		StringBuilder number = new StringBuilder();
		int remainderLen = remainder.length();
		boolean foundUnderscore = false;
		int pos = 0;

		while ((pos < remainderLen) && (Character.isDigit(remainder.charAt(pos)) || (remainder.charAt(pos) == '_')))
		{
			if (remainder.charAt(pos) == '_')
			{
				foundUnderscore = true;
			}

			number.append(remainder.charAt(pos));
			++pos;
		}


		// Date/times must started with 4 digits (the year) and a hyphen

		if ((pos < remainderLen) && (number.length() == 4) && (remainder.charAt(pos) == '-')  && !foundUnderscore)
		{
			return processDateTime(remainder);
		}
		else
		{
			return processNumberArgument(remainder);
		}
	}


	/**
	 * Process a single quoted string symbol.
	 *
	 * @param remainder The remainder of the input.
	 * @return The result of processing remainder.
	 */
	private static ProcessingResult processSingleQuotedStringArgument(String remainder)
	{
		// Read in until we see a closing single quote or come to the end

		StringBuilder text = new StringBuilder("'");
		int remainderLen = remainder.length();
		int pos = 1;

		while ((pos < remainderLen) && (remainder.charAt(pos) != '\''))
		{
			// Deal with any escaped single quotes

			if ((remainder.charAt(pos) == '\\') && (pos < remainderLen - 1) && (remainder.charAt(pos + 1) == '\''))
			{
				text.append("\\'");
				++pos;
			}
			else
			{
				text.append(remainder.charAt(pos));
			}


			++pos;
		}


		// Check that we had a closing single quote

		if ((pos < remainderLen) && (remainder.charAt(pos) == '\''))
		{
			text.append("'");
			String argument = text.toString();
			int argumentLen = argument.length();
			remainder = argumentLen < remainderLen ? remainder.substring(argumentLen) : "";
			ConstantType constantType = ConstantType.STRING;

			return new ProcessingResult(constantType, argument, remainder);
		}
		else
		{
			String errorMessage = "a string does not have a closing single quote (') character";
			return new ProcessingResult(errorMessage);
		}
	}


	/**
	 * Process a double quoted string symbol.
	 *
	 * @param remainder The remainder of the input.
	 * @return The result of processing remainder.
	 */
	private static ProcessingResult processDoubleQuotedString(String remainder)
	{
		// Read in until we see a closing double quote or come to the end

		StringBuilder text = new StringBuilder("\"");
		int remainderLen = remainder.length();
		int pos = 1;

		while ((pos < remainderLen) && (remainder.charAt(pos) != '"'))
		{
			// Deal with any escaped double quotes

			if ((remainder.charAt(pos) == '\\') && (pos < remainderLen - 1) && (remainder.charAt(pos + 1) == '"'))
			{
				text.append("\\\"");
				++pos;
			}
			else
			{
				text.append(remainder.charAt(pos));
			}

			++pos;
		}


		// Check that we had a closing double quote

		if ((pos < remainderLen) && (remainder.charAt(pos) == '"'))
		{
			text.append("\"");
			String argument = text.toString();
			int argumentLen = argument.length();
			remainder = argumentLen < remainderLen ? remainder.substring(argumentLen) : "";
			ConstantType constantType = ConstantType.STRING;

			return new ProcessingResult(constantType, argument, remainder);
		}
		else
		{
			String errorMessage = "a string does not have a closing double quote (\") character";
			return new ProcessingResult(errorMessage);
		}
	}


	/**
	 * Process a Boolean symbol.
	 *
	 * @param booleanStr The Boolean being processed.
	 * @param remainder  The remainder of the input.
	 * @return The result of processing remainder.
	 */
	private static ProcessingResult processBoolean(String booleanStr, String remainder)
	{
		ConstantType constantType = ConstantType.BOOL;
		String argument = remainder.substring(0, booleanStr.length());
		int argumentLen = argument.length();
		int remainderLen = remainder.length();
		remainder = argumentLen < remainderLen ? remainder.substring(argumentLen) : "";

		return new ProcessingResult(constantType, argument, remainder);
	}


	/**
	 * Process an operator symbol.
	 *
	 * @param operatorStr The operator being processed.
	 * @param remainder   The remainder of the input.
	 * @return The result of processing remainder.
	 */
	private static ProcessingResult processOperator(String operatorStr, String remainder)
	{
		int operatorLen = operatorStr.length();
		int remainderLen = remainder.length();
		remainder = operatorLen < remainderLen ? remainder.substring(operatorLen) : "";

		return new ProcessingResult(operatorStr, remainder);
	}


	/**
	 * Process a bracket symbol.
	 *
	 * @param bracketStr The bracket being processed.
	 * @param remainder  The remainder of the input.
	 * @return The result of processing remainder.
	 */
	private static ProcessingResult processBracket(String bracketStr, String remainder)
	{
		int bracketLen = bracketStr.length();
		int remainderLen = remainder.length();
		remainder = bracketLen < remainderLen ? remainder.substring(bracketLen) : "";

		return new ProcessingResult(bracketStr, remainder);
	}


	/**
	 * Checks if a given piece of text starts with a provide token.
	 *
	 * @param token The token that should be at the start of remainder.
	 * @param remainder  The remainder of the input.
	 * @return true if remainder starts with token, false otherwise.
	 */
	private static boolean startsWith(String token, String remainder)
	{
		if (remainder.startsWith(token))
		{
			int tokenLen = token.length();
			int remainderLen = remainder.length();

			if (tokenLen < remainderLen)
			{
				return !Character.isAlphabetic(remainder.charAt(tokenLen));
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}


	/**
	 * Obtains the next symbol from the input.
	 *
	 * @return The next symbol, or null if there are no symbols left.
	 */
	Symbol getNextSymbol()
	{
		// Make sure there is anything left to process

		if (_remainder != null)
		{
			String remainder = _remainder.trim();
			int remainderLen = remainder.length();

			if (remainderLen == 0)
			{
				return null;
			}
			else
			{
				// Check what type of symbol we have

				String remainderLower = remainder.toLowerCase();

				if (remainder.charAt(0) == '\'')
				{
					ProcessingResult result = processSingleQuotedStringArgument(remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new StringConstant(result.text);
					}
				}
				else if (remainder.charAt(0) == '"')
				{
					ProcessingResult result = processDoubleQuotedString(remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new StringConstant(result.text);
					}
				}
				else if (Character.isDigit(remainder.charAt(0)))
				{
					ProcessingResult result = processNumberOrDateTimeArgument(remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;

						if (ConstantType.INT.equals(result.constantType))
						{
							int value = Integer.parseInt(result.text.replace("_", ""));
							return new IntegerConstant(result.text, value);
						}
						else if (ConstantType.DOUBLE.equals(result.constantType))
						{
							double value = Double.parseDouble(result.text.replace("_", ""));
							return new DoubleConstant(result.text, value);
						}
						else if (ConstantType.DATE_TIME.equals(result.constantType))
						{
							Date value = DataUtils.getDate(DataUtils.parseDateTime(result.text));
							return new DateTimeConstant(result.text, value);
						}
						else
						{
							throw new IllegalArgumentException("Unrecognised symbol");
						}
					}
				}
				else if (startsWith(TRUE_CONSTANT, remainderLower))
				{
					ProcessingResult result = processBoolean(TRUE_CONSTANT, remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						boolean value = Boolean.parseBoolean(result.text);
						return new BooleanConstant(result.text, value);
					}
				}
				else if (startsWith(FALSE_CONSTANT, remainderLower))
				{
					ProcessingResult result = processBoolean(FALSE_CONSTANT, remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						boolean value = Boolean.parseBoolean(result.text);
						return new BooleanConstant(result.text, value);
					}
				}
				else if (startsWith(OperatorType.AND.toString(), remainderLower))
				{
					OperatorType operatorType = OperatorType.AND;
					ProcessingResult result = processOperator(operatorType.toString(), remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new LogicalOperator(result.text, operatorType);
					}
				}
				else if (startsWith(OperatorType.OR.toString(), remainderLower))
				{
					OperatorType operatorType = OperatorType.OR;
					ProcessingResult result = processOperator(operatorType.toString(), remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new LogicalOperator(result.text, operatorType);
					}
				}
				else if (startsWith(OperatorType.NOT.toString(), remainderLower))
				{
					OperatorType operatorType = OperatorType.NOT;
					ProcessingResult result = processOperator(operatorType.toString(), remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new LogicalOperator(result.text, operatorType);
					}
				}
				else if (remainderLower.startsWith(OperatorType.LESS_THAN_OR_EQUALS.toString()))
				{
					OperatorType operatorType = OperatorType.LESS_THAN_OR_EQUALS;
					ProcessingResult result = processOperator(operatorType.toString(), remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new ComparisonOperator(result.text, operatorType);
					}
				}
				else if (remainderLower.startsWith(OperatorType.LESS_THAN.toString()))
				{
					OperatorType operatorType = OperatorType.LESS_THAN;
					ProcessingResult result = processOperator(operatorType.toString(), remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new ComparisonOperator(result.text, operatorType);
					}
				}
				else if (remainderLower.startsWith(OperatorType.GREATER_THAN_OR_EQUALS.toString()))
				{
					OperatorType operatorType = OperatorType.GREATER_THAN_OR_EQUALS;
					ProcessingResult result = processOperator(operatorType.toString(), remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new ComparisonOperator(result.text, operatorType);
					}
				}
				else if (remainderLower.startsWith(OperatorType.GREATER_THAN.toString()))
				{
					OperatorType operatorType = OperatorType.GREATER_THAN;
					ProcessingResult result = processOperator(operatorType.toString(), remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new ComparisonOperator(result.text, operatorType);
					}
				}
				else if (remainderLower.startsWith(OperatorType.NOT_EQUALS.toString()))
				{
					OperatorType operatorType = OperatorType.NOT_EQUALS;
					ProcessingResult result = processOperator(operatorType.toString(), remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new ComparisonOperator(result.text, operatorType);
					}
				}
				else if (remainderLower.startsWith(OperatorType.EQUALS.toString()))
				{
					OperatorType operatorType = OperatorType.EQUALS;
					ProcessingResult result = processOperator(operatorType.toString(), remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new ComparisonOperator(result.text, operatorType);
					}
				}
				else if (remainderLower.startsWith(OperatorType.UNLIKE.toString()))
				{
					OperatorType operatorType = OperatorType.UNLIKE;
					ProcessingResult result = processOperator(operatorType.toString(), remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new ComparisonOperator(result.text, operatorType);
					}
				}
				else if (remainderLower.startsWith(OperatorType.LIKE.toString()))
				{
					OperatorType operatorType = OperatorType.LIKE;
					ProcessingResult result = processOperator(operatorType.toString(), remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new ComparisonOperator(result.text, operatorType);
					}
				}
				else if (remainderLower.startsWith(LEFT_BRACKET))
				{
					ProcessingResult result = processBracket(LEFT_BRACKET, remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new LeftBracket();
					}
				}
				else if (remainderLower.startsWith(RIGHT_BRACKET))
				{
					ProcessingResult result = processBracket(RIGHT_BRACKET, remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new RightBracket();
					}
				}
				else if (Character.isLetter(remainder.charAt(0)))
				{
					ProcessingResult result = processFeatureReference(remainder);

					if (result.errorOccurred)
					{
						throw new IllegalArgumentException(result.errorMessage);
					}
					else
					{
						_remainder = result.remainder;
						return new FeatureReference(result.text);
					}
				}
				else
				{
					throw new IllegalArgumentException(String.format("unrecognised symbol '%s'", _remainder));
				}
			}
		}
		else
		{
			return null;
		}
	}
}
