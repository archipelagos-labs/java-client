package archipelagos.common.data.filter.operators;

/**
 * The different types of operators that may be present in a filter.
 */
public enum OperatorType
{
	EQUALS("="),
	NOT_EQUALS("!="),
	GREATER_THAN(">"),
	GREATER_THAN_OR_EQUALS(">="),
	LESS_THAN("<"),
	LESS_THAN_OR_EQUALS("<="),
	LIKE("like"),
	UNLIKE("unlike"),
	NOT("not"),
	AND("and"),
	OR("or");


	/**
	 * @param operator The string representation of the operator.
	 */
	OperatorType(String operator)
	{
		_operator = operator;
	}


	/**
	 * @see Object
	 */
	@Override
	public String toString()
	{
		return _operator;
	}

	private final String _operator;
}
