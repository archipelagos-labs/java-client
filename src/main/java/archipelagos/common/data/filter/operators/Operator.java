package archipelagos.common.data.filter.operators;

import archipelagos.common.data.filter.Symbol;

/**
 * Should be implemented by all operators.
 */
public interface Operator extends Symbol
{
	/**
	 * @return The type of the operator.
	 */
	OperatorType getOperatorType();
}
