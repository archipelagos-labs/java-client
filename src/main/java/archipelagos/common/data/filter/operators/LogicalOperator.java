package archipelagos.common.data.filter.operators;

import archipelagos.common.data.filter.Symbol;

/**
 * Represents a logical operator (e.g. AND, OR, NOT...).
 */
public class LogicalOperator implements Operator
{
	private final OperatorType _operatorType;
	private final String _text;


	/**
	 * @param text         The string representation of the operator.
	 * @param operatorType The type of the operator.
	 */
	public LogicalOperator(String text, OperatorType operatorType)
	{
		_text = text;
		_operatorType = operatorType;
	}


	/**
	 * @see Symbol
	 */
	@Override
	public String toString()
	{
		return _text;
	}


	/**
	 * @see Operator
	 */
	@Override
	public OperatorType getOperatorType()
	{
		return _operatorType;
	}
}
