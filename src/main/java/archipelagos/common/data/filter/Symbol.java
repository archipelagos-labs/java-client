package archipelagos.common.data.filter;

/**
 * Needs to be implemented by all symbols.
 */
public interface Symbol
{
	/**
	 * @see Object
	 */
	@Override
	String toString();
}
