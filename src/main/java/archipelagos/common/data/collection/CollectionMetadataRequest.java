package archipelagos.common.data.collection;

import archipelagos.common.data.DataUtils;
import archipelagos.common.platform.ClientType;
import archipelagos.common.protobuf.common.data.collection.CollectionMetadataRequestProto;

/**
 * Represents a collection metadata request.
 */
public final class CollectionMetadataRequest
{
    private String _apiKey;
    private String _source;
    private String _category;
    private String _label;
    private String _clientType;


    /**
     *
     */
    CollectionMetadataRequest()
    {
    }


    /**
     * @param source   The source for the collection.
     * @param category The category for the collection.
     * @param label    The label for the collection.
     */
    public static CollectionMetadataRequest of(String source, String category, String label)
    {
        // Check the arguments

        if (source == null)
        {
            throw new IllegalArgumentException("source is null");
        }

        if(!DataUtils.validToken(source))
        {
            throw new IllegalArgumentException("source is not valid");
        }

        if (category == null)
        {
            throw new IllegalArgumentException("category is null");
        }

        if(!DataUtils.validToken(category))
        {
            throw new IllegalArgumentException("category is not valid");
        }

        if (label == null)
        {
            throw new IllegalArgumentException("label is null");
        }

        if(!DataUtils.validToken(label))
        {
            throw new IllegalArgumentException("label is not valid");
        }


        // Create the request and return

        CollectionMetadataRequest metadataRequest = new CollectionMetadataRequest();
        metadataRequest._apiKey = null;
        metadataRequest._source = source;
        metadataRequest._category = category;
        metadataRequest._label = label;
        metadataRequest._clientType = ClientType.JAVA_CLIENT;

        return metadataRequest;
    }


    /**
     * @param source   The source for the collection.
     * @param category The category for the collection.
     * @param label    The label for the collection.
     * @param apiKey The API key.
     */
    public static CollectionMetadataRequest of(String source, String category, String label, String apiKey)
    {
        // Check the arguments

        if (source == null)
        {
            throw new IllegalArgumentException("source is null");
        }

        if(!DataUtils.validToken(source))
        {
            throw new IllegalArgumentException("source is not valid");
        }

        if (category == null)
        {
            throw new IllegalArgumentException("category is null");
        }

        if(!DataUtils.validToken(category))
        {
            throw new IllegalArgumentException("category is not valid");
        }

        if (label == null)
        {
            throw new IllegalArgumentException("label is null");
        }

        if(!DataUtils.validToken(label))
        {
            throw new IllegalArgumentException("label is not valid");
        }

        if (apiKey == null)
        {
            throw new IllegalArgumentException("apiKey is null");
        }

        if(!DataUtils.validToken(apiKey))
        {
            throw new IllegalArgumentException("apiKey is not a valid API Key");
        }


        // Create the request and return

        CollectionMetadataRequest metadataRequest = new CollectionMetadataRequest();
        metadataRequest._apiKey = apiKey;
        metadataRequest._source = source;
        metadataRequest._category = category;
        metadataRequest._label = label;
        metadataRequest._clientType = ClientType.JAVA_CLIENT;

        return metadataRequest;
    }


    /**
     * @return The API key.
     */
    public String getApiKey()
    {
        return _apiKey;
    }


    /**
     * Sets the API Key
     *
     * @param apiKey The new API key.
     */
    public void setApiKey(String apiKey)
    {
        _apiKey = apiKey;
    }


    /**
     * @return The source for the collection.
     */
    public String getSource()
    {
        return _source;
    }


    /**
     * @return The category for the collection.
     */
    public String getCategory()
    {
        return _category;
    }


    /**
     * @return The label for the collection.
     */
    public String getLabel()
    {
        return _label;
    }


    /**
     * @return The type of client that sent the request.
     */
    public String getClientType()
    {
        return _clientType;
    }


    /**
     * Sets the state of this from a provided Protobuf object.
     *
     * @param request The Protobuf object to set the state from.
     */
    void readProto(CollectionMetadataRequestProto.CollectionMetadataRequest request)
    {
        _apiKey = request.getApiKey();
        _source = request.getSource();
        _category = request.getCategory();
        _label = request.getLabel();
        _clientType = request.getClientType();
    }


    /**
     * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
     *
     * @param serialized The serialized state of an object.
     * @throws Exception If an error happened during the deserialization process.
     */
    private void readProto(byte[] serialized) throws Exception
    {
        CollectionMetadataRequestProto.CollectionMetadataRequest request = CollectionMetadataRequestProto.CollectionMetadataRequest.parseFrom(serialized);
        readProto(request);
    }


    /**
     * Creates a new instance using a byte[] created by serialize().
     *
     * @param serialized The byte[].
     * @throws Exception If there were any issues when deserializing the object.
     */
    public static CollectionMetadataRequest deserialize(byte[] serialized) throws Exception
    {
        CollectionMetadataRequest collectionMetadataRequest = new CollectionMetadataRequest();
        collectionMetadataRequest.readProto(serialized);

        return collectionMetadataRequest;
    }


    /**
     * @return A Protobuf object containing the serialised state of this.
     */
    CollectionMetadataRequestProto.CollectionMetadataRequest writeProtoObject()
    {
        CollectionMetadataRequestProto.CollectionMetadataRequest.Builder requestBuilder = CollectionMetadataRequestProto.CollectionMetadataRequest.newBuilder();
        return requestBuilder.setApiKey(_apiKey).setSource(_source).setCategory(_category).setLabel(_label).setClientType(_clientType).build();
    }


    /**
     * @return A byte[] containing the state of this serialised.
     */
    public byte[] serialize()
    {
        CollectionMetadataRequestProto.CollectionMetadataRequest request = writeProtoObject();

        return request.toByteArray();
    }
}
