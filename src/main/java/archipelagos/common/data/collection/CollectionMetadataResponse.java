package archipelagos.common.data.collection;

import archipelagos.common.protobuf.common.data.collection.CollectionMetadataProto;
import archipelagos.common.protobuf.common.data.collection.CollectionMetadataResponseProto;

/**
 * Represents the response to a collection metadata request.
 */
public final class CollectionMetadataResponse
{
    private boolean _errorOccurred;
    private CollectionMetadata _metadata;
    private int _errorCode;
    private String _errorMessage;


    /**
     *
     */
    CollectionMetadataResponse()
    {
    }


    /**
     * Called when data is to be returned.
     *
     * @param metadata The data to return.
     */
    public static CollectionMetadataResponse of(CollectionMetadata metadata)
    {
        // Check the arguments

        if (metadata == null)
        {
            throw new IllegalArgumentException("metadata is null");
        }


        // Create the response and return

        CollectionMetadataResponse metadataResponse = new CollectionMetadataResponse();
        metadataResponse._errorOccurred = false;
        metadataResponse._metadata = metadata;

        return metadataResponse;
    }


    /**
     * Called when an error has occurred.
     *
     * @param errorCode    The error code.
     * @param errorMessage The error message (must be non-null).
     */
    public static CollectionMetadataResponse of(int errorCode, String errorMessage)
    {
        // Check the arguments

        if (errorMessage == null)
        {
            throw new IllegalArgumentException("metadata is null");
        }


        // Create the response and return

        CollectionMetadataResponse metadataResponse = new CollectionMetadataResponse();
        metadataResponse._errorOccurred = true;
        metadataResponse._errorCode = errorCode;
        metadataResponse._errorMessage = errorMessage;

        return metadataResponse;
    }


    /**
     * @return true if an error occurred, false otherwise.
     */
    public boolean errorOccurred()
    {
        return _errorOccurred;
    }


    /**
     * @return If an error did not occur then the data to return, null otherwise.
     */
    public CollectionMetadata getMetadata()
    {
        return _metadata;
    }


    /**
     * @return If an error occurred then the error code, 0 otherwise.
     */
    public int getErrorCode()
    {
        return _errorCode;
    }


    /**
     * @return If an error occurred then the error message, null otherwise.
     */
    public String getErrorMessage()
    {
        return _errorMessage;
    }


    /**
     * Sets the state of this from a provided Protobuf object.
     *
     * @param metadataResponse The Protobuf object to set the state from.
     */
    void readProto(CollectionMetadataResponseProto.CollectionMetadataResponse metadataResponse)
    {
        if (metadataResponse.getErrorHasOccurred())
        {
            _errorOccurred = true;
            _errorCode = metadataResponse.getErrorCode();
            _errorMessage = metadataResponse.getErrorMessage();
        }
        else
        {
            _errorOccurred = false;
            _metadata = new CollectionMetadata();
            _metadata.readProto(metadataResponse.getMetadata());
        }
    }


    /**
     * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
     *
     * @param serialized The serialized state of an object.
     * @throws Exception If an error happened during the deserialization process.
     */
    private void readProto(byte[] serialized) throws Exception
    {
        CollectionMetadataResponseProto.CollectionMetadataResponse response = CollectionMetadataResponseProto.CollectionMetadataResponse.parseFrom(serialized);
        readProto(response);
    }


    /**
     * Creates a new instance using a byte[] created by serialize().
     *
     * @param serialized The byte[].
     * @throws Exception If there were any issues when deserializing the object.
     */
    public static CollectionMetadataResponse deserialize(byte[] serialized) throws Exception
    {
        CollectionMetadataResponse metadataResponse = new CollectionMetadataResponse();
        metadataResponse.readProto(serialized);

        return metadataResponse;
    }


    /**
     * @return A Protobuf object containing the serialised state of this.
     */
    CollectionMetadataResponseProto.CollectionMetadataResponse writeProtoObject()
    {
        if (_errorOccurred)
        {
            return CollectionMetadataResponseProto.CollectionMetadataResponse.newBuilder().setErrorHasOccurred(true).setErrorCode(_errorCode).setErrorMessage(_errorMessage).build();
        }
        else
        {
            CollectionMetadataProto.CollectionMetadata metadata = _metadata.writeProtoObject();
            return CollectionMetadataResponseProto.CollectionMetadataResponse.newBuilder().setErrorHasOccurred(false).setMetadata(metadata).build();
        }
    }


    /**
     * @return A byte[] containing the state of this serialised.
     */
    public byte[] serialize()
    {
        CollectionMetadataResponseProto.CollectionMetadataResponse response = writeProtoObject();

        return response.toByteArray();
    }
}
