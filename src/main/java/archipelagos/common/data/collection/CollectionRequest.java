package archipelagos.common.data.collection;

import archipelagos.common.data.DataUtils;
import archipelagos.common.platform.ClientType;
import archipelagos.common.protobuf.common.TimestampProto;
import archipelagos.common.protobuf.common.data.collection.CollectionRequestProto;
import com.google.protobuf.ProtocolStringList;

import java.time.*;
import java.util.*;

/**
 * Represents a collection request.
 */
public final class CollectionRequest
{
    private String _apiKey;
    private String _source;
    private String _category;
    private String _label;
    private List<String> _features;
    private long _maxSize;
    private Map<String, String> _filters;
    private String _clientType;


    /**
     *
     */
    CollectionRequest()
    {
    }


    /**
     * @param source    The source for the collection.
     * @param category  The category for the collection.
     * @param label     The label for the collection.
     * @param features  The (optional) list of features, or null.
     * @param maxSize   The maximum number of items that can be returned, or -1 if there should be no limit.
     * @param filters   The filters.
     */
    public static CollectionRequest of(String source, String category, String label, List<String> features, long maxSize, Map<String, String> filters)
    {
        // Check the arguments

        if (source == null)
        {
            throw new IllegalArgumentException("source is null");
        }

        if (!DataUtils.validToken(source))
        {
            throw new IllegalArgumentException("source is not valid");
        }

        if (category == null)
        {
            throw new IllegalArgumentException("category is null");
        }

        if (!DataUtils.validToken(category))
        {
            throw new IllegalArgumentException("category is not valid");
        }

        if (label == null)
        {
            throw new IllegalArgumentException("label is null");
        }

        if (!DataUtils.validToken(label))
        {
            throw new IllegalArgumentException("label is not valid");
        }

        if (features != null)
        {
            for(String feature : features)
            {
                if (!DataUtils.validToken(feature))
                {
                    throw new IllegalArgumentException(String.format("\"%s\" is not a valid name for a feature", feature));
                }
            }
        }

        if (maxSize < -1)
        {
            throw new IllegalArgumentException("maxSize is less than -1");
        }

        if (filters == null)
        {
            throw new IllegalArgumentException("filters is null");
        }

        for(Map.Entry<String, String> filter : filters.entrySet())
        {
            String featureName = filter.getKey();

            if (!DataUtils.validToken(featureName))
            {
                throw new IllegalArgumentException(String.format("filters is invalid; \"%s\" is not a valid name for a feature", featureName));
            }
        }


        // Create the request and return

        CollectionRequest timeSeriesRequest = new CollectionRequest();
        timeSeriesRequest._apiKey = null;
        timeSeriesRequest._source = source;
        timeSeriesRequest._category = category;
        timeSeriesRequest._label = label;
        timeSeriesRequest._features = features;
        timeSeriesRequest._maxSize = maxSize;
        timeSeriesRequest._filters = filters;
        timeSeriesRequest._clientType = ClientType.JAVA_CLIENT;

        return timeSeriesRequest;
    }


    /**
     * @param source    The source for the collection.
     * @param category  The category for the collection.
     * @param label     The label for the collection.
     * @param features  The (optional) list of features, or null.
     * @param maxSize   The maximum number of items that can be returned, or -1 if there should be no limit.
     * @param filters   The filters.
     * @param apiKey    The API key.
     */
    public static CollectionRequest of(String source, String category, String label, List<String> features, long maxSize, Map<String, String> filters, String apiKey)
    {
        // Check the arguments

        if (source == null)
        {
            throw new IllegalArgumentException("source is null");
        }

        if (!DataUtils.validToken(source))
        {
            throw new IllegalArgumentException("source is not valid");
        }

        if (category == null)
        {
            throw new IllegalArgumentException("code is null");
        }

        if (!DataUtils.validToken(category))
        {
            throw new IllegalArgumentException("code is not valid");
        }

        if (label == null)
        {
            throw new IllegalArgumentException("id is null");
        }

        if (!DataUtils.validToken(label))
        {
            throw new IllegalArgumentException("id is not valid");
        }

        if (features != null)
        {
            for(String feature : features)
            {
                if (!DataUtils.validToken(feature))
                {
                    throw new IllegalArgumentException(String.format("\"%s\" is not a valid name for a feature", feature));
                }
            }
        }

        if (maxSize < -1)
        {
            throw new IllegalArgumentException("maxSize is less than -1");
        }

        if (apiKey == null)
        {
            throw new IllegalArgumentException("apiKey is null");
        }

        if (!DataUtils.validToken(apiKey))
        {
            throw new IllegalArgumentException("apiKey is not a valid API Key");
        }

        if (filters == null)
        {
            throw new IllegalArgumentException("filters is null");
        }

        for(Map.Entry<String, String> filter : filters.entrySet())
        {
            String featureName = filter.getKey();

            if (!DataUtils.validToken(featureName))
            {
                throw new IllegalArgumentException(String.format("filters is invalid; \"%s\" is not a valid name for a feature", featureName));
            }
        }


        // Create the request and return

        CollectionRequest timeSeriesRequest = new CollectionRequest();
        timeSeriesRequest._apiKey = apiKey;
        timeSeriesRequest._source = source;
        timeSeriesRequest._category = category;
        timeSeriesRequest._label = label;
        timeSeriesRequest._features = features;
        timeSeriesRequest._maxSize = maxSize;
        timeSeriesRequest._filters = filters;
        timeSeriesRequest._clientType = ClientType.JAVA_CLIENT;

        return timeSeriesRequest;
    }


    /**
     * @return The API key.
     */
    public String getApiKey()
    {
        return _apiKey;
    }


    /**
     * Sets the API Key
     *
     * @param apiKey The new API key.
     */
    public void setApiKey(String apiKey)
    {
        _apiKey = apiKey;
    }


    /**
     * @return The source for the collection.
     */
    public String getSource()
    {
        return _source;
    }


    /**
     * @return The category for the collection.
     */
    public String getCategory()
    {
        return _category;
    }


    /**
     * @return The label for the collection.
     */
    public String getLabel()
    {
        return _label;
    }


    /**
     * @return The (optional) list of features, or null.
     */
    public List<String> getFeatures()
    {
        return _features;
    }


    /**
     * @return The maximum number of items that can be returned, or -1 if there should be no limit.
     */
    public long getMaxSize()
    {
        return _maxSize;
    }


    /**
     * @return The filters.
     */
    public Map<String, String> getFilters()
    {
        return _filters;
    }


    /**
     * @return The type of client that sent the request.
     */
    public String getClientType()
    {
        return _clientType;
    }


    /**
     * Used to convert a Protobuf timestamp to a LocalDateTime.
     *
     * @param timestamp The Protobuf timestamp (which may be null).
     * @return The date/time for timestamp, or null if timestamp is null.
     */
    private static LocalDateTime getLocalDateTime(TimestampProto.Timestamp timestamp)
    {
        if (timestamp == null)
        {
            return null;
        }
        else
        {
            return LocalDateTime.ofEpochSecond(timestamp.getEpochSecond(), timestamp.getNanosecond(), ZoneOffset.UTC);
        }
    }


    /**
     * Sets the state of this from a provided Protobuf object.
     *
     * @param request The Protobuf object to set the state from.
     */
    void readProto(CollectionRequestProto.CollectionRequest request)
    {
        // Get the atomic properties

        _apiKey = request.getApiKey();
        _source = request.getSource();
        _category = request.getCategory();
        _label = request.getLabel();
        _maxSize = request.getMaxSize();
        _filters = request.getFiltersMap();
        _clientType = request.getClientType();


        // Build the features list

        if (request.getFeaturesSet())
        {
            ProtocolStringList stringList = request.getFeaturesList();
            _features = new ArrayList<>();

            if (!stringList.isEmpty())
            {
                _features.addAll(stringList);
            }
        }
        else
        {
            _features = null;
        }
    }


    /**
     * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
     *
     * @param serialized The serialized state of an object.
     * @throws Exception If an error happened during the deserialization process.
     */
    private void readProto(byte[] serialized) throws Exception
    {
        CollectionRequestProto.CollectionRequest request = CollectionRequestProto.CollectionRequest.parseFrom(serialized);
        readProto(request);
    }


    /**
     * Creates a new instance using a byte[] created by serialize().
     *
     * @param serialized The byte[].
     * @throws Exception If there were any issues when deserializing the object.
     */
    public static CollectionRequest deserialize(byte[] serialized) throws Exception
    {
        CollectionRequest request = new CollectionRequest();
        request.readProto(serialized);

        return request;
    }


    /**
     * @return A Protobuf object containing the serialised state of this.
     */
    CollectionRequestProto.CollectionRequest writeProtoObject()
    {
        CollectionRequestProto.CollectionRequest.Builder requestBuilder = CollectionRequestProto.CollectionRequest.newBuilder();
        requestBuilder.setApiKey(_apiKey).setSource(_source).setCategory(_category).setLabel(_label).setMaxSize(_maxSize);

        if (_features == null)
        {
            requestBuilder.setFeaturesSet(false);
        }
        else
        {
            requestBuilder.addAllFeatures(_features);
            requestBuilder.setFeaturesSet(true);
        }

        requestBuilder.putAllFilters(_filters);
        requestBuilder.setClientType(_clientType);

        return requestBuilder.build();
    }


    /**
     * @return A byte[] containing the state of this serialised.
     */
    public byte[] serialize()
    {
        CollectionRequestProto.CollectionRequest request = writeProtoObject();

        return request.toByteArray();
    }
}
