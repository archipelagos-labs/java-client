package archipelagos.common.data.collection;

import archipelagos.common.protobuf.common.data.collection.CollectionProto;

/**
 * Holds data about a subset of a collection.
 */
public final class Collection
{
   private CollectionData _data;
   private CollectionMetadata _metadata;


   /**
    * @param metadata The metadata for the collection.
    * @param data     The data.
    * @return The time-series.
    */
   public static Collection of(CollectionMetadata metadata, CollectionData data)
   {
      // Check the arguments

      if (metadata == null)
      {
         throw new IllegalArgumentException("metadata is null");
      }

      if (data == null)
      {
         throw new IllegalArgumentException("data is null");
      }


      // Create the collection and return

      Collection collection = new Collection();
      collection._metadata = metadata;
      collection._data = data;

      return collection;
   }


   /**
    * Creates an object without initializing any properties.
    */
   Collection()
   {
   }


   /**
    * @return The metadata for the collection.
    */
   public CollectionMetadata getMetadata()
   {
      return _metadata;
   }


   /**
    * @return The data.
    */
   public CollectionData getData()
   {
      return _data;
   }


   /**
    * @see Object
    */
   @Override
   public boolean equals(Object o)
   {
      if (o instanceof Collection)
      {
         Collection collection = (Collection) o;

         if (_metadata == null)
         {
            if (collection._metadata != null)
            {
               return false;
            }
         }
         else
         {
            if (!_metadata.equals(collection._metadata))
            {
               return false;
            }
         }

         if (_data == null)
         {
            return collection._data == null;
         }
         else
         {
            return _data.equals(collection._data);
         }
      }
      else
      {
         return false;
      }
   }


   /**
    * Sets the state of this from a provided Protobuf object.
    *
    * @param collection The Protobuf object to set the state from.
    */
   void readProto(CollectionProto.Collection collection)
   {
      _metadata = new CollectionMetadata();
      _metadata.readProto(collection.getMetadata());

      _data = new CollectionData();
      _data.readProto(collection.getData());
   }


   /**
    * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
    *
    * @param serialized The serialized state of an object.
    * @throws Exception If an error happened during the deserialization process.
    */
   private void readProto(byte[] serialized) throws Exception
   {
      CollectionProto.Collection collection = CollectionProto.Collection.parseFrom(serialized);
      readProto(collection);
   }


   /**
    * Creates a new instance using a byte[] created by serialize().
    *
    * @param serialized The byte[].
    * @throws Exception If there were any issues when deserializing the object.
    */
   public static Collection deserialize(byte[] serialized) throws Exception
   {
      Collection collection =  new Collection();
      collection.readProto(serialized);

      return collection;
   }


   /**
    * @return A Protobuf object containing the serialised state of this.
    */
   CollectionProto.Collection writeProtoObject()
   {
      return CollectionProto.Collection.newBuilder().setMetadata(_metadata.writeProtoObject()).setData(_data.writeProtoObject()).build();
   }


   /**
    * @return A byte[] containing the state of this serialised.
    */
   public byte[] serialize()
   {
      CollectionProto.Collection collection = writeProtoObject();

      return collection.toByteArray();
   }
}
