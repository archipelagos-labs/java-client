package archipelagos.common.data.collection;

import archipelagos.common.protobuf.common.data.collection.CollectionProto;
import archipelagos.common.protobuf.common.data.collection.CollectionResponseProto;

/**
 * Represents the response to a collection request.
 */
public final class CollectionResponse
{
    private boolean _errorOccurred;
    private Collection _collection;
    private int _errorCode;
    private String _errorMessage;


    /**
     *
     */
    CollectionResponse()
    {
    }


    /**
     * Called when data is to be returned.
     *
     * @param collection The data to return.
     */
    public static CollectionResponse of(Collection collection)
    {
        // Check the arguments

        if (collection == null)
        {
            throw new IllegalArgumentException("collection is null");
        }


        // Create the response and return

        CollectionResponse response = new CollectionResponse();
        response._errorOccurred = false;
        response._collection = collection;

        return response;
    }


    /**
     * Called when an error has occurred.
     *
     * @param errorCode    The error code.
     * @param errorMessage The error message (must be non-null).
     */
    public static CollectionResponse of(int errorCode, String errorMessage)
    {
        // Check the arguments

        if (errorMessage == null)
        {
            throw new IllegalArgumentException("errorMessage is null");
        }


        // Create the response and return

        CollectionResponse response = new CollectionResponse();
        response._errorOccurred = true;
        response._errorCode = errorCode;
        response._errorMessage = errorMessage;

        return response;
    }


    /**
     * @return true if an error occurred, false otherwise.
     */
    public boolean errorOccurred()
    {
        return _errorOccurred;
    }


    /**
     * @return if an error did not occur then the data to return, null otherwise.
     */
    public Collection getCollection()
    {
        return _collection;
    }


    /**
     * @return if an error occurred then the error code, 0 otherwise.
     */
    public int getErrorCode()
    {
        return _errorCode;
    }


    /**
     * @return if an error occurred then the error message, null otherwise.
     */
    public String getErrorMessage()
    {
        return _errorMessage;
    }


    /**
     * Sets the state of this from a provided Protobuf object.
     *
     * @param response The Protobuf object to set the state from.
     */
    void readProto(CollectionResponseProto.CollectionResponse response)
    {
        if (response.getErrorHasOccurred())
        {
            _errorOccurred = true;
            _errorCode = response.getErrorCode();
            _errorMessage = response.getErrorMessage();
        }
        else
        {
            _errorOccurred = false;
            _collection = new Collection();
            _collection.readProto(response.getCollection());
        }
    }


    /**
     * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
     *
     * @param serialized The serialized state of an object.
     * @throws Exception If an error happened during the deserialization process.
     */
    private void readProto(byte[] serialized) throws Exception
    {
        CollectionResponseProto.CollectionResponse collectionResponse = CollectionResponseProto.CollectionResponse.parseFrom(serialized);
        readProto(collectionResponse);
    }


    /**
     * Creates a new instance using a byte[] created by serialize().
     *
     * @param serialized The byte[].
     * @throws Exception If there were any issues when deserializing the object.
     */
    public static CollectionResponse deserialize(byte[] serialized) throws Exception
    {
        CollectionResponse response = new CollectionResponse();
        response.readProto(serialized);

        return response;
    }


    /**
     * @return A Protobuf object containing the serialised state of this.
     */
    CollectionResponseProto.CollectionResponse writeProtoObject()
    {
        if (_errorOccurred)
        {
            return CollectionResponseProto.CollectionResponse.newBuilder().setErrorHasOccurred(true).setErrorCode(_errorCode).setErrorMessage(_errorMessage).build();
        }
        else
        {
            CollectionProto.Collection collection = _collection.writeProtoObject();
            return CollectionResponseProto.CollectionResponse.newBuilder().setErrorHasOccurred(false).setCollection(collection).build();
        }
    }


    /**
     * @return A byte[] containing the state of this serialised.
     */
    public byte[] serialize()
    {
        CollectionResponseProto.CollectionResponse response = writeProtoObject();

        return response.toByteArray();
    }
}
