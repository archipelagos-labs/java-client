package archipelagos.common.data.collection;

import archipelagos.common.data.DataUtils;
import archipelagos.common.data.SerializationUtils;
import archipelagos.common.protobuf.common.data.DataEntryProto;
import archipelagos.common.protobuf.common.data.collection.CollectionDataProto;

import java.util.*;

/**
 * Contains the data for a collection.
 */
public final class CollectionData
{
    private Map<String, Map<String, Object>> _data;


    /**
     * @param data The data.
     * @return The collection data.
     */
    public static CollectionData of(Map<String, Map<String, Object>> data)
    {
        if (data == null)
        {
            throw new IllegalArgumentException("data is null");
        }
        else
        {
            // Check that the contents of data are valid

            for (Map.Entry<String, Map<String, Object>> dataEntry : data.entrySet())
            {
                // Check the time for the entry is not null

                String id = dataEntry.getKey();

                if (id == null)
                {
                    throw new IllegalArgumentException("data contains a null key");
                }


                // Check that the feature map is not null

                Map<String, Object> featuresMap = dataEntry.getValue();

                if (featuresMap == null)
                {
                    throw new IllegalArgumentException("data contain a null value");
                }


                // Check that features

                for (Map.Entry<String, Object> feature : featuresMap.entrySet())
                {
                    // Check the feature name is valid

                    String featureName = feature.getKey();

                    if (featureName == null)
                    {
                        throw new IllegalArgumentException(String.format("data is invalid; the Map for %s contains a null key", id));
                    }
                    else
                    {
                        if (!DataUtils.validToken(featureName))
                        {
                            throw new IllegalArgumentException(String.format("data is invalid; the Map for %s contains an invalid key (%s)", id, featureName));
                        }
                    }


                    // Check the feature value is valid (null is valid)

                    Object featureValue = feature.getValue();

                    if (!SerializationUtils.validFeatureValue(featureValue))
                    {
                        throw new IllegalArgumentException(String.format("data is invalid; the Map for %s contains an invalid value (%s) for the feature \"%s\")", id, featureValue, featureName));
                    }
                }
            }


            // Create the data object and return

            CollectionData collectionData = new CollectionData();
            collectionData._data = data;

            return collectionData;
        }
    }


    /**
     * Creates an object without initializing any properties.
     */
    CollectionData()
    {
    }


    /**
     * @return The data.
     */
    public Map<String, Map<String, Object>> getData()
    {
        return _data;
    }


    /**
     * @return The feature maps without the associated IDs.
     */
    public java.util.Collection<Map<String, Object>> getFlattenedData()
    {
        return _data.values();
    }


    /**
     * @see Object
     */
    @Override
    public boolean equals(Object o)
    {
        if (o instanceof CollectionData)
        {
            CollectionData collectionData = (CollectionData) o;

            if (_data.size() != collectionData._data.size())
            {
                return false;
            }
            else
            {
                for (Map.Entry<String, Map<String, Object>> dataMapEntry : _data.entrySet())
                {
                    String id = dataMapEntry.getKey();

                    if (!collectionData._data.containsKey(id))
                    {
                        return false;
                    }
                    else
                    {
                        Map<String, Object> featuresMap1 = dataMapEntry.getValue();
                        Map<String, Object> featuresMap2 = collectionData._data.get(id);

                        if (featuresMap1.size() != featuresMap2.size())
                        {
                            return false;
                        }
                        else
                        {
                            for (Map.Entry<String, Object> feature : featuresMap1.entrySet())
                            {
                                String featureName = feature.getKey();

                                if (!featuresMap2.containsKey(featureName))
                                {
                                    return false;
                                }
                                else
                                {
                                    Object value1 = feature.getValue();
                                    Object value2 = featuresMap2.get(featureName);

                                    if (!SerializationUtils.featureValueEquals(value1, value2))
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }

                return true;
            }
        }
        else
        {
            return false;
        }
    }


    /**
     * Sets the state of this from a provided Protobuf object.
     *
     * @param data The Protobuf object to set the state from.
     */
    void readProto(CollectionDataProto.CollectionData data)
    {
        int count = data.getIdsCount();
        _data = new HashMap<>();

        for (int i = 0; i < count; i++)
        {
            // Get the feature map

            DataEntryProto.DataEntry dataEntry = data.getData(i);
            Map<String, Object> featuresMap = new HashMap<>();

            for (Map.Entry<String, DataEntryProto.DataEntry> dataForEntry : dataEntry.getMapValueMap().entrySet())
            {
                DataEntryProto.DataEntry featureValueProto = dataForEntry.getValue();
                String featureName = dataForEntry.getKey();
                Object featureValue = SerializationUtils.getFeatureValue(featureValueProto);
                featuresMap.put(featureName, featureValue);
            }


            // Add the data collected above to that stored within this object

            String id = data.getIds(i);

            if (_data.containsKey(id))
            {
                throw new IllegalArgumentException(String.format("There is more than one entry with ID \"%s\"", id));
            }
            else
            {
                _data.put(id, featuresMap);
            }
        }
    }


    /**
     * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
     *
     * @param serialized The serialized state of an object.
     * @throws Exception If an error happened during the deserialization process.
     */
    private void readProto(byte[] serialized) throws Exception
    {
        CollectionDataProto.CollectionData data = CollectionDataProto.CollectionData.parseFrom(serialized);
        readProto(data);
    }


    /**
     * Creates a new instance using a byte[] created by writeProto().
     *
     * @param serialized The byte[].
     * @throws Exception If there were any issues when deserializing the object.
     */
    public static CollectionData deserialize(byte[] serialized) throws Exception
    {
        CollectionData data = new CollectionData();
        data.readProto(serialized);

        return data;
    }


    /**
     * @return A Protobuf object containing the serialised state of this.
     */
    CollectionDataProto.CollectionData writeProtoObject()
    {
        // Create the lists used below to build the proto objects

        int count = _data.size();
        List<String> ids = new ArrayList<>(count);
        List<DataEntryProto.DataEntry> data = new ArrayList<>(count);


        // Add the data contained in this to the objects created above

        for (Map.Entry<String, Map<String, Object>> entry : _data.entrySet())
        {
            // Create and add the ID

            String id = entry.getKey();
            ids.add(id);


            // Create and add the features

            Map<String, DataEntryProto.DataEntry> protoFeatures = new HashMap<>();
            Map<String, Object> featuresMap = entry.getValue();

            for (Map.Entry<String, Object> feature : featuresMap.entrySet())
            {
                protoFeatures.put(feature.getKey(), SerializationUtils.getFeatureValue(feature.getValue()));
            }

            data.add(DataEntryProto.DataEntry.newBuilder().setIsMap(true).setIsList(false).setIsAtomic(false).putAllMapValue(protoFeatures).build());
        }


        // Return the proto object

        return CollectionDataProto.CollectionData.newBuilder().addAllIds(ids).addAllData(data).build();
    }


    /**
     * @return A byte[] containing the state of this serialised.
     */
    public byte[] serialize()
    {
        CollectionDataProto.CollectionData data = writeProtoObject();

        return data.toByteArray();
    }
}
