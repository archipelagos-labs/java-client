package archipelagos.common.data;

import archipelagos.common.protobuf.common.data.DataIdProto;

/**
 * Represents the ID for an item of data.
 */
public final class DataId
{
    private String _source;
    private String _category;
    private String _label;
    private int _hashCode;


    /**
     * @param source    The source for the data.
     * @param category  The category for the data.
     * @param label     The label for the data.
     */
    public static DataId of(String source, String category, String label)
    {
        return new DataId(source, category, label);
    }


    /**
     * Creates an object without initializing any properties.
     */
    DataId()
    {
    }


    /**
     * @param source   The source for the data.
     * @param category The category for the data.
     * @param label    The label for the data.
     */
    DataId(String source, String category, String label)
    {
        // Check the arguments

        if (!DataUtils.validToken(source))
        {
            throw new IllegalArgumentException(String.format("The source \"%s\" is not a valid token", source));
        }

        if (!DataUtils.validToken(category))
        {
            throw new IllegalArgumentException(String.format("The category \"%s\" is not a valid token", category));
        }

        if (!DataUtils.validToken(label))
        {
            throw new IllegalArgumentException(String.format("The label \"%s\" is not a valid token", label));
        }


        // Store the data

        _source = source;
        _category = category;
        _label = label;

        buildHashCode();
    }

    /**
     * Builds and caches the hash code associated with this.
     */
    private void buildHashCode()
    {
        int prime = 31, result = 1;

        result = prime * result + ((_source == null) ? 0 : _source.hashCode());
        result = prime * result + ((_category == null) ? 0 : _category.hashCode());
        result = prime * result + ((_label == null) ? 0 : _label.hashCode());

        _hashCode = result;
    }


    /**
     * @return The source for the data.
     */
    public String getSource()
    {
        return _source;
    }


    /**
     * @return The category for the data.
     */
    public String getCategory()
    {
        return _category;
    }


    /**
     * @return The label for the data.
     */
    public String getLabel()
    {
        return _label;
    }


    /**
     * @see Object
     */
    @Override
    public boolean equals(Object o)
    {
        // Check that the object is non-null and of the correct type

        if (o instanceof DataId)
        {
            DataId dataId = (DataId) o;


            // Evaluate the source

            if (_source == null)
            {
                if (dataId._source != null)
                {
                    return false;
                }
            }
            else
            {
                if (!_source.equals(dataId._source))
                {
                    return false;
                }
            }


            // Evaluate the category

            if (_category == null)
            {
                if (dataId._category != null)
                {
                    return false;
                }
            }
            else
            {
                if (!_category.equals(dataId._category))
                {
                    return false;
                }
            }


            // Evaluate the label

            if (_label == null)
            {
                if (dataId._label != null)
                {
                    return false;
                }
            }
            else
            {
                if (!_label.equals(dataId._label))
                {
                    return false;
                }
            }


            // Everything matches so return true

            return true;
        }
        else
        {
            return false;
        }
    }


    /**
     * @see Object
     */
    @Override
    public int hashCode()
    {
        return _hashCode;
    }


    /**
     * @see Object
     */
    @Override
    public String toString()
    {
        StringBuilder toReturn = new StringBuilder();

        toReturn.append("Source = \"");
        toReturn.append(_source);
        toReturn.append("\", Category = \"");
        toReturn.append(_category);
        toReturn.append("\", Label = \"");
        toReturn.append(_label);
        toReturn.append("\"");

        return toReturn.toString();
    }


    /**
     * Sets the state of this from a provided Protobuf object.
     *
     * @param dataId The Protobuf object to set the state from.
     */
    void readProto(DataIdProto.DataId dataId)
    {
        // Set any simple attributes

        _source = dataId.getSource();
        _category = dataId.getCategory();
        _label = dataId.getLabel();


        // Rebuild the hashcode

        buildHashCode();
    }


    /**
     * Sets the state of this from a byte[] containing the state of an object serialised using Protobuf.
     *
     * @param serialized The serialized state of an object.
     * @throws Exception If an error happened during the deserialization process.
     */
    private void readProto(byte[] serialized) throws Exception
    {
        DataIdProto.DataId dataId = DataIdProto.DataId.parseFrom(serialized);
        readProto(dataId);
    }


    /**
     * Creates a new instance using a byte[] created by serialize().
     *
     * @param serialized The byte[].
     * @throws Exception If there were any issues when deserializing the object.
     */
    public static DataId deserialize(byte[] serialized) throws Exception
    {
        DataId dataId = new DataId();
        dataId.readProto(serialized);

        return dataId;
    }


    /**
     * @return A Protobuf object containing the serialised state of this.
     */
    DataIdProto.DataId writeProtoObject()
    {
        return DataIdProto.DataId.newBuilder().setSource(_source).setCategory(_category).setLabel(_label).build();
    }


    /**
     * @return A byte[] containing the state of this serialised.
     */
    public byte[] serialize()
    {
        DataIdProto.DataId dataId = writeProtoObject();

        return dataId.toByteArray();
    }
}
