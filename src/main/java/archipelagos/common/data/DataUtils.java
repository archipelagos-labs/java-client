package archipelagos.common.data;

import java.text.*;
import java.time.*;
import java.time.format.*;
import java.util.*;

/**
 * Provides static utility methods and constants of use when dealing with data.
 */
public class DataUtils
{
	public final static String FOLDER_SEPARATOR = "/";
	public final static String ROOT_FOLDER = "";
	private final static DateTimeFormatter YYYY_MM_DD_HH_MM_SS_N_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.n");
	private final static DateTimeFormatter YYYY_MM_DD_HH_MM_SS_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
	private final static DateTimeFormatter YYYY_M_D_FORMATTER = DateTimeFormatter.ofPattern("yyyy-M-d");
	private final static SimpleDateFormat YYYY_MM_DD_HH_MM_SS_SSS_FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");


	/**
	 * Ascertains if a folder name is of a valid format.
	 *
	 * @param folder The folder to check.
	 * @return true if the folder was of a valid format, false otherwise.
	 */
	public static boolean isValidFolder(String folder)
	{
		String[] subFolders = folder.split(DataUtils.FOLDER_SEPARATOR);

		for (String subFolder : subFolders)
		{
			if (!DataUtils.validToken(subFolder))
			{
				return false;
			}
		}

		return true;
	}


	/**
	 * Ascertains the sub-folders contained in a folder.
	 *
	 * Assumes isValidFolder(folder).
	 *
	 * @param folder The folder.
	 * @return The names of the sub-folders in folder ().
	 */
	public static String[] getSubFolders(String folder)
	{
		if ("".equals(folder))
		{
			return new String[0];
		}
		else
		{
			return folder.split(DataUtils.FOLDER_SEPARATOR);
		}
	}


	/**
	 * Gets a LocalDateTime representing a Date.
	 *
	 * @param date The date/time.
	 * @return The LocalDateTime representing date, or null if it could not be parsed.
	 */
	public static LocalDateTime getLocalDateTime(Date date)
	{
		if (date == null)
		{
			return null;
		}
		else
		{
			Instant instant = date.toInstant();
			return LocalDateTime.ofEpochSecond(instant.getEpochSecond(), instant.getNano(), ZoneOffset.UTC);
		}
	}


	/**
	 * Gets a Date representing a LocalDateTime (excluding microseconds/nanoseconds).
	 *
	 * @param dateTime The date/time.
	 * @return The Date representing dateTime, or null if it could not be parsed.
	 */
	public static Date getDate(LocalDateTime dateTime)
	{
		if (dateTime == null)
		{
			return null;
		}
		else
		{
			return Date.from(dateTime.atZone(ZoneOffset.UTC).toInstant());
		}
	}


	/**
	 * Parses a date/time in YYYY-MM-ddTHH:mm:ss[.N] format.
	 *
	 * @param dateTime The date/time.
	 * @return The date/time representing dateTime, or null if it could not be parsed.
	 */
	public static LocalDateTime parseDateTime(String dateTime)
	{
		try
		{
			return LocalDateTime.parse(dateTime, YYYY_MM_DD_HH_MM_SS_N_FORMATTER);
		}
		catch (DateTimeParseException e1)
		{
			try
			{
				return LocalDateTime.parse(dateTime, YYYY_MM_DD_HH_MM_SS_FORMATTER);
			}
			catch (DateTimeParseException e2)
			{
				try
				{
					LocalDate localDate = LocalDate.parse(dateTime, YYYY_M_D_FORMATTER);
					return LocalDateTime.of(localDate.getYear(), localDate.getMonth(), localDate.getDayOfMonth(), 0, 0, 0);
				}
				catch (DateTimeParseException e3)
				{
					return null;
				}
			}
		}
	}


	/**
	 * Produces a String representing a date/time in YYYY-MM-ddTHH:mm:ss.SSS format.
	 *
	 * @param date The date/time.
	 * @return The date/time in YYYY-MM-ddTHH:mm:ss.SSS format, or "" if date was null.
	 */
	public static String getDateTime(Date date)
	{
		if (date == null)
		{
			return "";
		}
		else
		{
			return YYYY_MM_DD_HH_MM_SS_SSS_FORMATTER.format(date);
		}
	}


	/**
	 * Produces a String representing a date/time in YYYY-MM-ddTHH:mm:ss.n format.
	 *
	 * @param dateTime The date/time.
	 * @return The date/time in YYYY-MM-ddTHH:mm:ss format, or "" if dateTime was null.
	 */
	public static String getDateTime(LocalDateTime dateTime)
	{
		if (dateTime == null)
		{
			return "";
		}
		else
		{
			return YYYY_MM_DD_HH_MM_SS_N_FORMATTER.format(dateTime);
		}
	}


	/**
	 * Generates a list from a string containing the elements of a list separated by commas.
	 *
	 * @param commaSeparated The list items separated by commas.
	 * @return The list.
	 */
	public static List<String> fromList(String commaSeparated)
	{
		List<String> toReturn = new ArrayList<>();

		if (commaSeparated != null)
		{
			String[] listItems = commaSeparated.split(",");
			toReturn.addAll(Arrays.asList(listItems));
		}

		return toReturn;
	}


	/**
	 * Generates a  string containing the elements of a list seperated by commas.
	 *
	 * @param items The items of the list.
	 * @return The elements in items separated by commas.
	 */
	public static String getList(List<String> items)
	{
		StringBuilder sb = new StringBuilder();
		int size = items.size(), i = 0;

		for (; i < size - 1; i++)
		{
			sb.append(items.get(i));
			sb.append(",");
		}

		sb.append(items.get(i));
		return sb.toString();
	}


	/**
	 * Used to determine if a token is a valid (i.e. only contains: characters, digits, underscore, hyphen).
	 *
	 * @param token The token to evaluate.
	 * @return true if the token is valid, false otherwise.
	 */
	public static boolean validToken(String token)
	{
		if (token == null)
		{
			return false;
		}
		else
		{
			int size = token.length();

			if (size == 0)
			{
				return false;
			}
			else if (token.trim().length() == 0)
			{
				return false;
			}
			else
			{
				boolean valid = true;

				for (int i = 0; i < size; i++)
				{
					char ch = token.charAt(i);

					if (!Character.isLetterOrDigit(ch) && ('_' != ch) && ('-' != ch))
					{
						valid = false;
						break;
					}
				}

				return valid && size <= 30;
			}
		}
	}
}
