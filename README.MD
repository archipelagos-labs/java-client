Setup Sonatype project.
Verify domain name via TXT record.
Install GPG
Use the server setting from settings.xml in DevOps project to reference key.
Update version in pom.xml.
Maven clean, compile, install and deploy.
Tag in gitlab.com